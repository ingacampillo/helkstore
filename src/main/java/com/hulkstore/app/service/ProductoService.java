package com.hulkstore.app.service;

import java.util.List;

import com.hulkstore.app.dto.request.ProductoRequestDTO;
import com.hulkstore.app.dto.response.ProductoResponseDTO;
import com.hulkstore.app.entity.Producto;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
/**
 * Interfaz Service encargada de realizar las transacciones y operaciones de {@link Producto}.
 * @author </br>
 * Developer: Arnold Campillo
 */
public interface ProductoService {
	
	Producto obtenerProductoPorId(Integer productoId);
	/**
	 * Método encargado de obtener todos los productos del sistema.
	 * 
	 * @return {@link List}<{@link ProductoResponseDTO}>: listado de las productos del
	 *         sistema.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	List<ProductoResponseDTO> findAll()
			throws ValidateServiceException, NoDataFoundException, GeneralServiceException;

	 /**
     * Método encargado de actualizar la Producto.
     * @param dto del tipo {@link ProductoRequestDTO}: dto solicitado en la actualización.
     * @return {@link ProductoResponseDTO}: Producto del sistema actualizado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	ProductoResponseDTO update(ProductoRequestDTO dto)throws ValidateServiceException;
	
	 /**
     * Método encargado de registrar la marca.
     * @param dto del tipo {@link ProductoRequestDTO}: dto solicitado en el registro.
     * @return {@link ProductoResponseDTO}: Marca del sistema registrado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	ProductoResponseDTO insert(ProductoRequestDTO dto)throws ValidateServiceException;
	
	
}
