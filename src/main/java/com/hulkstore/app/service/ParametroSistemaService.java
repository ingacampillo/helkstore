package com.hulkstore.app.service;

import java.util.List;
import java.util.Map;

import com.hulkstore.app.dto.request.ParametroSistemaRequestDTO;
import com.hulkstore.app.dto.response.ParametroSistemaResponseDTO;
import com.hulkstore.app.entity.ParametroSistema;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
/**
 * Interfaz Service encargada de realizar las transacciones y operaciones de {@link ParametroSistema}.
 * @author </br>
     * Developer: Arnold Campillo
 */
public interface ParametroSistemaService {

	 /**
     * Método encargado de obtener todos los parámetros del sistema.
     * @return {@link List}<{@link ParametroSistemaResponseDTO}>: listado de los parámetros del sistema. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	List<ParametroSistemaResponseDTO> findAll()  throws ValidateServiceException, NoDataFoundException, GeneralServiceException;
	
	/**
     * Método encargado de obtener todos los parámetros del sistema.
     * @return {@link Map}<{@link String},{@link String}>: Map de los parámetros del sistema. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	Map<String, String> obtenerParametros() throws ValidateServiceException, NoDataFoundException, GeneralServiceException;
	
	/**
     * Método encargado de obtener el parámetro del sistema por código.
     * @param codigo del tipo {@link String}: codigo solicitado en la consulta.
     * @return {@link ParametroSistemaResponseDTO}: Parámetro del sistema obtenido. 
    * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	ParametroSistemaResponseDTO getParametroByCodigo(String codigo) throws ValidateServiceException, NoDataFoundException, GeneralServiceException ;
	
	 /**
     * Método encargado de actualizar el parámetro del sistema.
     * @param dto del tipo {@link ParametroSistemaRequestDTO}: dto solicitado en la actualización.
     * @return {@link ParametroSistemaResponseDTO}: Parámetro del sistema actualizado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	ParametroSistemaResponseDTO update(ParametroSistemaRequestDTO dto)throws ValidateServiceException;
	
	 /**
     * Método encargado de registrar el parámetro del sistema.
     * @param dto del tipo {@link ParametroSistemaRequestDTO}: dto solicitado en el registro.
     * @return {@link ParametroSistemaResponseDTO}: Parámetro del sistema registrado. 
    * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	ParametroSistemaResponseDTO insert(ParametroSistemaRequestDTO dto)throws ValidateServiceException;
	
	
	
}
