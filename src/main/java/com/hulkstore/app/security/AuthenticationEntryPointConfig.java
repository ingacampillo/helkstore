package com.hulkstore.app.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.AuthenticationsException;
import com.hulkstore.app.util.ResponseUtil;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
/**
 * Clase de configuración encargada de personalizar lo errores presentados al realizar la autenticación por HTTP_BASIC.
 * @author </br>
     * Developer: Arnold Campillo
 */
@Component
@RequiredArgsConstructor
public class AuthenticationEntryPointConfig  extends BasicAuthenticationEntryPoint {

	private final ResponseUtil responseUtil;

	 /**
     * Método encargado de gestionar los errores({@link AuthenticationException}) y realizar su respectiva personalización.
     * @param request del tipo {@link HttpServletRequest}: Request para uso del tratamiento del error.  
     * @param response del tipo {@link HttpServletResponse}: Response para uso del tratamiento del error.
     * @param authException del tipo {@link AuthenticationException}: Error presentado a tratar.  
    * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws AuthenticationsException {	
		String requestURI = request.getRequestURI();
		if(isPublicRouteSwagger(requestURI)) {
			   response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");
		}

		this.responseUtil.buildErrorResponse(response,request, HttpServletResponse.SC_UNAUTHORIZED, MensajesEnum.AUTH_REQUIRED,authException);	
	
	}
	/**
     * Método de configuración encargado de asignar el realmName. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	@Override
	public void afterPropertiesSet() {
		setRealmName("levare-admincontent");
	    super.afterPropertiesSet();
	 }
	/**
     * Método encargado de validar si el request es de swagger(OpenAPi).
     * @param requestURI del tipo {@link String}: Request Url a validar.
     * @return {@link boolean}: Resultado de la validación.  
     * @author </br>
     * Developer: Arnold Campillo
     */ 
	private boolean isPublicRouteSwagger(String requestURI) {
		// Verificar si la ruta coincide con alguna de las rutas públicas.
		// Si no coincide con ninguna de las rutas públicas, se considera una ruta
				// privada.
		return ConstantesApp.PUBLIC_ROUTES_SWAGGER.stream().anyMatch(requestURI::contains);
	}

}
