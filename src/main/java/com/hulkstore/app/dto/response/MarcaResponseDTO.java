package com.hulkstore.app.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

/**
 * DTO response que contiene la información acerca de las marcas 
 * @author </br>
 * Developer: Arnold campillo
 */
@Builder
@Schema(description = "DTO response que contiene la información acerca de marcas.")
public record MarcaResponseDTO (

	@Schema(description = "Identificador de la marca.",example = "16551615615611")
	Integer id,
	@Schema(description = "Código de la marca.",example = "CODE_052")
	String codigo,
	@Schema(description = "Nombre de la marca.",example = "MARCA_UNICA")
	String nombre,
	@Schema(description = "Indica si la marca  esta activo.",example = "true")
	Boolean estBorrado
	

){}
