package com.hulkstore.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hulkstore.app.entity.OrdenProducto;

@Repository
public interface OrdenProductoRepository extends JpaRepository<OrdenProducto, Integer> {
	
	Optional<OrdenProducto> findById(Integer id);

	
	

}
