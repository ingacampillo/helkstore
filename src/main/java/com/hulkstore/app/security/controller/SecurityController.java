package com.hulkstore.app.security.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.AuthenticationsException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.controller.dto.WrapperResponse;
import com.hulkstore.app.security.dto.request.RefreshTokenRequestDTO;
import com.hulkstore.app.security.dto.response.AuthUserResponseDTO;
import com.hulkstore.app.security.service.SecurityService;
import com.hulkstore.app.util.ResponseUtil;
import com.hulkstore.app.util.WebUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

/**
 * Controller encargado de gestionar los Endpoints con relación a la seguridad
 * del sistema.
 * 
 * @author </br>
 *         Developer: Arnold Campillo
 */
@RestController
@RequestMapping({ "/" })
@Tag(name = "Autenticación Api", description = "Api encargada de las funcionalidades relacionadas con la seguridad de la aplicación.")
@RequiredArgsConstructor
public class SecurityController {

	private final SecurityService service;
	private final ResponseUtil responseUtil;
	private final WebUtil webUtil;

	/**
	 * Método encargado de realizar la petición(POST) para realizar la autenticación
	 * en el sistema.
	 * 
	 * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}<{@link AuthUserResponseDTO}>>>:
	 *         Response con el resultado de la petición.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@PostMapping("/auth")
	@SecurityRequirement(name = ConstantesApp.AUTH_METHOD_BASIC)
	@Operation(summary = "Realiza la autenticación en el sistema.", description = "Realiza la autenticación en el sistema con la modalidad Basic(Username, password).", parameters = {
			@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in = ParameterIn.HEADER, name = ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE) }, responses = {
					@ApiResponse(responseCode = "200", description = "Autenticación exitosa."),
					@ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
					@ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
					@ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
					@ApiResponse(responseCode = "404", description = "Usuario no existe.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
					@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))) })
	public ResponseEntity<WrapperResponse<AuthUserResponseDTO>> auth() throws AuthenticationsException {
		service.hashCode();
		AuthUserResponseDTO auth = webUtil.getUser();
		if (ObjectUtils.isEmpty(auth)) {
			throw new AuthenticationsException(MensajesEnum.CREDENCIALS_REQUIRED);
		}
		return responseUtil.buildSuccessResponseWrapper(auth).createResponse();
	}

	/**
	 * Método encargado de realizar la petición(POST) para realizar la renovación de
	 * los tokens de acceso al sistema.
	 * 
	 * @param dto del tipo {@link RefreshTokenRequestDTO}: Dto Request solicitado en
	 *            la petición.
	 * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}<{@link AuthUserResponseDTO}>>>:
	 *         Response con el resultado de la petición.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@PostMapping("/refresh-token")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@SecurityRequirement(name = ConstantesApp.AUTH_METHOD_BEARER)
	@Operation(summary = "Realiza la renovación de lo tokens de acceso al sistema.", description = "<strong>Debes tener en cuenta que el token enviado en el ResponseBody es el token de acceso a renovar y el token enviado en el bearer es el token refresh</strong></br>Realiza la renovación de lo tokens de acceso al sistema.", parameters = {
			@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in = ParameterIn.HEADER, name = ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE) }, responses = {
					@ApiResponse(responseCode = "200", description = "Renovación exitosa."),
					@ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
					@ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
					@ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
					@ApiResponse(responseCode = "404", description = "Token no existe.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
					@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))) })
	public ResponseEntity<WrapperResponse<AuthUserResponseDTO>> refreshToken(
			@Valid @RequestBody RefreshTokenRequestDTO dto)
			throws ValidateServiceException, NoDataFoundException, AuthenticationsException {
		return responseUtil.buildSuccessResponseWrapper(service.refreshToken(dto)).createResponse();
	}

	/**
	 * Método encargado de realizar la petición(POST) para realizar el logout del
	 * usuario en el sistema.
	 * 
	 * @param usercode del tipo {@link String}: Path param solicitado en la
	 *                 petición.
	 * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}<{@link Void}>>>:
	 *         Response con el resultado de la petición.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@PostMapping("/logout/{usercode}")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@SecurityRequirement(name = ConstantesApp.AUTH_METHOD_BEARER)
	@Operation(summary = "Realiza el cierre de sesión de lo usuario autenticado en el sistema.", description = ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS
			+ "Realiza el cierre de sesión de lo usuario autenticado en el sistema a partir de su usercode.", parameters = {
					@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in = ParameterIn.HEADER, name = ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE),
					@Parameter(description = "Código del usuario.", in = ParameterIn.PATH, name = "usercode", schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true) }, responses = {
							@ApiResponse(responseCode = "200", description = "Cierre de sesión exitosa."),
							@ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
							@ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
							@ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
							@ApiResponse(responseCode = "404", description = "Usuario no existe.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
							@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.", content = @Content(schema = @Schema(implementation = WrapperResponse.class))) })
	public ResponseEntity<WrapperResponse<Void>> logout(@PathVariable(name = "usercode") String usercode)
			throws NoDataFoundException {
		service.logout(usercode);
		return responseUtil.buildSuccessResponseWrapper().createResponseVoid();
	}

}
