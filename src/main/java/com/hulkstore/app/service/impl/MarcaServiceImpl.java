package com.hulkstore.app.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.adapter.MarcaAdapter;
import com.hulkstore.app.dto.request.MarcaRequestDTO;
import com.hulkstore.app.dto.response.CategoriaResponseDTO;
import com.hulkstore.app.dto.response.MarcaResponseDTO;
import com.hulkstore.app.entity.Marca;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.repository.MarcaRepository;
import com.hulkstore.app.service.MarcaService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * Service encargado de realizar las transacciones y operaciones de {@link Marca}. Implementación {@link MarcaService}.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class MarcaServiceImpl implements MarcaService {
	
	private final MarcaRepository repository;
	
	public Marca obtenerMarcaPorId(Integer marcaId) {
		return repository.findById(marcaId).orElseThrow(() -> new NoDataFoundException(MensajesEnum.MARCA_BY_CODE_NOT_FOUND,marcaId));
	}
	
	/**
	 * Método encargado de obtener todos las categorias del sistema.
	 * 
	 * @return {@link List}<{@link CategoriaResponseDTO}>: listado de las categorias del
	 *         sistema.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@Override
	public List<MarcaResponseDTO> findAll()
			throws ValidateServiceException, NoDataFoundException, GeneralServiceException {
		try {
			List<Marca> marcaList = this.repository.findAll();
			return MarcaAdapter.listEntityToResponse(marcaList);
		} catch (ValidateServiceException | NoDataFoundException e) {
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(MensajesEnum.ERROR, e);
		}
	}

	
	 /**
     * Método encargado de actualizar la marca.
     * @param dto del tipo {@link MarcaRequestDTO}: dto solicitado en la actualización.
     * @return {@link MarcaResponseDTO}: Marca del sistema actualizado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	@Override
	public MarcaResponseDTO update(MarcaRequestDTO dto) throws ValidateServiceException {
		if(ObjectUtils.isEmpty(dto)) {
			return null;
		}
		if(ObjectUtils.isEmpty(dto.id())) {
			throw new ValidateServiceException(MensajesEnum.ID_REQUIRED);
		}		
		return this.save(dto);
	}
	
	 /**
    * Método encargado de registrar la publicación.
    * @param dto del tipo {@link MarcaRequestDTO}: dto solicitado en el registro.
    * @return {@link MarcaResponseDTO}: Publicación del sistema registrado. 
    * @author </br>
    * Developer: Arnold Campillo
    * @throws
    */
	@Override
	public MarcaResponseDTO insert(MarcaRequestDTO dto) throws ValidateServiceException {
		return this.save(dto);
	}
	
	
	
	/**
     * Método encargado de registrar o actualizar la marca
     * @param dto del tipo {@link MarcaRequestDTO}: dto solicitado en el registro o actualización.
     * @return {@link MarcaResponseDTO}: Marca  registrado o actualizado. 
     * @author </br>
     * Developer: Arnold Campillo.
     * @throws
     */
	private MarcaResponseDTO save(MarcaRequestDTO dto) {
		Marca entity=MarcaAdapter.requestToEntity.apply(dto);
		validarNombreMarcaUnico(entity);
		entity.setEstBorrado(false);
		Marca savedMarca=repository.save(entity);
		return MarcaAdapter.entityToResponse.apply(savedMarca);
	}

	
	private void validarNombreMarcaUnico(Marca marca) {
		if (ObjectUtils.isEmpty(marca.getId()) && repository.existsByNombre(marca.getNombre())) {
			throw new ValidateServiceException(MensajesEnum.MARCA_EXIST_BY_NAME,marca.getNombre());
		}
	}


}
