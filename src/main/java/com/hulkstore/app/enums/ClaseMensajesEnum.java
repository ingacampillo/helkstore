package com.hulkstore.app.enums;
/**
 * {@link Enum} que contiene las clases disponibles para los mensajes {@link MensajesEnum}.
 * @author </br>
 * Developer: Arnold Campillo
 */
public enum ClaseMensajesEnum {
	GENERIC, BUSINESS_ERROR, TECHNICAL_ERROR;
}
