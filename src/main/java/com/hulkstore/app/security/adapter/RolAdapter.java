package com.hulkstore.app.security.adapter;

import java.util.List;
import java.util.function.Function;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.security.dto.response.RolResponseDTO;
import com.hulkstore.app.security.entity.Rol;

/**
 * Adaptador encargado de definir la manera en que se deben convertir
 * objetos entre Entity y DTOs (Y viceversa).
 * @author </br>
     * Developer: Arnold Campillo
 */
public class RolAdapter {

	private RolAdapter() {}
	
	 /**
     * Función encargada de adaptar un objeto Entity a su respectivo DTO Response(de {@link Rol} a {@link RolResponseDTO}).
     * @param entity del tipo {@link Rol}: Entity a convertir a DTO Response. 
     * @return {@link RolResponseDTO}: DTO Response resultado de la conversión. 
    * @author </br>
     * Developer: Arnold Campillo
     */

	public static final Function<Rol, RolResponseDTO> entityToResponse = (Rol entity) -> {
		if (ObjectUtils.isEmpty(entity)) {
			return null;
		}  
        return RolResponseDTO.builder()
        		.id(entity.getId())
	    		.codigo(entity.getCodigo())
	    		.valor(entity.getValor())
	    		.descripcion(entity.getDescripcion())
	    	    .enabled(entity.isEnabled())
	    	    .estBorrado(entity.isEstBorrado())
	    	    .build();
    };
    
	  /**
     * Función encargada de adaptar un listado Entity a su respectivo listado DTO Response(de {@link List}<{@link Rol}> a {@link List}<{@link RolResponseDTO}>).
     * @param entityList del tipo {@link List}<{@link Rol}>: Entity List a convertir a DTO Response List. 
     * @return {@link List}<{@link RolResponseDTO}>: DTO Response List resultado de la conversión. 
     * @author </br>
     * Developer: Arnold Campillo
     */
    public static final Function<List<Rol>, List<RolResponseDTO>> entityListToResponseList = (List<Rol> entityList) -> {
		if (CollectionUtils.isEmpty(entityList)) {
			return null;
		}  
        return entityList.stream().map(entityToResponse::apply).toList();
    };
}
