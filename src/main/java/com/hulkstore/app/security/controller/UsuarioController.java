package com.hulkstore.app.security.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.controller.dto.WrapperResponse;
import com.hulkstore.app.security.dto.request.NewUserRequestDTO;
import com.hulkstore.app.security.dto.response.UsuarioResponseDTO;
import com.hulkstore.app.security.service.UsuarioService;
import com.hulkstore.app.util.ResponseUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
/**
 * Controller encargado de gestionar los Endpoints con relación a los usuarios del sistema.
* @author </br>
     * Developer: Arnold Campillo
 */
@RestController
@RequestMapping({ "/user" })
@Tag(name="Usuario Api", description="Api encargada de las funcionalidades relacionadas con los usuarios de la aplicación."+ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER)
@RequiredArgsConstructor
public class UsuarioController {

	private final UsuarioService service;
	private final ResponseUtil responseUtil;
	
	 /**
    * Método encargado de realizar la petición(DELETE) para realizar la eliminación del usuario en el sistema.
    * @param usercode del tipo {@link String}: Path param solicitado en la petición.
    * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}<{@link Void}>>>: Response con el resultado de la petición. 
   * @author </br>
     * Developer: Arnold Campillo
    * @throws
    */
	@DeleteMapping(value = "/{usercode}")
	@Secured({"ROLE_ADMIN"})
	@SecurityRequirement(name = ConstantesApp.AUTH_METHOD_BASIC)
	@Operation(summary = "Realiza la eliminación del usuario en el sistema.", description=ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS+"Realiza la eliminación del usuario a partir de su código de registro, en el sistema.",
	parameters = {
			@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in =ParameterIn.HEADER, name =ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example=ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE),
			@Parameter(description = "Código del usuario.", in =ParameterIn.PATH, name = "usercode", schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true)
	},
	responses =  {
			@ApiResponse(responseCode = "200", description = "Eliminación exitosa."),
            @ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
            @ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
            @ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
            @ApiResponse(responseCode = "404", description = "Usuario no existe.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
			@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) 
	}) 
	public ResponseEntity<WrapperResponse<Void>> delete(@PathVariable(name = "usercode") String usercode) throws ValidateServiceException,NoDataFoundException{
		service.deleteUser(usercode);
		return responseUtil.buildSuccessResponseWrapper().createResponseVoid();
	}
	 /**
    * Método encargado de realizar la petición(GET) para realizar la consulta de los usuarios registrados en el sistema.
    * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}<{@link UsuarioResponseDTO}>>>: Response con el resultado de la petición. 
   * @author </br>
     * Developer: Arnold Campillo
    */
	@GetMapping
	@Secured({"ROLE_ADMIN"})
	@SecurityRequirement(name = ConstantesApp.AUTH_METHOD_BASIC)
	@Operation(summary = "Realiza la consulta de los usuarios en el sistema.", description=ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS+"Realiza la consulta de todos los usuario registrados en el sistema.",
	parameters = {
			@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in =ParameterIn.HEADER, name =ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example=ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE)
	},
	responses =  {
			@ApiResponse(responseCode = "200", description = "Consulta exitosa."),
            @ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
            @ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
            @ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
            @ApiResponse(responseCode = "404", description = "No existen usuarios.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
			@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) 
	}) 
	public ResponseEntity<WrapperResponse<List<UsuarioResponseDTO>>>getAll(){
		return responseUtil.buildSuccessResponseWrapper(service.findAll()).createResponse();
	}
	
	 /**
    * Método encargado de realizar la petición(POST) para la creación de los usuarios admin en el sistema.
    * @param dto del tipo {@link NewUserRequestDTO}: Dto request solicitado en la petición.
    * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}<{@link Void}>>>: Response con el resultado de la petición. 
   * @author </br>
     * Developer: Arnold Campillo
    * @throws
    */
	@PostMapping(value = "/create/admin")	
	@Secured({"ROLE_ADMIN"})
	@SecurityRequirement(name = ConstantesApp.AUTH_METHOD_BASIC)
	@Operation(summary = "Realiza la creación de usuario con rol administrador en el sistema.", description=ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS+"Realiza la creación de usuario con rol administrador en el sistema.", 
	parameters = {
			@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in =ParameterIn.HEADER, name =ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example=ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE)
	},
	responses =  {
			@ApiResponse(responseCode = "200", description = "Creación exitosa."),
            @ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
            @ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
            @ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
            @ApiResponse(responseCode = "404", description = "Usuario ya existe.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
			@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) 
	}) 
	public ResponseEntity<WrapperResponse<Void>> createAdmin(@Valid @RequestBody NewUserRequestDTO dto) throws ValidateServiceException {
		service.registerAdmin(dto);
		return responseUtil.buildSuccessResponseWrapper().createResponseVoid();
	}
	 /**
    * Método encargado de realizar la petición(POST) para la creación de los usuarios en el sistema.
    * @param dto del tipo {@link NewUserRequestDTO}: Dto request solicitado en la petición.
    * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}<{@link Void}>>>: Response con el resultado de la petición. 
    * @author </br>
     * Developer: Arnold Campillo
    * @throws
    */
	@PostMapping(value = "/create/user")
	@Secured({"ROLE_ADMIN"})
	@SecurityRequirement(name = ConstantesApp.AUTH_METHOD_BASIC)
	@Operation(summary = "Realiza la creación de usuario con rol usuario en el sistema.", description=ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS+"Realiza la creación de usuario con rol usuario en el sistema.",
	parameters = {
			@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in =ParameterIn.HEADER, name =ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example=ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE)
	},
	responses =  {
			@ApiResponse(responseCode = "200", description = "Creación exitosa."),
            @ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
            @ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
            @ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
            @ApiResponse(responseCode = "404", description = "Usuario ya existe.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
			@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) 
	}) 
	public ResponseEntity<WrapperResponse<Void>> createUser(@Valid @RequestBody NewUserRequestDTO dto) throws ValidateServiceException {
		service.registerUser(dto);
		return responseUtil.buildSuccessResponseWrapper().createResponseVoid();
	}
	
	
	
}
