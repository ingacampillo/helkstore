package com.hulkstore.app.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.dto.request.OrdenProductoRequestDTO;
import com.hulkstore.app.dto.response.OrdenProductoResponseDTO;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.controller.dto.WrapperResponse;
import com.hulkstore.app.service.OrdenProductoService;
import com.hulkstore.app.util.ResponseUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

/**
 * Controller encargado de gestionar los Endpoints de OrdenProductos.
 * @author </br>
 * Developer: Arnold Campillo
 */
@RestController
@RequestMapping({ "/orden-producto" })
@Tag(name="OrdenProducto Api", description="Api Encargada de administrar las OrdenProductos. "+ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER)
@RequiredArgsConstructor
public class OrdenController {

	private final OrdenProductoService service;
	private final  ResponseUtil responseUtil;
	
	 /**
     * Método encargado de realizar la petición(GET) de todos los OrdenProductos del sistema.
     * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}<{@link OrdenProductoResponseDTO}>>>: Response con el resultado de la petición. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	@Operation(summary = "Realiza la consulta de todos las OrdenProductos del sistema.", description=ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS+"Realiza la consulta general de todos las OrdenProductos del sistema registrados en la aplicación.", 
			parameters = {
					@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in =ParameterIn.HEADER, name =ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example=ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE)		
			},
			responses =  {
					@ApiResponse(responseCode = "200", description = "Consulta exitosa."),
		            @ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
		            @ApiResponse(responseCode = "404", description = "No hay registros disponibles.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
					@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) 
			}) 
	@Secured({"ROLE_ADMIN"})
	@GetMapping
	public ResponseEntity<WrapperResponse<List<OrdenProductoResponseDTO>>> getAll() throws ValidateServiceException, NoDataFoundException, GeneralServiceException{
		return responseUtil.buildSuccessResponseWrapper(service.findAll()).createResponse();
	}
		
	 /**
     * Método encargado de realizar la petición(POST) para registrar la OrdenProducto.
     * @param dto del tipo {@link OrdenProductoRequestDTO}: Dto request solicitado en la petición.
     * @return {@link ResponseEntity}<{@link WrapperResponse}<<{@link OrdenProductoResponseDTO}>>: Response con el resultado de la petición. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	@Operation(summary = "Realiza el registro de OrdenProductos en el sistema.", description=ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS+"Realiza el registro de un nueva OrdenProducto en la aplicación.", 
			parameters = {
					@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in =ParameterIn.HEADER, name =ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example=ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE)		
			},
			responses =  {
					@ApiResponse(responseCode = "200", description = "Registro exitoso."),
		            @ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
		            @ApiResponse(responseCode = "404", description = "El código ya se encuentra registrado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
					@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) 
			}) 
	@Secured({"ROLE_ADMIN"})
	@PostMapping
	public ResponseEntity<WrapperResponse<OrdenProductoResponseDTO>> insert(@Valid @RequestBody OrdenProductoRequestDTO dto) throws ValidateServiceException, NoDataFoundException, GeneralServiceException{
		return responseUtil.buildSuccessResponseWrapper(service.insert(dto)).createResponse();
	}
	
	 /**
     * Método encargado de realizar la petición(PUT) para actualizar el OrdenProducto.
     * @param dto del tipo {@link OrdenProductoRequestDTO}: Dto request solicitado en la petición.
     * @return {@link ResponseEntity}<{@link WrapperResponse}<<{@link OrdenProductoResponseDTO}>>: Response con el resultado de la petición. 
     * @author Acasi.</br>
     * Developer: Giovanny Camacho.
     * @throws
     */
	@Operation(summary = "Realiza la actualización del OrdenProducto en el sistema.", description=ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS+"Realiza la actualización del OrdenProducto en la aplicación.", 
			parameters = {
					@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in =ParameterIn.HEADER, name =ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example=ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE)		
			},
			responses =  {
					@ApiResponse(responseCode = "200", description = "Actualización exitosa."),
		            @ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
		            @ApiResponse(responseCode = "404", description = "El id debe ser enviado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
					@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) 
			}) 
	@Secured({"ROLE_ADMIN"})
	@PutMapping
	public ResponseEntity<WrapperResponse<OrdenProductoResponseDTO>> update(@Valid @RequestBody OrdenProductoRequestDTO dto) throws ValidateServiceException, NoDataFoundException, GeneralServiceException{
		return responseUtil.buildSuccessResponseWrapper(service.update(dto)).createResponse();
	}
	
	
	
}
