package com.hulkstore.app.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

/**
 * DTO response que contiene la información acerca de las categorias 
 * @author </br>
 * Developer: Arnold campillo
 */
@Builder
@Schema(description = "DTO response que contiene la información acerca de categorias.")
public record CategoriaResponseDTO (

	@Schema(description = "Identificador de la categoria.",example = "16551615615611")
	Integer id,
	@Schema(description = "Código de la categoria.",example = "CODE_052")
	String codigo,
	@Schema(description = "Nombre de la categoria.",example = "MARCA_UNICA")
	String nombre,
	@Schema(description = "Indica si la categoria  esta activo.",example = "true")
	Boolean estBorrado
	

){}
