package com.hulkstore.app.security.service;

import org.springframework.security.core.context.SecurityContextHolder;

import com.hulkstore.app.exceptions.AuthenticationsException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.dto.request.AuthUserRequestDTO;
import com.hulkstore.app.security.dto.request.RefreshTokenRequestDTO;
import com.hulkstore.app.security.dto.response.AuthUserResponseDTO;

import jakarta.servlet.http.HttpServletRequest;

/**
 * Intefaz Service encargado de realizar las transacciones y operaciones de seguridad en el sistema.
 * @author </br>
     * Developer: Arnold Campillo
 */
public interface SecurityService {
	
	 /**
	 * Método encargado de obtener la información del usuario a partir del usercode.
	 * @param username del tipo {@link String}: Usuario a buscar.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario obtenido.  
	 * @author </br>
     * Developer: Arnold Campillo
	 */
	public AuthUserResponseDTO loadUserByUsername(String username) throws NoDataFoundException;
	
	 /**
	 * Método encargado de validar el token con relación al usuario.
	 * @param username del tipo {@link String}: Usuario a validar.
	 * @param token del tipo {@link String}: Token a validar.
	 * @author </br>
     * Developer: Arnold Campillo
	 * @throws
	 */
	public void validateTokenUser(String username, String token) throws AuthenticationsException, NoDataFoundException, ValidateServiceException;
	
	 /**
	 * Método encargado de realizar la autenticación del usuario en el sistema.
	 * @param dto del tipo {@link AuthUserRequestDTO}: Dto request con la información requerida para la autenticación.
	 * @param token del tipo {@link String}: Token a validar.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario autenticado.
	* @author </br>
     * Developer: Arnold Campillo
	 * @throws
	 */
	public AuthUserResponseDTO authenticate(AuthUserRequestDTO dto) throws AuthenticationsException ;
	
	 /**
	 * Método encargado de realizar el refresh token y generar nuevos tokens de acceso.
	 * @param dto del tipo {@link RefreshTokenRequestDTO}: Dto request con la información requerida para la petición.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario con los tokens actualizados.
	 * @author </br>
     * Developer: Arnold Campillo
	 * @throws
	 */
	public AuthUserResponseDTO refreshToken(RefreshTokenRequestDTO dto) throws ValidateServiceException, NoDataFoundException, AuthenticationsException;
	
	 /**
	 * Método encargado de realizar el logout del usuario en el sistema.
	 * @param usercode del tipo {@link String}: usercode requerido para la petición. 
	* @author </br>
     * Developer: Arnold Campillo
	 * @throws
	 */
	public void logout(String usercode) throws NoDataFoundException, AuthenticationsException;
	
	 /**
	 * Método encargado de asignar el usuario al contexto de autenticación {@link SecurityContextHolder}.
	 * @param token del tipo {@link String}: token requerido para la petición. 
	 * @param request del tipo {@link HttpServletRequest}: Request requerido para la solicitud.
	 * @author </br>
     * Developer: Arnold Campillo
	 * @throws
	 */
	public String setAuthenticationContext(String token, HttpServletRequest request) throws ValidateServiceException, NoDataFoundException;	


}
