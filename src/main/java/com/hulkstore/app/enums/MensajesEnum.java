package com.hulkstore.app.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 * {@link Enum} que contiene los mensajes del sistema.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Getter
@AllArgsConstructor
public enum MensajesEnum {
	
	OK("app.message.000","APP_000", CategoriaMensajesEnum.INFO, ClaseMensajesEnum.GENERIC),
	ERROR("app.message.999","APP_999", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	ERROR_PROCESS("app.message.998","APP_998", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	METHOD_NOT_ALLOWED("app.message.900","APP_900", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	BAD_REQUEST("app.message.901","APP_901", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	BAD_REQUEST_QUERY_PARAM("app.message.902","APP_902", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	BAD_REQUEST_PATH_PARAM("app.message.903","APP_903", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	BAD_REQUEST_TYPE_DATA("app.message.904","APP_904", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	BAD_REQUEST_TYPE_DATA_BODY("app.message.905","APP_905", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	BAD_REQUEST_TYPE_DATA_GENERIC("app.message.906","APP_906", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	BAD_REQUEST_VALID_PARAMS("app.message.907","APP_907", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	ERROR_AUTH("app.message.908","APP_908", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	AUTH_REQUIRED("app.message.909","APP_909", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	HEADERS_REQUIRED("app.message.910","APP_910", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	HEADERS_STRUCTURE_INVALID("app.message.911","APP_911", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	CORS_ORIGIN_ERROR("app.message.912","APP_912", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	HEADER_REQUIRED("app.message.913","APP_913", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	HEADER_EQUALS("app.message.914","APP_914", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	LANG_NO_SOPORT("app.message.915","APP_915", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	RE_AUTH_REQUIRED("app.message.916","APP_916", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	NOT_FOUND_RESOURCE("app.message.917","APP_917", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	CREDENCIALS_REQUIRED("app.message.001","APP_001", CategoriaMensajesEnum.ERROR,ClaseMensajesEnum.BUSINESS_ERROR),
	USER_REQUIRED("app.message.002","APP_002", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	PASSWORD_REQUIRED("app.message.003","APP_003", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	TOKEN_INCORRECT("app.message.004","APP_004", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	TOKEN_EXPIRED("app.message.005","APP_005", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	TOKEN_REFRESH_INCORRECT("app.message.006","APP_006", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	TOKEN_REFRESH_EXPIRED("app.message.007","APP_007", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	TOKEN_INCORRECT_USER("app.message.008","APP_008", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	TOKEN_INCORRECT_USER_NO_AUTH("app.message.009","APP_009", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	TOKEN_REFRESH_ATTEMPTS("app.message.010","APP_010", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	TOKEN_REFRESH_NO_CORRECT("app.message.011","APP_011", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.TECHNICAL_ERROR),
	USER_NO_FOUND("app.message.012","APP_012", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_NO_ACTIVE("app.message.013","APP_013", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_BLOQ_PASS_INCORRECT("app.message.014","APP_014", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_PASS_INCORRECT("app.message.015","APP_015", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_NO_ROLES("app.message.016","APP_016", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_FOUND("app.message.017","APP_017", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_NO_FOUND_ID("app.message.018","APP_018", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_CHANGE_PASS("app.message.019","APP_019", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USERCODE_NO_FOUND("app.message.020","APP_020", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	ROLE_NO_FOUND("app.message.021","APP_021", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_PETITION_CODE_NOT_FOUND("app.message.022","APP_022", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_PETITION_CODE_EXPIRED("app.message.023","APP_023", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_PASS_ACTUAL_INCORRECT("app.message.024","APP_024", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_PASS_TEMP_INCORRECT("app.message.025","APP_025", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),	
	USER_IS_DELETED("app.message.026","APP_026", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_IS_INACTIVE("app.message.027","APP_027", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_IS_BLOCKED("app.message.028","APP_028", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_IS_ACTIVE("app.message.029","APP_029", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_IS_UNBLOCKED("app.message.030","APP_030", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	USER_USERNAME_INVALID("app.message.031","APP_031", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	ID_REQUIRED("app.message.032","APP_032", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	CODE_ALREADY_REGISTERED("app.message.033","APP_033", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	REGISTRY_NO_UPDATES("app.message.034","APP_034", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	CATEGORIA_BY_CODE_NOT_FOUND("app.message.036","APP_036", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	MARCA_BY_CODE_NOT_FOUND("app.message.037","APP_037", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	PRODUCTO_BY_CODE_NOT_FOUND("app.message.038","APP_038", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	OUT_STOCK_PRODUCT("app.message.039","APP_039", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	CATEGORIA_EXIST_BY_NAME("app.message.040","APP_040", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	MARCA_EXIST_BY_NAME("app.message.041","APP_041", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	PRODUCTO_EXIST_BY_REFERENCIA("app.message.042","APP_042", CategoriaMensajesEnum.ERROR, ClaseMensajesEnum.BUSINESS_ERROR),
	;
	
	private String message;
	private String code;
	private CategoriaMensajesEnum category;
	private ClaseMensajesEnum clas;
	

}

