package com.hulkstore.app.adapter;

import java.util.List;
import java.util.function.Function;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.dto.request.ParametroSistemaRequestDTO;
import com.hulkstore.app.dto.response.ParametroSistemaResponseDTO;
import com.hulkstore.app.entity.ParametroSistema;

/**
 * Adaptador encargado de definir la manera en que se deben convertir
 * objetos entre Entity y DTOs (Y viceversa).
 * @author </br>
 * Developer: Arnold Campillo.
 */
public class ParametroSistemaAdapter {
	
	private ParametroSistemaAdapter() {}
	
	  /**
     * Función encargada de adaptar un objeto DTO Request a su respectivo Entity(de {@link ParametroSistemaRequestDTO} a {@link ParametroSistema}).
     * @param dto del tipo {@link ParametroSistemaRequestDTO}: Dto Request a convertir a Entity.  
     * @return {@link ParametroSistema}: Entity resultado de la conversión. 
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static final Function<ParametroSistemaRequestDTO, ParametroSistema> requestToEntity = (ParametroSistemaRequestDTO dto) -> {
		if (ObjectUtils.isEmpty(dto)) {
			return null;
		}  
        return ParametroSistema.builder()
        		.id(dto.id())
        		.codigo(dto.codigo())
        		.valor(dto.valor())
        		.descripcion(dto.descripcion())
        		.estBorrado(dto.estBorrado())
        		.permiteUpdate(dto.permiteUpdate())
				.build();
    };
    
	  /**
     * Función encargada de adaptar un objeto Entity a su respectivo DTO Response(de {@link ParametroSistema} a {@link ParametroSistemaRequestDTO}).
     * @param entity del tipo {@link ParametroSistema}: Entity a convertir a DTO Response. 
     * @return {@link ParametroSistemaRequestDTO}: DTO Response resultado de la conversión. 
     * @author </br>
     * Developer: Arnold Campillo
     */
    public static final Function<ParametroSistema, ParametroSistemaResponseDTO> entityToResponse = (ParametroSistema entity) -> {
		if (ObjectUtils.isEmpty(entity)) {
			return null;
		}  
		
		return ParametroSistemaResponseDTO.builder()
        		.id(entity.getId())
        		.codigo(entity.getCodigo())
        		.valor(entity.getValor())
        		.descripcion(entity.getDescripcion())
        		.estBorrado(entity.isEstBorrado())
        		.permiteUpdate(entity.isPermiteUpdate())
				.build();
    };
    
	  /**
     * Función encargada de adaptar un listado Entity a su respectivo listado DTO Response(de {@link List}<{@link ParametroSistema}> a {@link List}<{@link ParametroSistemaRequestDTO}>).
     * @param entityList del tipo {@link List}<{@link ParametroSistema}>: Entity List a convertir a DTO Response List. 
     * @return {@link List}<{@link ParametroSistemaRequestDTO}>: DTO Response List resultado de la conversión. 
    * @author </br>
     * Developer: Arnold Campillo
     */
    
    public static final List<ParametroSistemaResponseDTO> listEntityToResponse(List<ParametroSistema> entityList) {
    	if (CollectionUtils.isEmpty(entityList)) {
    		return List.of();
    	} 
        return entityList.stream()
                .map(entityToResponse)
               .toList();
    }

}
