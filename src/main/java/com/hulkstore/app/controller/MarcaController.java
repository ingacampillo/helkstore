package com.hulkstore.app.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.dto.request.MarcaRequestDTO;
import com.hulkstore.app.dto.response.MarcaResponseDTO;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.controller.dto.WrapperResponse;
import com.hulkstore.app.service.MarcaService;
import com.hulkstore.app.util.ResponseUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

/**
 * Controller encargado de gestionar los Endpoints de marcas.
 * @author </br>
 * Developer: Arnold Campillo
 */
@RestController
@RequestMapping({ "/marca" })
@Tag(name="Marca Api", description="Api Encargada de administrar las marcas. "+ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER)
@RequiredArgsConstructor
public class MarcaController {

	private final MarcaService service;
	private final  ResponseUtil responseUtil;
	
	 /**
     * Método encargado de realizar la petición(GET) de todos los marcas del sistema.
     * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}<{@link MarcaResponseDTO}>>>: Response con el resultado de la petición. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	@Operation(summary = "Realiza la consulta de todos las marcas del sistema.", description=ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS+"Realiza la consulta general de todos las marcas del sistema registrados en la aplicación.", 
			parameters = {
					@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in =ParameterIn.HEADER, name =ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example=ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE)		
			},
			responses =  {
					@ApiResponse(responseCode = "200", description = "Consulta exitosa."),
		            @ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
		            @ApiResponse(responseCode = "404", description = "No hay registros disponibles.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
					@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) 
			}) 
	@Secured({"ROLE_ADMIN"})
	@GetMapping
	public ResponseEntity<WrapperResponse<List<MarcaResponseDTO>>> getAll() throws ValidateServiceException, NoDataFoundException, GeneralServiceException{
		return responseUtil.buildSuccessResponseWrapper(service.findAll()).createResponse();
	}
		
	 /**
     * Método encargado de realizar la petición(POST) para registrar la marca.
     * @param dto del tipo {@link MarcaRequestDTO}: Dto request solicitado en la petición.
     * @return {@link ResponseEntity}<{@link WrapperResponse}<<{@link MarcaResponseDTO}>>: Response con el resultado de la petición. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	@Operation(summary = "Realiza el registro de marcas en el sistema.", description=ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS+"Realiza el registro de un nueva marca en la aplicación.", 
			parameters = {
					@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in =ParameterIn.HEADER, name =ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example=ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE)		
			},
			responses =  {
					@ApiResponse(responseCode = "200", description = "Registro exitoso."),
		            @ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
		            @ApiResponse(responseCode = "404", description = "El código ya se encuentra registrado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
					@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) 
			}) 
	@Secured({"ROLE_ADMIN"})
	@PostMapping
	public ResponseEntity<WrapperResponse<MarcaResponseDTO>> insert(@Valid @RequestBody MarcaRequestDTO dto) throws ValidateServiceException, NoDataFoundException, GeneralServiceException{
		return responseUtil.buildSuccessResponseWrapper(service.insert(dto)).createResponse();
	}
	
	 /**
     * Método encargado de realizar la petición(PUT) para actualizar el Marca.
     * @param dto del tipo {@link MarcaRequestDTO}: Dto request solicitado en la petición.
     * @return {@link ResponseEntity}<{@link WrapperResponse}<<{@link MarcaResponseDTO}>>: Response con el resultado de la petición. 
     * @author Acasi.</br>
     * Developer: Giovanny Camacho.
     * @throws
     */
	@Operation(summary = "Realiza la actualización del Marca en el sistema.", description=ConstantesApp.MSG_OPENAPI_GENERIC_CONTROLLER_METHODS+"Realiza la actualización del Marca en la aplicación.", 
			parameters = {
					@Parameter(description = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_DESCRIP, in =ParameterIn.HEADER, name =ConstantesApp.HEADER_TRACE_CODE, schema = @Schema(type = ConstantesApp.SWAGGER_SCHEMA_STRING), required = true, example=ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE)		
			},
			responses =  {
					@ApiResponse(responseCode = "200", description = "Actualización exitosa."),
		            @ApiResponse(responseCode = "400", description = "Validaciones no cumplidas.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "401", description = "Se require autenticación o permisos.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))),
		            @ApiResponse(responseCode = "403", description = "No tiene permisos para realizar esta acción.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
		            @ApiResponse(responseCode = "404", description = "El id debe ser enviado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) ,
					@ApiResponse(responseCode = "500", description = "Ha ocurrido un error inesperado.",content = @Content(schema = @Schema(implementation = WrapperResponse.class))) 
			}) 
	@Secured({"ROLE_ADMIN"})
	@PutMapping
	public ResponseEntity<WrapperResponse<MarcaResponseDTO>> update(@Valid @RequestBody MarcaRequestDTO dto) throws ValidateServiceException, NoDataFoundException, GeneralServiceException{
		return responseUtil.buildSuccessResponseWrapper(service.update(dto)).createResponse();
	}
	
	
	
}
