package com.hulkstore.app.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.adapter.ProductoAdapter;
import com.hulkstore.app.dto.request.ProductoRequestDTO;
import com.hulkstore.app.dto.response.ProductoResponseDTO;
import com.hulkstore.app.entity.Categoria;
import com.hulkstore.app.entity.Marca;
import com.hulkstore.app.entity.Producto;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.repository.ProductoRepository;
import com.hulkstore.app.service.CategoriaService;
import com.hulkstore.app.service.MarcaService;
import com.hulkstore.app.service.ProductoService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Service encargado de realizar las transacciones y operaciones de
 * {@link Producto}. Implementación {@link ProductoService}.
 * 
 * @author </br>
 *         Developer: Arnold Campillo
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ProductoServiceImpl implements ProductoService {

	private final ProductoRepository repository;
	private final MarcaService marcaService;
	private final CategoriaService categoriaService;

	public Producto obtenerProductoPorId(Integer productoId) {
		return repository.findById(productoId)
				.orElseThrow(() -> new NoDataFoundException(MensajesEnum.PRODUCTO_BY_CODE_NOT_FOUND, productoId));
	}

	/**
	 * Método encargado de obtener todos los productos del sistema.
	 * 
	 * @return {@link List}<{@link ProductoResponseDTO}>: listado de las productos
	 *         del sistema.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@Override
	public List<ProductoResponseDTO> findAll()
			throws ValidateServiceException, NoDataFoundException, GeneralServiceException {
		try {
			List<Producto> productoList = this.repository.findAll();
			return ProductoAdapter.listEntityToResponse(productoList);
		} catch (ValidateServiceException | NoDataFoundException e) {
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(MensajesEnum.ERROR, e);
		}
	}

	/**
	 * Método encargado de actualizar la Producto.
	 * 
	 * @param dto del tipo {@link ProductoRequestDTO}: dto solicitado en la
	 *            actualización.
	 * @return {@link ProductoResponseDTO}: Producto del sistema actualizado.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@Override
	public ProductoResponseDTO update(ProductoRequestDTO dto) throws ValidateServiceException {
		if (ObjectUtils.isEmpty(dto)) {
			return null;
		}
		if (ObjectUtils.isEmpty(dto.id())) {
			throw new ValidateServiceException(MensajesEnum.ID_REQUIRED);
		}
		return this.save(dto);
	}

	/**
	 * Método encargado de registrar la Producto.
	 * 
	 * @param dto del tipo {@link ProductoRequestDTO}: dto solicitado en el
	 *            registro.
	 * @return {@link ProductoResponseDTO}: Producto del sistema registrado.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@Override
	public ProductoResponseDTO insert(ProductoRequestDTO dto) throws ValidateServiceException {
		return this.save(dto);
	}

	/**
	 * Método encargado de registrar o actualizar la Producto
	 * 
	 * @param dto del tipo {@link ProductoRequestDTO}: dto solicitado en el registro
	 *            o actualización.
	 * @return {@link ProductoResponseDTO}: Producto registrado o actualizado.
	 * @author </br>
	 *         Developer: Arnold Campillo.
	 * @throws
	 */
	private ProductoResponseDTO save(ProductoRequestDTO dto) {
		Producto entity = ProductoAdapter.requestToEntity.apply(dto);
		validarReferencia(entity);
		entity.setEstBorrado(false);
		Marca marca = marcaService.obtenerMarcaPorId(dto.marcaId());
		Categoria caategoria = categoriaService.obtenerCategoriaPorId(dto.categoriaId());
		entity.setMarca(marca);
		entity.setCategoria(caategoria);
		Producto savedProducto = repository.save(entity);
		return ProductoAdapter.entityToResponse.apply(savedProducto);
	}

	
	private void validarReferencia(Producto producto) {
		if (ObjectUtils.isEmpty(producto.getId()) && repository.existsByReferencia(producto.getReferencia())) {
			throw new ValidateServiceException(MensajesEnum.PRODUCTO_EXIST_BY_REFERENCIA,producto.getReferencia());
		}
	}
}
