package com.hulkstore.app.security.service;

import java.util.List;

import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.dto.request.NewUserRequestDTO;
import com.hulkstore.app.security.dto.response.UsuarioResponseDTO;

/**
 * Intefaz Service encargado de realizar las transacciones y operaciones del usuario.
 * @author </br>
 * Developer: Arnold Campillo
 */
public interface UsuarioService {
	
	 /**
	 * Método encargado de obtener el usuario a patir del username.
	 * @param username del tipo {@link String}: Username del usuario.
	 * @return user del tipo {@link UsuarioResponseDTO}: Usuario obtenido de la consulta.
	* @author </br>
 * Developer: Arnold Campillo
	 * @throws
	 */
	public UsuarioResponseDTO findUserByUsername(String username) throws NoDataFoundException;
	
	 /**
	 * Método encargado de obtener el usuario a patir del usercode.
	 * @param usercode del tipo {@link String}: Usercode del usuario.
	 * @return user del tipo {@link UsuarioResponseDTO}: Usuario obtenido de la consulta.
	* @author </br>
 * Developer: Arnold Campillo
	 * @throws
	 */
	public UsuarioResponseDTO findUserByUsercode(String usercode) throws NoDataFoundException;
	
	 /**
	 * Método encargado de obtener el usuario a patir del id.
	 * @param id del tipo {@link Long}: Id del usuario.
	 * @return user del tipo {@link UsuarioResponseDTO}: Usuario obtenido de la consulta.
	 * @author </br>
 * Developer: Arnold Campillo
	 * @throws
	 */
	public UsuarioResponseDTO findUserById(Long id) throws NoDataFoundException;
	 /**
     * Método encargado de obtener todos los usuarios del sistema.
     * @return {@link List}<{@link UsuarioResponseDTO}>: listado de los usuarios del sistema. 
     * @author </br>
 * Developer: Arnold Campillo
     */
	public List<UsuarioResponseDTO> findAll();
	
	 /**
    * Método encargado de realizar la creación de los usuarios admin en el sistema.
    * @param dto del tipo {@link NewUserRequestDTO}: Dto request solicitado en la petición.
    * @author </br>
 * Developer: Arnold Campillo
    * @throws
    */
	public void registerAdmin(NewUserRequestDTO dto) throws ValidateServiceException ;
	
	 /**
    * Método encargado de realizar la creación de los usuarios en el sistema.
    * @param dto del tipo {@link NewUserRequestDTO}: Dto request solicitado en la petición.
    * @author </br>
 * Developer: Arnold Campillo
    * @throws
    */
	public void registerUser(NewUserRequestDTO dto) throws ValidateServiceException ;
	
	 /**
    * Método encargado de realizar la eliminación del usuario en el sistema.
    * @param usercode del tipo {@link String}: Usercode solicitado en la petición.
    * @author </br>
 * Developer: Arnold Campillo
    * @throws
    */
	public void deleteUser(String usercode) throws ValidateServiceException,NoDataFoundException;
	
	 /**
    * Método encargado de realizar la inactivación del usuario en el sistema.
    * @param usercode del tipo {@link String}: Usercode solicitado en la petición.
    * @author </br>
 * Developer: Arnold Campillo
    * @throws
    */
	public void inactivateUser(String usercode) throws NoDataFoundException, ValidateServiceException;
	
	 /**
    * Método encargado de realizar la activación del usuario en el sistema.
    * @param usercode del tipo {@link String}: Usercode solicitado en la petición.
    * @author </br>
 * Developer: Arnold Campillo
    * @throws
    */
	public void activateUser(String usercode) throws NoDataFoundException, ValidateServiceException;
	
	
	
	
}
