package com.hulkstore.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hulkstore.app.entity.Categoria;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {
	
	Boolean existsByNombre(String nombre);

	Optional<Categoria> findById(Integer id);

	Optional<Categoria> findByCodigo(String codigo);
	

}
