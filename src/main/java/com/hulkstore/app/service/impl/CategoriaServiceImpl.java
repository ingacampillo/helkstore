package com.hulkstore.app.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.adapter.CategoriaAdapter;
import com.hulkstore.app.dto.request.CategoriaRequestDTO;
import com.hulkstore.app.dto.response.CategoriaResponseDTO;
import com.hulkstore.app.entity.Categoria;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.repository.CategoriaRepository;
import com.hulkstore.app.service.CategoriaService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Service encargado de realizar las transacciones y operaciones de
 * {@link Categoria}. Implementación {@link CategoriaService}.
 * 
 * @author </br>
 *         Developer: Arnold Campillo
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class CategoriaServiceImpl implements CategoriaService {

	private final CategoriaRepository repository;
	
	public Categoria obtenerCategoriaPorId(Integer categoriaId) {
		return repository.findById(categoriaId)
				.orElseThrow(() ->new NoDataFoundException(MensajesEnum.CATEGORIA_BY_CODE_NOT_FOUND,categoriaId));
	}

	/**
	 * Método encargado de obtener todos las categorias del sistema.
	 * 
	 * @return {@link List}<{@link CategoriaResponseDTO}>: listado de las categorias del
	 *         sistema.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@Override
	public List<CategoriaResponseDTO> findAll()
			throws ValidateServiceException, NoDataFoundException, GeneralServiceException {
		try {
			List<Categoria> categoriasList = this.repository.findAll();
			return CategoriaAdapter.listEntityToResponse(categoriasList);
		} catch (ValidateServiceException | NoDataFoundException e) {
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(MensajesEnum.ERROR, e);
		}
	}

	/**
	 * Método encargado de actualizar la Categoria.
	 * 
	 * @param dto del tipo {@link CategoriaRequestDTO}: dto solicitado en la
	 *            actualización.
	 * @return {@link CategoriaResponseDTO}: Categoria del sistema actualizado.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@Override
	public CategoriaResponseDTO update(CategoriaRequestDTO dto) throws ValidateServiceException {
		if (ObjectUtils.isEmpty(dto)) {
			return null;
		}
		if (ObjectUtils.isEmpty(dto.id())) {
			throw new ValidateServiceException(MensajesEnum.ID_REQUIRED);
		}
		return this.save(dto);
	}

	/**
	 * Método encargado de registrar la Categoria.
	 * 
	 * @param dto del tipo {@link CategoriaRequestDTO}: dto solicitado en el
	 *            registro.
	 * @return {@link CategoriaResponseDTO}: Categoria del sistema registrado.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@Override
	public CategoriaResponseDTO insert(CategoriaRequestDTO dto) throws ValidateServiceException {
		return this.save(dto);
	}

	/**
	 * Método encargado de registrar o actualizar la categoria
	 * 
	 * @param dto del tipo {@link CategoriaRequestDTO}: dto solicitado en el
	 *            registro o actualización.
	 * @return {@link CategoriaResponseDTO}: Categoria registrado o actualizado.
	 * @author </br>
	 *         Developer: Arnold Campillo.
	 * @throws
	 */
	private CategoriaResponseDTO save(CategoriaRequestDTO dto) {
		Categoria entity = CategoriaAdapter.requestToEntity.apply(dto);
		validarNombreCategoriaUnico(entity);
		entity.setEstBorrado(false);
		Categoria savedCategoria = repository.save(entity);
		return CategoriaAdapter.entityToResponse.apply(savedCategoria);
	}
	
	private void validarNombreCategoriaUnico(Categoria categoriaEntity) {
		if (ObjectUtils.isEmpty(categoriaEntity.getId())
				&& repository.existsByNombre(categoriaEntity.getNombre())) {
			throw new ValidateServiceException(MensajesEnum.CATEGORIA_EXIST_BY_NAME,categoriaEntity.getNombre());
		}
		
	}

}
