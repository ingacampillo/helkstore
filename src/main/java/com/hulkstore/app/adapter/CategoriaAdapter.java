package com.hulkstore.app.adapter;

import java.util.List;
import java.util.function.Function;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.dto.request.CategoriaRequestDTO;
import com.hulkstore.app.dto.response.CategoriaResponseDTO;
import com.hulkstore.app.entity.Categoria;

/**
 * Adaptador encargado de definir la manera en que se deben convertir objetos
 * entre Entity y DTOs (Y viceversa).
 * 
 * @author </br>
 *         Developer: Arnold Campillo.
 */
public class CategoriaAdapter {

	private CategoriaAdapter() {
	}

	/**
	 * Función encargada de adaptar un objeto DTO Request a su respectivo Entity(de
	 * {@link CategoriaRequestDTO} a {@link Categoria}).
	 * 
	 * @param dto del tipo {@link CategoriaRequestDTO}: Dto Request a convertir a
	 *            Entity.
	 * @return {@link Categoria}: Entity resultado de la conversión.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 */
	public static final Function<CategoriaRequestDTO, Categoria> requestToEntity = (CategoriaRequestDTO dto) -> {
		if (ObjectUtils.isEmpty(dto)) {
			return null;
		}
		return Categoria.builder().id(dto.id()).nombre(dto.nombre()).estBorrado(dto.estBorrado()).build();
	};

	/**
	 * Función encargada de adaptar un objeto Entity a su respectivo DTO Response(de
	 * {@link Categoria} a {@link CategoriaResponseDTO}).
	 * 
	 * @param entity del tipo {@link Categoria}: Entity a convertir a DTO Response.
	 * @return {@link CategoriaResponseDTO}: DTO Response resultado de la conversión.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 */
	public static final Function<Categoria, CategoriaResponseDTO> entityToResponse = (Categoria entity) -> {
		if (ObjectUtils.isEmpty(entity)) {
			return null;
		}

		return CategoriaResponseDTO.builder().id(entity.getId()).codigo(entity.getCodigo()).nombre(entity.getNombre())
				.estBorrado(entity.isEstBorrado()).build();
	};

	/**
	 * Función encargada de adaptar un listado Entity a su respectivo listado DTO
	 * Response(de {@link List}<{@link Categoria}> a
	 * {@link List}<{@link CategoriaResponseDTO}>).
	 * 
	 * @param entityList del tipo {@link List}<{@link Categoria}>: Entity List a
	 *                   convertir a DTO Response List.
	 * @return {@link List}<{@link CategoriaResponseDTO}>: DTO Response List resultado
	 *         de la conversión.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 */

	public static final List<CategoriaResponseDTO> listEntityToResponse(List<Categoria> entityList) {
		if (CollectionUtils.isEmpty(entityList)) {
			return List.of();
		}
		return entityList.stream().map(entityToResponse).toList();
	}

}
