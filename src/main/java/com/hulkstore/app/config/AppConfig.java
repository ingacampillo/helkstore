package com.hulkstore.app.config;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.service.ParametroSistemaService;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityScheme;
/**
 * Clase encargada de contener las configuraciones del sistema.
 * @author </br>
 * Developer:Arnold Campillo	
 */
@Configuration
public class AppConfig {
	
	private ParametroSistemaService parametroSistemaService;
	private static Map<String, String> paramsSystem;

	  /**
     * Contructor encargado de inicializar la clase {@link AppConfig}.
     * @param parametroSistemaService del tipo {@link ParametroSistemaService}: interfaz del servicio.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public AppConfig(ParametroSistemaService parametroSistemaService) {
		super();
		this.parametroSistemaService = parametroSistemaService;
		init(this.parametroSistemaService);

	}
	 /**
     * Método encargado de inicializar los parámetros del sistema.
     * @param parametroSistemaService del tipo {@link ParametroSistemaService}: interfaz del servicio.  
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	private static void init(ParametroSistemaService parametroSistemaService)throws ValidateServiceException, NoDataFoundException, GeneralServiceException  {
		paramsSystem = parametroSistemaService.obtenerParametros();
	}
	
	
	 /**
     * Método encargado de inicializar y configurar el Bean {@link MessageSource}, encargado de realizar la internacionalización de los mensajes.
     * @return MessageSource del tipo {@link MessageSource}: Bean inicializado.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@Bean
    MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        	messageSource.setBasename(ConstantesApp.PATH_LANGUAJE);
        	messageSource.setDefaultEncoding(ConstantesApp.ENCODING_UTF8);
            messageSource.setCacheSeconds(1);
        return messageSource;
    }
	 /**
     * Método encargado de inicializar y configurar el Bean {@link Jackson2ObjectMapperBuilder}, encargado de formatear las fechas del json response en UTC.
     * @return Jackson2ObjectMapperBuilder del tipo {@link Jackson2ObjectMapperBuilder}: Bean inicializado.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@Bean
	Jackson2ObjectMapperBuilder jacksonBuilder() {
	    Jackson2ObjectMapperBuilder b = new Jackson2ObjectMapperBuilder();
	    b.timeZone(TimeZone.getTimeZone(ConstantesApp.TIMEZONE_UTC));
	    b.dateFormat(new StdDateFormat().withColonInTimeZone(true));
	    return b;
	}
	
	 /**
     * Método encargado de inicializar y configurar el Bean {@link OpenAPI}, encargado de la documentacion ApiRest de la aplicación [OpenApi(Swagger)].
     * @return OpenAPI del tipo {@link OpenAPI}: Bean inicializado.  
     * @author </br>
     * Developer: Arnold Campillo.
     */
   @Bean
    OpenAPI customOpenAPI() {    	
        return new OpenAPI()
        		      .components(new Components()
        		        .addSecuritySchemes(ConstantesApp.AUTH_METHOD_BEARER, new SecurityScheme()
        		          .name(ConstantesApp.AUTH_METHOD_BEARER)
        		          .type(SecurityScheme.Type.HTTP)
        		          .scheme(ConstantesApp.AUTH_METHOD_BEARER)
        		          .bearerFormat("JWT"))
        		        .addSecuritySchemes(ConstantesApp.AUTH_METHOD_BASIC, new SecurityScheme()
              		          .name(ConstantesApp.AUTH_METHOD_BASIC)
            		          .type(SecurityScheme.Type.HTTP)
            		          .scheme(ConstantesApp.AUTH_METHOD_BASIC))        		       
        		    		  ) 
                .info(new Info()
                        .title(getParamSystem("APP_OPENAPI_TITLE"))
                        .version(getParamSystem("APP_OPENAPI_VERSION"))
                        .description(getParamSystem("APP_OPENAPI_DESCRIPTION"))
                        .termsOfService(getParamSystem("APP_OPENAPI_TERMS_URL"))
                        .license(new License().name(getParamSystem("APP_OPENAPI_LICENSE_NAME")).url(getParamSystem("APP_OPENAPI_LICENSE_URL"))));
    }
	 /**
    * Método encargado de inicializar y configurar el Bean {@link JavaMailSender}, encargado de servicio de correos de la aplicación.
    * @return JavaMailSender del tipo {@link JavaMailSender}: Bean inicializado.  
    * @author </br>
     * Developer: Arnold Campillo
    */   
	@Bean
	JavaMailSender javaMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(paramsSystem.get("APP_MAIL_HOST"));
		mailSender.setPort(paramsSystem.get("APP_MAIL_PORT") != null ? Integer.parseInt(paramsSystem.get("APP_MAIL_PORT")) : 0);
		mailSender.setUsername(paramsSystem.get("APP_MAIL_USERNAME"));
		mailSender.setPassword(paramsSystem.get("APP_MAIL_PASS"));
		mailSender.setJavaMailProperties(javaMailProperties());

		return mailSender;
	}
	 /**
    * Método encargado de agregar las configuraciones requeridas para el servicio de correos de la aplicación.
    * @return Properties del tipo {@link Properties}: propiedades cargadas.  
    * @author </br>
     * Developer: Arnold Campillo
    */ 
	private Properties javaMailProperties() {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.ssl.enable", "true");
		return properties;
	}
	
	/**
    * Método encargado de inicializar y configurar el Bean {@link PasswordEncoder}, encargado de la encripción de las passwors de la aplicación.
    * @return PasswordEncoder del tipo {@link PasswordEncoder}: Bean inicializado.  
    * @author </br>
     * Developer: Arnold Campillo
    */   
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

	/**
     * Método encargado de obtener los parámetro de la aplicación.
     * @return PasswordEncoder del tipo {@link Map}<{@link String},{@link String}>: Parámetros obtenidos.  
     * @author </br>
     * Developer: Arnold Campillo
     */   
	public static Map<String, String> getParamsSystem(){
		return paramsSystem;
	}
	/**
     * Método encargado de obtener el parámetro del sistema a partir de la key.
     * @param key del tipo {@link String}: Key del parámetro a obtener.  
     * @return String del tipo {@link String}: Parámetro obtenido.  
     * @author </br>
     * Developer: Arnold Campillo
     */   
	public static String getParamSystem(String key){
		return paramsSystem.get(key);
	}
	/**
     * Método encargado de reemplazar el parámetro del sistema a partir de la key.
     * @param key del tipo {@link String}: Key del parámetro a reemplazar.
     * @param value del tipo {@link String}: Value del parámetro a reemplazar.  
    * @author </br>
     * Developer: Arnold Campillo
     */
	public static void replaceParamSystem(String key,String value) {
		paramsSystem.replace(key, value);
	}
	
	/**
     * Método encargado de obtener los parámetros del CorsOrigin de la aplicación.
     * @return List<String> del tipo {@link List}: CorsOrigin obtenidos.  
     * @author </br>
     * Developer: Arnold Campillo.
     */ 
	public static List<String> getCorsOrigin(){
	  return Arrays.asList(getParamSystem("APP_CORS_ORIGIN").split(";"));
	}
	
	/**
     * Método encargado de obtener los parámetros del CorsOrigin de la aplicación.
     * @return String del tipo {@link String}: CorsOrigin obtenidos.  
     * @author </br>
     * Developer: Arnold Campillo
     */ 
	public static String getCorsOriginString(){
	  return  String.join(",", getCorsOrigin()) ;
	}
	/**
     * Método encargado de obtener el parámetro de secret de JWT.
     * @return String del tipo {@link String}: parámetro obtenidos.  
     * @author </br>
     * Developer: Arnold Campillo
     */ 
	public static String getJwtSecret(){
		  return getParamSystem("APP_JWT_SECRET");
	}
	/**
     * Método encargado de obtener el parámetro de expiración del token.
     * @return int del tipo {@link int}: parámetro obtenido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static int getJwtTokenExpiration(){
		  return  Integer.valueOf(getParamSystem("APP_JWT_TOKEN_EXPIRATION"));
	}
	/**
     * Método encargado de obtener el parámetro de expiración del refresh token.
     * @return int del tipo {@link int}: parámetro obtenido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static int getJwtTokenRefreshExpiration(){
		  return  Integer.valueOf(getParamSystem("APP_JWT_TOKEN_REFRESH_EXPIRATION"));
	}
	/**
     * Método encargado de obtener el parámetro de tiempo de sesión inactiva en minutos.
     * @return int del tipo {@link int}: parámetro obtenido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static int getJwtSessionInactiveExpireMinutes(){
		  return  Integer.valueOf(getParamSystem("APP_SESSION_IDLE"));
	}
	/**
     * Método encargado de obtener el parámetro de intentos permitidos para cambio de refresh token.
     * @return int del tipo {@link int}: parámetro obtenido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static int getJwtTokenRefressAttempts(){
		  return  Integer.valueOf(getParamSystem("APP_JWT_TOKEN_REFRESH_ATTEMPTS"));
	}
	/**
     * Método encargado de obtener el parámetro de key de crifrado AES.
     * @return String del tipo {@link String}: parámetro obtenido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static String getkeyAes(){
		  return getParamSystem("KEY_AES_APP");
	}
	/**
     * Método encargado de obtener el parámetro de intentos permitidos para contraseña erronea.
     * @return int del tipo {@link int}: parámetro obtenido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static int getAttemptsPassword(){
		  return Integer.valueOf(getParamSystem("APP_ATTEMPTS_PASSWORD"));
	}
	/**
     * Método encargado de obtener el parámetro de tiempo de expiración del codigo de las peticiones del usuario.
     * @return int del tipo {@link int}: parámetro obtenido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static int getUserPetitionTimeExpiredCode(){
		  return  Integer.valueOf(getParamSystem("APP_USER_PET_CODE_EXPIRED"));
	}
	
	/**
     * Método encargado de obtener el fileSystem de la aplicación.
     * @return String del tipo {@link String}: parámetro obtenido.  
    * @author </br>
     * Developer: Arnold Campillo
     */
	public static String getFileSystemApp(){
		  return getParamSystem("APP_PATH_FILESYSTEM");
	}
	
	
	/**
     * Método encargado de obtener el correo encargado de recepción de correos.
     * @return String del tipo {@link String}: parámetro obtenido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static String getEmailReceiver(){
		  return getParamSystem("MAIL_RECEIVER");
	}
	
	
}
