package com.hulkstore.app.service;

import java.util.List;

import com.hulkstore.app.dto.request.CategoriaRequestDTO;
import com.hulkstore.app.dto.response.CategoriaResponseDTO;
import com.hulkstore.app.dto.response.MarcaResponseDTO;
import com.hulkstore.app.entity.Categoria;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
/**
 * Interfaz Service encargada de realizar las transacciones y operaciones de {@link Categoria}.
 * @author </br>
 * Developer: Arnold Campillo
 */
public interface CategoriaService {
	
	Categoria obtenerCategoriaPorId(Integer categoriaId);
	
	 /**
     * Método encargado de obtener todos las categorias del sistema.
     * @return {@link List}<{@link MarcaResponseDTO}>: listado de las categorias del sistema. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	List<CategoriaResponseDTO> findAll()  throws ValidateServiceException, NoDataFoundException, GeneralServiceException;

	 /**
     * Método encargado de actualizar la Categoria.
     * @param dto del tipo {@link CategoriaRequestDTO}: dto solicitado en la actualización.
     * @return {@link CategoriaResponseDTO}: Categoria del sistema actualizado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	CategoriaResponseDTO update(CategoriaRequestDTO dto)throws ValidateServiceException;
	
	 /**
     * Método encargado de registrar la marca.
     * @param dto del tipo {@link CategoriaRequestDTO}: dto solicitado en el registro.
     * @return {@link CategoriaResponseDTO}: Marca del sistema registrado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	CategoriaResponseDTO insert(CategoriaRequestDTO dto)throws ValidateServiceException;
	
	
}
