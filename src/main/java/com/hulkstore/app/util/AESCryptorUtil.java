package com.hulkstore.app.util;

import java.nio.charset.StandardCharsets;
import java.security.DigestException;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Clase utilitaria encargada de gestionar la criptografia AES.
 * @author </br>
 * Developer: Arnold Campillo
 */
public class AESCryptorUtil {
	
	private AESCryptorUtil() {}
	
	 /**
     * Método encargado de desencriptar texto AES.
     * @param cipherText del tipo {@link String}: Texto a desencriptar.
     * @param secret del tipo {@link String}: Secret requerido para desencriptar.
     * @return String del tipo {@link String}: Texto desencriptado.  
      * @author </br>
 * Developer: Arnold Campillo
     */
	public static String decryptText(String cipherText,String secret){

        String decryptedText=null;
        byte[] cipherData = java.util.Base64.getDecoder().decode(cipherText);
        byte[] saltData = Arrays.copyOfRange(cipherData, 8, 16);
        try{
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            final byte[][] keyAndIV = generateKeyAndIV(32, 16, 1, saltData, secret.getBytes(StandardCharsets.UTF_8), md5);
            if(keyAndIV==null) {
            	return cipherText;
            }
            SecretKeySpec key = new SecretKeySpec(keyAndIV[0], "AES");
            IvParameterSpec iv = new IvParameterSpec(keyAndIV[1]);

            byte[] encrypted = Arrays.copyOfRange(cipherData, 16, cipherData.length);
            Cipher aesCBC = Cipher.getInstance("AES/CBC/PKCS5Padding");
            aesCBC.init(Cipher.DECRYPT_MODE, key, iv);
            byte[] decryptedData = aesCBC.doFinal(encrypted);
            decryptedText = new String(decryptedData, StandardCharsets.UTF_8);
            return decryptedText;
        }
        catch (Exception ex){
            return cipherText;
        }
    }

	 /**
     * Método encargado de generar KeyAnIV byte Array para AES.
     * @param keyLength del tipo {@link int}: Parámetro requerido para la generación.
     * @param ivLength del tipo {@link int}: Parámetro requerido para la generación.
     * @param iterations del tipo {@link int}: Parámetro requerido para la generación
     * @param salt del tipo {@link byte}[]: Parámetro requerido para la generación.
     * @param password del tipo {@link byte}[]: Password en byte array requerido para la generación.
     * @param md del tipo {@link MessageDigest}: Parámetro requerido para la generación.
     * @return Array del tipo {@link byte}[]: Byte Array generado.  
      * @author </br>
 * Developer: Arnold Campillo
     * @throws
     */
	 private static byte[][] generateKeyAndIV(int keyLength, int ivLength, int iterations, byte[] salt, byte[] password, MessageDigest md) {
	        int digestLength = md.getDigestLength();
	        int requiredLength = (keyLength + ivLength + digestLength - 1) / digestLength * digestLength;
	        byte[] generatedData = new byte[requiredLength];
	        int generatedLength = 0;
	        try {
	            md.reset();
	            while (generatedLength < keyLength + ivLength) {
	                if (generatedLength > 0)
	                    md.update(generatedData, generatedLength - digestLength, digestLength);
	                md.update(password);
	                if (salt != null)
	                    md.update(salt, 0, 8);
	                md.digest(generatedData, generatedLength, digestLength);
	                for (int i = 1; i < iterations; i++) {
	                    md.update(generatedData, generatedLength, digestLength);
	                    md.digest(generatedData, generatedLength, digestLength);
	                }
	                generatedLength += digestLength;
	            }

	            byte[][] result = new byte[2][];
	            result[0] = Arrays.copyOfRange(generatedData, 0, keyLength);
	            if (ivLength > 0) {
	            	result[1] = Arrays.copyOfRange(generatedData, keyLength, keyLength + ivLength);
	            }
	            return result;

	        } catch (DigestException e) {
	           return null;
	        } finally {
	            Arrays.fill(generatedData, (byte)0);
	        }
	    }
}
