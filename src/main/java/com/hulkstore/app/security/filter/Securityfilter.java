package com.hulkstore.app.security.filter;

import java.io.IOException;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.hulkstore.app.config.AppConfig;
import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.AuthenticationsException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.service.JwtServices;
import com.hulkstore.app.security.service.SecurityService;
import com.hulkstore.app.util.ResponseUtil;
import com.hulkstore.app.util.WebUtil;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * Clase de seguridad la cual es un filtro de request encargada validar las condiciones de seguridad en las peticiones.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class Securityfilter extends OncePerRequestFilter {

	private final JwtServices jwtServices;
	private final SecurityService securityService;
	private final WebUtil webUtil;
	private final ResponseUtil responseUtil;

	 /**
    * Método encargado de inteceptar y validar las condiciones de seguridad de los request.
    * 
    * @param request del tipo {@link HttpServletRequest}: Request interceptado a validar.
    * @param response del tipo {@link HttpServletResponse}: Response a interceptado a validar.
    * @param filterChain del tipo {@link FilterChain}: filterChain a interceptado.
    * @author </br>
    * Developer: Arnold Campillo.
    * @throws
    */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException, ValidateServiceException, AuthenticationsException {
		try {
			
			String requestURI = request.getRequestURI();		
			webUtil.setDataInfoLog(request);
	        if (webUtil.isPublicRoute(requestURI) || webUtil.isPublicRouteSwagger(requestURI) || !webUtil.hasAuthorizationBearer(request)) {
	          if(!requestURI.contains(ConstantesApp.PUBLIC_RESOURCES)) {
	        	  log.info(ConstantesApp.END_POINT + requestURI);
	          }
	        	
	            filterChain.doFilter(request, response);
	            return;
	        }

			String token = webUtil.getToken(request);
			String userToken=webUtil.isApiRefreshToken(requestURI)?jwtServices.getUserNameFromRefreshToken(token):jwtServices.getUserNameFromToken(token);
			securityService.validateTokenUser(userToken,token);
			if (!webUtil.isApiRefreshToken(requestURI)&&StringUtils.hasText(token) && !jwtServices.validateAccessToken(token)) {				
				securityService.loadUserByUsername(userToken);
				log.info(ConstantesApp.END_POINT+ requestURI);
				filterChain.doFilter(request, response);
				return;
			}

			securityService.validateTokenUser(userToken,token);
			if (webUtil.isApiRefreshToken(requestURI) && StringUtils.hasText(token) && !jwtServices.validateRefreshToken(token)) {
				log.info(ConstantesApp.END_POINT+ requestURI);
				securityService.loadUserByUsername(userToken);
				filterChain.doFilter(request, response);
				return;
			}
			
			String username =securityService.setAuthenticationContext(token, request);			
			webUtil.setDataInfoLog(username,request);
			log.info(ConstantesApp.END_POINT+ requestURI);

		}catch (Exception e) {
			if(e instanceof ValidateServiceException ex) {
				this.responseUtil.buildErrorResponse(response,request, HttpServletResponse.SC_UNAUTHORIZED,ex.getError(),ex,ex.getParamMessage());
			}else if(e instanceof AuthenticationsException ex) {
				this.responseUtil.buildErrorResponse(response,request, HttpServletResponse.SC_UNAUTHORIZED,ex.getError(),ex,ex.getParamMessage());
			}else{
				this.responseUtil.buildErrorResponse(response,request, HttpServletResponse.SC_UNAUTHORIZED, MensajesEnum.ERROR,e);			    
			}	
			return;
		}
	    response.setCharacterEncoding(ConstantesApp.ENCODING_UTF8);
        response.setHeader("Access-Control-Allow-Origin", AppConfig.getCorsOriginString()); // Ajusta según tu configuración
        response.setHeader("Access-Control-Allow-Methods",ConstantesApp.ALLOWED_HEADERS_STRING);
        response.setHeader("Access-Control-Allow-Headers", ConstantesApp.FULL_HEADERS_STRING);
        response.setHeader("Access-Control-Allow-Credentials", "true");
		filterChain.doFilter(request, response);
	}

}
