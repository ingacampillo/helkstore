package com.hulkstore.app.entity;

import java.io.Serializable;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Entity que gestiona la información acerca de los parametros del sistema.
 * @author </br>
 * Developer: Arnold campillo
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "parametro_sistema")
@EqualsAndHashCode(callSuper = false)
@ToString
public class ParametroSistema implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Integer id;
	
	@Basic(optional = false)
    @Column(name = "codigo", nullable = false, length = 50)
	private String codigo;
	
	@Basic(optional = false)
    @Column(name = "valor" , nullable = false, length = 150)
	private String valor;
	
    @Column(name = "descripcion", length = 300)
	private String descripcion;
	
    @Column(name = "est_borrado", length = 1, columnDefinition = "boolean default false")
	@Builder.Default
	private boolean estBorrado = false;

    @Column(name = "permite_update", length = 1, columnDefinition = "boolean default true")
	@Builder.Default
	private boolean permiteUpdate = false;
    
}
