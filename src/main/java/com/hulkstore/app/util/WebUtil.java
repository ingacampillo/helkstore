package com.hulkstore.app.util;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.context.MessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.security.dto.response.AuthUserResponseDTO;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
/**
 * Clase utilitaria encargada de gestionar metodos comúnes o utilitarios de la aplicación[Web].
 * @author </br>
     * Developer: Arnold Campillo
 */
@Service
@RequiredArgsConstructor
public class WebUtil {
	
	private final MessageSource messageSource;

	
	/**
     * Método encargado de obtener el request de la petición {@link HttpServletRequest}.
     * @return request del tipo {@link HttpServletRequest}: Request obtenido de la petición.
    * @author </br>
     * Developer: Arnold Campillo
     */
	public HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	}

	/**
     * Método encargado de cargar los headers del request de la petición {@link HttpServletRequest}.
     * @param headers del tipo {@link Map}<{@link String},{@link String}>: Mapa con los headers del request.
     * @param key del tipo {@link String}: Key a asignar al mapa.
     * @param value del tipo {@link String}: Valor a asignar al mapa.
     * @author </br>
     * Developer: Arnold Campillo
     */
	private void loadHeaderRequest(Map<String, String> headers, String key, String value) {
		headers.put(key.toUpperCase(), value);
	}

	/**
     * Método encargado de obtener los headers del request de la petición {@link HttpServletRequest}.
     * @param request del tipo {@link HttpServletRequest}: Request para obtener información.
     * @return headers del tipo {@link Map}<{@link String},{@link String}>: Request obtenido de la petición.
    * @author </br>
     * Developer: Arnold Campillo
     */
	public Map<String, String> getHeadersRequest(HttpServletRequest request) {
		Map<String, String> headers = new HashMap<>();
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = headerNames.nextElement();
			this.loadHeaderRequest(headers, key, request.getHeader(key));
		}
		return headers;
	}
	/**
     * Método encargado de obtener los headers del request de la petición {@link WebRequest}.
     * @param request del tipo {@link WebRequest}: Request para obtener información.
     * @return headers del tipo {@link Map}<{@link String},{@link String}>: Request obtenido de la petición.
    * @author </br>
     * Developer: Arnold Campillo
     */
	public Map<String, String> getHeadersRequest(WebRequest request) {
		Map<String, String> headers = new HashMap<>();
		Iterator<String> headerNames = request.getHeaderNames();
		while (headerNames.hasNext()) {
			String key = headerNames.next();
			this.loadHeaderRequest(headers, key, request.getHeader(key));
		}
		return headers;
	}

	/**
     * Método encargado de obtener los headers del request de la petición {@link HttpServletRequest}.
     * @return headers del tipo {@link Map}<{@link String},{@link String}>: Request obtenido de la petición.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public Map<String, String> getHeadersRequest() {
		Map<String, String> headers = new HashMap<>();
		HttpServletRequest request = this.getRequest();
		Enumeration<?> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			this.loadHeaderRequest(headers, key, request.getHeader(key));
		}
		return headers;
	}

	/**
     * Método encargado de obtener el valor del header a partir del key.
     * @param headers del tipo {@link Map}<{@link String},{@link String}>: headers obtenidos de la petición.
     * @param key del tipo {@link String}: Key a buscar en los headers obtenidos de la petición.
     * @param valueDefault del tipo {@link String}: Valor default a asignar al header si no se encuentra de los headers enviados.
     * @return header del tipo {@link String}: Header obtenido.
     * @author </br>
     * Developer: Arnold Campillo
     */
	private String getDataHeader(Map<String, String> headers, String key, String valueDefault) {
		String data = headers.get(key);
		return ObjectUtils.isEmpty(data) ? valueDefault : data;
	}

	/**
     * Método encargado de asignar información al log.
     * @param user del tipo {@link String}: Usuario a asignar en el log.
     * @param traceCode del tipo {@link String}: Tracecode a asignar en el log.
     * @param ip del tipo {@link String}: Ip a asignar en el log.
     * @author </br>
     * Developer: Arnold Campillo
     */	
	private void setDataInfoLog(String user, String traceCode, String ip) {
		AppUtil.addValueInLog(user, traceCode, ip);
	}

	/**
     * Método encargado de asignar información al log partiendo del request de la petición {@link HttpServletRequest}.
     * @param request del tipo {@link HttpServletRequest}: Request para obtener información.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public void setDataInfoLog(HttpServletRequest request) {
		this.setDataInfoLog(this.getUsername(), this.getTraceCode(request), this.getIpClient(request));
	}
	/**
     * Método encargado de asignar información al log partiendo del request de la petición {@link WebRequest}.
     * @param request del tipo {@link WebRequest}: Request para obtener información.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public void setDataInfoLog(WebRequest request) {
		this.setDataInfoLog(this.getUsername(), this.getTraceCode(request), this.getIpClient(request));
	}
	/**
     * Método encargado de asignar información al log partiendo del request de la petición {@link HttpServletRequest}.
    * @author </br>
     * Developer: Arnold Campillo
     */
	public void setDataInfoLog() {
		this.setDataInfoLog(this.getUsername(), this.getTraceCode(getRequest()), this.getIpClient(getRequest()));
	}

	
	/**
     * Método encargado de asignar información al log partiendo del request de la petición {@link HttpServletRequest} y de los parametros enviados.
     * @param username del tipo {@link String}: Usuario a asignar en el log.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public void setDataInfoLog(String username) {
		this.setDataInfoLog(username, this.getTraceCode(getRequest()), this.getIpClient(getRequest()));
	}

	/**
     * Método encargado de asignar información al log partiendo del request de la petición {@link HttpServletRequest} y de los parametros enviados.
     * @param username del tipo {@link String}: Usuario a asignar en el log.
     * @param request del tipo {@link HttpServletRequest}: Request para obtener información.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public void setDataInfoLog(String username, HttpServletRequest request) {
		this.setDataInfoLog(username, this.getTraceCode(request), this.getIpClient(request));
	}

	/**
     * Método encargado de obtener el idioma[lang] del request de la petición {@link WebRequest}.
     * @param request del tipo {@link WebRequest}: Request para obtener información.
     * @return lang del tipo {@link String}: Lang obtenido del Request de la petición.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public String getLang(WebRequest request) {
		Map<String, String> headers = this.getHeadersRequest(request);
		return this.getDataHeader(headers, ConstantesApp.HEADER_ACCEPT_LENGUAJE.toUpperCase(), ConstantesApp.LOCALE_DEFAULT);
	}

	/**
     * Método encargado de obtener el idioma[lang] del request de la petición {@link HttpServletRequest}.
     * @param request del tipo {@link HttpServletRequest}: Request para obtener información.
     * @return lang del tipo {@link String}: Lang obtenido del Request de la petición.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public String getLang(HttpServletRequest request) {
		Map<String, String> headers = this.getHeadersRequest(request);
		return this.getDataHeader(headers, ConstantesApp.HEADER_ACCEPT_LENGUAJE.toUpperCase(), ConstantesApp.LOCALE_DEFAULT);
	}
	
	/**
     * Método encargado de obtener el token(token, refreshToken) de autenticación del request de la petición {@link HttpServletRequest}.
     * @param request del tipo {@link HttpServletRequest}: Request para obtener información.
     * @return headers del tipo {@link Map}<{@link String},{@link String}>: Request obtenido de la petición.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public String getToken(HttpServletRequest request) {
		String header = request.getHeader(ConstantesApp.AUTHORIZATION);
		return header.split(" ")[1].trim();
	}
	/**
     * Método encargado de obtener el token(token, refreshToken) de autenticación del request de la petición {@link HttpServletRequest}.
     * @return token del tipo {@link String}: token obtenido del Request de la petición.
    * @author </br>
     * Developer: Arnold Campillo
     */
	public String getToken() {
		String header = getRequest().getHeader(ConstantesApp.AUTHORIZATION);
		return header.split(" ")[1].trim();
	}
	/**
     * Método encargado de obtener el username del usuario del contexto de autenticacion {@link SecurityContextHolder}.
     * @return username del tipo {@link String}: Username del usuario obtenido.
    * @author </br>
     * Developer: Arnold Campillo
     */
	public String getUsername() {
		AuthUserResponseDTO obj = this.getUser();
		return ObjectUtils.isEmpty(obj) ? ConstantesApp.UNDEFINED : obj.username();
	}
	/**
     * Método encargado de obtener los idiomas[langs] configurados en la aplicación.
     * @return langs del tipo {@link List}<{@link String}>: Langs configurados.
    * @author </br>
     * Developer: Arnold Campillo
     */
	public List<String> langsAvailables() {
		 List<Locale> locales=localesAvailables();
	     return locales.stream().map(Locale::toLanguageTag).toList();
	}
	/**
     * Método encargado de obtener los locales configurados en la aplicación.
     * @return langs del tipo {@link List}<{@link Locale}>: Locales configurados.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public List<Locale> localesAvailables(){
		List<Locale> locales;
		try {
			ClassPathResource resource = new ClassPathResource("lang");
			File directory  = new File(resource.getURI());
	        File[] files = directory.listFiles();	
	        locales= Arrays.stream(files)
	                .filter(file -> file.isFile() && file.getName().startsWith("messages_") && file.getName().endsWith(".properties"))
	                .map(file -> {
	                    String fileName = file.getName();
	                    String localeString = fileName.substring(fileName.indexOf("_") + 1, fileName.indexOf(".properties"));
	                    String[] localeParts = localeString.split("-");
	                    if(localeParts.length==1) {
	                    	return new Locale(localeParts[0]);	
	                    }else {
	                    	return new Locale(localeParts[0], localeParts[1]);
	                    }
	                    
	                })
	                .collect(Collectors.toList());
	        locales.add(new Locale("es","CO"));
	        locales.add(new Locale("es","ES"));
		} catch (IOException e) {
			locales=List.of();
		}
		return locales;

	}
	
	/**
     * Método encargado de obtener el usuario del contexto de autenticacion {@link SecurityContextHolder}.
     * @return dto del tipo {@link AuthUserResponseDTO}: Información del usuario obtenido.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public AuthUserResponseDTO getUser() {		
		if(ObjectUtils.isEmpty(SecurityContextHolder.getContext().getAuthentication())) {
			return null;
		}
		if(!SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
			return null;
		}
		
		if(ObjectUtils.isEmpty(SecurityContextHolder.getContext().getAuthentication().getPrincipal())) {
			return null;
		}
		if(!(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof AuthUserResponseDTO)) {
			return null;
		}
		return (AuthUserResponseDTO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	/**
     * Método encargado de obtener el idioma[lang] del request de la petición {@link HttpServletRequest}.
     * @return lang del tipo {@link String}: Lang obtenido del Request de la petición.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public String getLang() {
		Map<String, String> headers = this.getHeadersRequest();
		return this.getDataHeader(headers, ConstantesApp.HEADER_ACCEPT_LENGUAJE.toUpperCase(), ConstantesApp.LOCALE_DEFAULT);
	}
	/**
     * Método encargado de obtener el tracecode del request de la petición {@link WebRequest}.
     * @param request del tipo {@link WebRequest}: Request para obtener información.
     * @return traceCode del tipo {@link String}: TraceCode obtenido de la petición.
    * @author </br>
     * Developer: Arnold Campillo
     */
	public String getTraceCode(WebRequest request) {
		Map<String, String> headers = this.getHeadersRequest(request);
		return this.getDataHeader(headers, ConstantesApp.HEADER_TRACE_CODE.toUpperCase(), UUID.randomUUID().toString());
	}

	/**
     * Método encargado de obtener el tracecode del request de la petición {@link HttpServletRequest}.
     * @param request del tipo {@link HttpServletRequest}: Request para obtener información.
     * @return traceCode del tipo {@link String}: TraceCode obtenido de la petición.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public String getTraceCode(HttpServletRequest request) {
		Map<String, String> headers = this.getHeadersRequest(request);
		return this.getDataHeader(headers, ConstantesApp.HEADER_TRACE_CODE.toUpperCase(), UUID.randomUUID().toString());
	}
	/**
     * Método encargado de obtener la ip del request de la petición {@link WebRequest}.
     * @param request del tipo {@link WebRequest}: Request para obtener información.
     * @return ip del tipo {@link String}: Ip obtenido del request.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public String getIpClient(WebRequest request) {
		HttpServletRequest servletRequest = ((ServletWebRequest) request).getNativeRequest(HttpServletRequest.class);
		return this.getIpClient(servletRequest);
	}
	/**
     * Método encargado de obtener la ip del request de la petición {@link HttpServletRequest}.
     * @param request del tipo {@link HttpServletRequest}: Request para obtener información.
     * @return ip del tipo {@link String}: Ip obtenido del request.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public String getIpClient(HttpServletRequest request) {
		return request.getRemoteAddr();
	}

	 /**
     * Método encargado de traducir un texto a partir de su mensaje{@link MensajesEnum}.
     * @param message del tipo {@link MensajesEnum}: Mensaje a traducir.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @param locale del tipo {@link String}: Locale o idioma a traducir el texto.
     * @return traslate del tipo {@link String}: texto traducido.  
    * @author </br>
     * Developer: Arnold Campillo
     */
	public String translate(MensajesEnum message, Object[] paramMessage, String locale) {
		return this.messageSource.getMessage(message.getMessage(), paramMessage,
				new Locale(ObjectUtils.isEmpty(locale) ? ConstantesApp.LOCALE_DEFAULT : locale));
	}
	 /**
     * Método encargado de traducir un texto a partir de su mensaje{@link MensajesEnum}, con el idioma del request.
     * @param message del tipo {@link MensajesEnum}: Mensaje a traducir.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @return traslate del tipo {@link String}: texto traducido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public String translate(MensajesEnum message, Object[] paramMessage) {
		return this.translate(message, paramMessage, this.getLang());
	}

	 /**
     * Método encargado de traducir un texto a partir de su mensaje{@link MensajesEnum}, con el idioma del request.
     * @param message del tipo {@link MensajesEnum}: Mensaje a traducir.
     * @return traslate del tipo {@link String}: texto traducido.  
    * @author </br>
     * Developer: Arnold Campillo
     */
	public String translate(MensajesEnum message) {
		return this.translate(message, null, this.getLang());
	}
	 /**
     * Método encargado de traducir un texto a partir de su mensaje{@link MensajesEnum}.
     * @param message del tipo {@link MensajesEnum}: Mensaje a traducir.
     * @param locale del tipo {@link String}: Locale o idioma a traducir el texto.
     * @return traslate del tipo {@link String}: texto traducido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public String translate(MensajesEnum message, String locale) {
		return this.translate(message, null, locale);
	}

	 /**
     * Método encargado de validar si la ruta del request es una ruta publica.
     * @param requestURI del tipo {@link String}: Request Url a validar.
     * @return result del tipo {@link boolean}: resultado de la validación.  
    * @author </br>
     * Developer: Arnold Campillo
     */
	public boolean isPublicRoute(String requestURI) {
		return ConstantesApp.PUBLIC_ROUTES.stream().anyMatch(requestURI::contains);
	}
	 /**
     * Método encargado de validar si la ruta del request es una ruta de swagger(OpenApi).
     * @param requestURI del tipo {@link String}: Request Url a validar.
     * @return result del tipo {@link boolean}: resultado de la validación.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public boolean isPublicRouteSwagger(String requestURI) {
		return ConstantesApp.PUBLIC_ROUTES_SWAGGER.stream().anyMatch(requestURI::contains);
	}
	
	 /**
     * Método encargado de validar si la ruta del request es la ruta o endpoint de refresh token.
     * @param requestURI del tipo {@link String}: Request Url a validar.
     * @return result del tipo {@link boolean}: resultado de la validación.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public boolean isApiRefreshToken(String requestURI) {
		return ConstantesApp.ROUTE_REFRESH_TOKEN.equals(requestURI);
	}

	 /**
     * Método encargado de validar si el request tiene el encabezado de autenticacion Bearer.
     * @param request del tipo {@link HttpServletRequest}: Request para obtener información.
     * @return result del tipo {@link boolean}: resultado de la validación.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public boolean hasAuthorizationBearer(HttpServletRequest request) {
		String header = request.getHeader(ConstantesApp.AUTHORIZATION);
		return !(ObjectUtils.isEmpty(header) || !header.startsWith("Bearer"));
		
	}	

}
