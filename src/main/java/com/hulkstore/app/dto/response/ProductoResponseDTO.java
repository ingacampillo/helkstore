package com.hulkstore.app.dto.response;

import java.math.BigDecimal;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

/**
 * DTO response que contiene la información acerca de las productos 
 * @author </br>
 * Developer: Arnold campillo
 */
@Builder
@Schema(description = "DTO response que contiene la información acerca de productos.")
public record ProductoResponseDTO (

	@Schema(description = "Identificador del producto.",example = "16551615615611")
	Integer id,
	@Schema(description = "Código de la producto.",example = "CODE_052")
	String codigo,
	@Schema(description = "Nombre del producto.",example = "PROD_UNICA")
	String nombre,
	@Schema(description = "Descripción del producto.",example = "DESCRIPCIÓN_PRODUCTO")
	String descripcion,
	@Schema(description = "Referencia del producto.",example = "REF_UNICA")
	String referencia,
	@Schema(description = "Marca relacionado.",example = "{nombre:sas}")
	MarcaResponseDTO marca,
	@Schema(description = "Categoria relacionado.",example = "{nombre:sas}")
	CategoriaResponseDTO categoria,	
	@Schema(description = "Cantidad del producto.",example = "1")
	Integer cantidad,
	@Schema(description = "PrecioVenta del producto.",example = "50.000")
	BigDecimal precioVenta,
	@Schema(description = "Indica si el producto esta activo.",example = "true")
	Boolean estBorrado
	

){}
