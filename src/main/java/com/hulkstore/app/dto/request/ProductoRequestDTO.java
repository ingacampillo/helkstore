package com.hulkstore.app.dto.request;

import java.math.BigDecimal;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;

/**
 * DTO request que contiene la información solicitada de los productos
 * 
 * @author </br>
 *         Developer: Arnold Campillo.
 */
@Builder
@Schema(description = "DTO request que contiene la información solicitada de productos.")
public record ProductoRequestDTO(
		
		@Schema(description = "Identificador de la producto.", example = "16551615615611") 
		Integer id,
		@Schema(description = "Código del producto.",example = "CODE_052")
		@NotNull(message = "{app.message.valid.not.null}")
		@NotBlank(message = "{app.message.valid.not.black}")
		@Size(message =  "{app.message.valid.size.min.max}", min = 1, max = 50)
		String codigo,
		@Schema(description = "Nombre des producto.", example = "NOMBRE_001") 
		@NotNull(message = "{app.message.valid.not.null}")
		@NotBlank(message = "{app.message.valid.not.black}") 
		@Size(message = "{app.message.valid.size.min.max}", min = 1, max = 200) 
		String nombre,
		@Schema(description = "Descripción del producto.",example = "DESCRIBE EL PRUDUCTO")
		@Size(message =  "{app.message.valid.size.max}", max = 300)
		String descripcion,
		@Schema(description = "Indica si el producto esta activo.",example = "true")
		@NotNull(message = "{app.message.valid.not.null}")
		Boolean estBorrado,
		@Schema(description = "PrecioVenta del producto.",example = "50000")
		@NotNull(message = "{app.message.valid.not.null}")
		@DecimalMin(value = "0", message = "El campo 'precioVenta' debe ser un número no negativo")
		BigDecimal precioVenta,
		@Schema(description = "Cantidad del producto.",example = "10")
		@NotNull(message = "{app.message.valid.not.null}")
		@Size(message =  "{app.message.valid.size.min.max}", min = 0)
		@Min(value = 0, message = "El campo 'cantidad' debe ser un número positivo mayor o igual a cero")
		String cantidad,
		@Schema(description = "Referencia del producto.", example = "REF_001") 
		@NotNull(message = "{app.message.valid.not.null}")
		@NotBlank(message = "{app.message.valid.not.black}") 
		@Size(message = "{app.message.valid.size.min.max}", min = 1, max = 200) 
		String referencia,
		@Schema(description = "Identificador de la categoria.", example = "1") 
		@NotNull(message = "El campo 'categoriaId' no puede ser nulo")
		Integer categoriaId,
		@Schema(description = "Identificador de la marca.", example = "1")
		@NotNull(message = "El campo 'marcaId' no puede ser nulo")
	    Integer marcaId
		

) {}