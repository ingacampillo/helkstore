package com.hulkstore.app.dto.response;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hulkstore.app.enums.TipoTransaccionInventarioEnum;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

/**
 * DTO response que contiene la información acerca de las inventarios de los productos 
 * @author </br>
 * Developer: Arnold campillo
 */
@Builder
@Schema(description = "DTO response que contiene la información acerca de inventario de productos.")
public record OrdenProductoResponseDTO (

	@Schema(description = "Identificador del inventario.",example = "16551615615611")
	Integer id,
	@Schema(description = "Producto relacionado.",example = "{nombre:sas}")
	ProductoResponseDTO producto,	
	@Schema(description = "Cantidad del inventario.",example = "1")
	Integer cantidad,
	@Schema(description = "Tipo de referencia.",example = "COMPRA")
	TipoTransaccionInventarioEnum tipoTransaccion,
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
	@Schema(description = "Fecha del inventario.",example = "0000-00-00 00:00")
	LocalDateTime fecha

){}
