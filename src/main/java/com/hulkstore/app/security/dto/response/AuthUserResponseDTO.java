package com.hulkstore.app.security.dto.response;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

/**
 * DTO response que contiene la información acerca de la autenticación del usuario.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Builder
@Schema(description = "DTO response que contiene la información acerca de la autenticación del usuario")
public record AuthUserResponseDTO(
		@Schema(hidden = true)
		@JsonIgnore
		Long id,
		@Schema(description = "Username del usuario.",example = "USER11")
		String username,
	    @Schema(description = "Usercode del usuario.",example = "USER11")
	    String usercode,
		@Schema(description = "Token generado en la autenticación.",example = "54d54sd54a64d65a4sda564ds")
		String token,
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
		@Schema(description = "Fecha de expiración del token generado en la autenticación.",example = "0000-00-00 00:00")
		LocalDateTime tokenExpire,
		@Schema(description = "Token refresh generado en la autenticación.",example = "54d54sd54a64d65a4sda564ds")
		String refreshToken,
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
		@Schema(description = "Fecha de expiración del token refresh generado en la autenticación.",example = "0000-00-00 00:00")
		LocalDateTime refreshTokenExpire,
		@Schema(description = "Especifica cuantos minutos durará la sesión por inactividad.",example = "5")
		Integer sessionInactiveExpireMinutes,
		@Schema(description = "Roles los cuales tiene asignados el usuario.",example = "[ADMIN,USER]")
		List<String> roles,
		@Schema(description = "Indica si el usuario debe cambiar la contraseña.",example = "true")
	    boolean forceChangePass,
	    @Schema(description = "Indica si el usuario esta activo.",example = "true")
	    boolean enabled,
	    @Schema(description = "Indica si el usuario esta bloqueado por superar el limite de intentos al digitar la password.",example = "false")
	    boolean bloquedByInvalidPass

) {
	
}
