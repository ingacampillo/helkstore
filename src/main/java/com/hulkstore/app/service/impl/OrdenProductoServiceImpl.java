package com.hulkstore.app.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.adapter.OrdenProductoAdapter;
import com.hulkstore.app.dto.request.OrdenProductoRequestDTO;
import com.hulkstore.app.dto.response.OrdenProductoResponseDTO;
import com.hulkstore.app.entity.OrdenProducto;
import com.hulkstore.app.entity.Producto;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.enums.TipoTransaccionInventarioEnum;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.repository.OrdenProductoRepository;
import com.hulkstore.app.repository.ProductoRepository;
import com.hulkstore.app.service.OrdenProductoService;
import com.hulkstore.app.service.ProductoService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * Service encargado de realizar las transacciones y operaciones de {@link OrdenProducto}. Implementación {@link OrdenProductoService}.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class OrdenProductoServiceImpl implements OrdenProductoService {
	
	private final OrdenProductoRepository repository;
	private final ProductoRepository productoRepository;
	private final ProductoService productoService; 
	
	
	/**
	 * Método encargado de obtener todos los InventarioProductos del sistema.
	 * 
	 * @return {@link List}<{@link OrdenProductoResponseDTO}>: listado de las InventarioProductos del
	 *         sistema.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	@Override
	public List<OrdenProductoResponseDTO> findAll()
			throws ValidateServiceException, NoDataFoundException, GeneralServiceException {
		try {
			List<OrdenProducto> inventarioProductoList = this.repository.findAll();
			return OrdenProductoAdapter.listEntityToResponse(inventarioProductoList);
		} catch (ValidateServiceException | NoDataFoundException e) {
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(MensajesEnum.ERROR, e);
		}
	}

	
	 /**
     * Método encargado de actualizar la InventarioProducto.
     * @param dto del tipo {@link OrdenProductoRequestDTO}: dto solicitado en la actualización.
     * @return {@link OrdenProductoResponseDTO}: InventarioProducto del sistema actualizado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	@Override
	public OrdenProductoResponseDTO update(OrdenProductoRequestDTO dto) throws ValidateServiceException {
		if(ObjectUtils.isEmpty(dto)) {
			return null;
		}
		if(ObjectUtils.isEmpty(dto.id())) {
			throw new ValidateServiceException(MensajesEnum.ID_REQUIRED);
		}		
		return this.save(dto);
	}
	
	 /**
    * Método encargado de registrar la InventarioProducto.
    * @param dto del tipo {@link OrdenProductoRequestDTO}: dto solicitado en el registro.
    * @return {@link OrdenProductoResponseDTO}: InventarioProducto del sistema registrado. 
    * @author </br>
    * Developer: Arnold Campillo
    * @throws
    */
	@Override
	public OrdenProductoResponseDTO insert(OrdenProductoRequestDTO dto) throws ValidateServiceException {
		return this.save(dto);
	}
	
	
	
	/**
     * Método encargado de registrar o actualizar la InventarioProducto
     * @param dto del tipo {@link OrdenProductoRequestDTO}: dto solicitado en el registro o actualización.
     * @return {@link OrdenProductoResponseDTO}: InventarioProducto  registrado o actualizado. 
     * @author </br>
     * Developer: Arnold Campillo.
     * @throws
     */
	private OrdenProductoResponseDTO save(OrdenProductoRequestDTO dto) {
	    OrdenProducto entity = OrdenProductoAdapter.requestToEntity.apply(dto);
	    Producto producto = productoService.obtenerProductoPorId(dto.productoId());
	    TipoTransaccionInventarioEnum tipoTransaccion = dto.tipoTransaccion();
	    int cantidad = entity.getCantidad();

	    switch (tipoTransaccion) {
	        case VENTA:
	            if (cantidad > producto.getCantidad()) {
	                throw new NoDataFoundException(MensajesEnum.OUT_STOCK_PRODUCT, producto.getNombre());
	            }
	            producto.setCantidad(producto.getCantidad() - cantidad);
	            break;
	        case COMPRA:
	            producto.setCantidad(producto.getCantidad() + cantidad);
	            break;
	        default:
	            throw new IllegalArgumentException("Tipo de transacción no válido: " + tipoTransaccion);
	    }

	    productoRepository.save(producto);
	    entity.setProducto(producto);
	    OrdenProducto savedInventarioProducto = repository.save(entity);
	    return OrdenProductoAdapter.entityToResponse.apply(savedInventarioProducto);
	}
	
	
	

	

}
