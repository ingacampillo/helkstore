package com.hulkstore.app.entity;

import java.io.Serializable;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "marca")
@EqualsAndHashCode(callSuper = false)
@ToString
public class Marca implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "codigo", nullable = false, unique = true, updatable = false)
	@Builder.Default
	private String codigo = UUID.randomUUID().toString();

	@Column(name = "enabled", length = 1, columnDefinition = "boolean default true")
	@Builder.Default
	private boolean enabled = true;

	@Column(name = "est_borrado", length = 1, columnDefinition = "boolean default false")
	@Builder.Default
	private boolean estBorrado = false;

	@Column(name = "nombre", nullable = false, length = 150, unique = true)
	private String nombre;

}
