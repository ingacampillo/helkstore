package com.hulkstore.app.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
/**
 * DTO response que contiene la información acerca de los parametros del sistema.
 * @author </br>
 * Developer: Arnold campillo
 */
@Builder
@Schema(description = "DTO response que contiene la información acerca de parametros del sistema.")
public record ParametroSistemaResponseDTO(
		@Schema(description = "Identificador del parametro.",example = "16551615615611")
		Integer id,
		@Schema(description = "Código del parametro.",example = "CODE_052")
		String codigo,
		@Schema(description = "Valor del parametro.",example = "VALOR_50")
		String valor,
		@Schema(description = "Descripción del parametro.",example = "DESCRIBE EL PARAMETRO")
		String descripcion,
		@Schema(description = "Indica si el parametro esta activo.",example = "true")
		Boolean estBorrado,
		@Schema(description = "Indica si el parametro esta permite ser actualizado.",example = "true")
		Boolean permiteUpdate
		
		
) {}
