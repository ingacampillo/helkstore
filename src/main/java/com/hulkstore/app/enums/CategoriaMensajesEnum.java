package com.hulkstore.app.enums;

/**
 * {@link Enum} que contiene las categorias disponibles para los mensajes {@link MensajesEnum}.
 * @author </br>
 * Developer: Arnold Campillo
 */
public enum CategoriaMensajesEnum {
	ERROR, INFO ,WARN
}
