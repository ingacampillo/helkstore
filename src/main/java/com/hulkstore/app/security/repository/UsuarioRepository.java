package com.hulkstore.app.security.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hulkstore.app.security.entity.Usuario;

/**
 * Interfaz Repository encargada de realizar las transacciones hacia la base de datos del entity {@link Usuario}.
 * @author </br>
    * Developer: Arnold Campillo.
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
	 /**
	 * Método encargado de obtener el {@link Usuario} a partir del username.
	 * @param username del tipo {@link String}: Parámetro requerido para la transacción.  
	 * @return {@link Optional}<{@link Usuario}>: Entity resultado de la transacción. 
	 * @author </br>
    * Developer: Arnold Campillo.
	 */
	Optional<Usuario> findByUsername(String username);
	 /**
	 * Método encargado de obtener el {@link Usuario} a partir del username y que no este eliminado.
	 * @param username del tipo {@link String}: Parámetro requerido para la transacción.  
	 * @return {@link Optional}<{@link Usuario}>: Entity resultado de la transacción. 
	 * @author </br>
    * Developer: Arnold Campillo.
	 */
	Optional<Usuario> findByUsernameAndEstBorradoIsFalse(String username);
	
	 /**
	 * Método encargado de obtener el {@link Usuario} a partir del usercode y que no este eliminado.
	 * @param usercode del tipo {@link String}: Parámetro requerido para la transacción.  
	 * @return {@link Optional}<{@link Usuario}>: Entity resultado de la transacción. 
	 * @author </br>
    * Developer: Arnold Campillo.
	 */
	Optional<Usuario> findByUsercodeAndEstBorradoIsFalse(String usercode);
	
	/**
	 * Método encargado de obtener los {@link Usuario} que no este eliminados. 
	 * @return {@link List}<{@link Usuario}>: Listado resultado de la transacción. 
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	List<Usuario> findByEstBorradoIsFalse();

	
}
