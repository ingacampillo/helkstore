package com.hulkstore.app.entity;

import java.io.Serializable;
import java.util.UUID;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "categoria")
@EqualsAndHashCode(callSuper = false)
@ToString
public class Categoria implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Integer id;

	@Column(name = "codigo", nullable = false, unique = true, updatable = false)
	@Builder.Default
	private String codigo = UUID.randomUUID().toString();

	@Column(name = "enabled", length = 1, columnDefinition = "boolean default true")
	@Builder.Default
	private boolean enabled = true;

	@Column(name = "est_borrado", length = 1, columnDefinition = "boolean default false")
	@Builder.Default
	private boolean estBorrado = false;

	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;

	@Column(name = "descripcion", length = 1000)
	private String descripcion;

}
