package com.hulkstore.app.security.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hulkstore.app.config.AppConfig;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.adapter.UsuarioAdapter;
import com.hulkstore.app.security.dto.request.NewUserRequestDTO;
import com.hulkstore.app.security.dto.response.UsuarioResponseDTO;
import com.hulkstore.app.security.entity.Rol;
import com.hulkstore.app.security.entity.Usuario;
import com.hulkstore.app.security.enums.TypeRol;
import com.hulkstore.app.security.repository.RolRepository;
import com.hulkstore.app.security.repository.UsuarioRepository;
import com.hulkstore.app.security.service.UsuarioService;
import com.hulkstore.app.util.AESCryptorUtil;

import lombok.RequiredArgsConstructor;
/**
 * Intefaz Service encargado de realizar las transacciones y operaciones del usuario. Implementación {@link UsuarioService}
  * @author </br>
    * Developer: Arnold Campillo.
 */
@Service
@RequiredArgsConstructor
public class UsuarioServiceImpl implements UsuarioService {
	
	private final UsuarioRepository usuarioRepository;
	private final PasswordEncoder passwordEncoder;
	private final RolRepository rolRepository;
	
	 /**
	 * Método encargado de obtener el usuario a patir del username.
	 * @param username del tipo {@link String}: Username del usuario.
	 * @return user del tipo {@link Usuario}: Entity Usuario obtenido de la consulta.
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private Usuario findUserByUsernameEntity(String username) throws NoDataFoundException {
		return usuarioRepository.findByUsernameAndEstBorradoIsFalse(username).orElseThrow(
				() -> new NoDataFoundException(MensajesEnum.USER_NO_FOUND,username));
		
	}
	 /**
	 * Método encargado de obtener el usuario a patir del usercode.
	 * @param usercode del tipo {@link String}: Usercode del usuario.
	 * @return user del tipo {@link Usuario}: Entity Usuario obtenido de la consulta.
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private Usuario findUserByUsercodeEntity(String usercode) throws NoDataFoundException{
		return usuarioRepository.findByUsercodeAndEstBorradoIsFalse(usercode).orElseThrow(
			() -> new NoDataFoundException(MensajesEnum.USERCODE_NO_FOUND,usercode));

	}
	 /**
	 * Método encargado de obtener el usuario a patir del username.
	 * @param username del tipo {@link String}: Username del usuario.
	 * @return user del tipo {@link UsuarioResponseDTO}: Usuario obtenido de la consulta.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public UsuarioResponseDTO findUserByUsername(String username) throws NoDataFoundException{
		Usuario user=findUserByUsernameEntity(username);			
		return UsuarioAdapter.entityToResponse.apply(user);
	}
	
	 /**
	 * Método encargado de obtener el usuario a patir del usercode.
	 * @param usercode del tipo {@link String}: Usercode del usuario.
	 * @return user del tipo {@link UsuarioResponseDTO}: Usuario obtenido de la consulta.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public UsuarioResponseDTO findUserByUsercode(String usercode) throws NoDataFoundException{
		Usuario user=findUserByUsercodeEntity(usercode);			
		return UsuarioAdapter.entityToResponse.apply(user);
	}
	 /**
	 * Método encargado de obtener el usuario a patir del id.
	 * @param id del tipo {@link Long}: Id del usuario.
	 * @return user del tipo {@link UsuarioResponseDTO}: Usuario obtenido de la consulta.
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public UsuarioResponseDTO findUserById(Long id) throws NoDataFoundException{
		Usuario user= usuarioRepository.findById(id).orElseThrow(
				() -> new NoDataFoundException(MensajesEnum.USER_NO_FOUND_ID,id));
		
		return UsuarioAdapter.entityToResponse.apply(user);
	}
	 /**
     * Método encargado de obtener todos los usuarios del sistema.
     * @return {@link List}<{@link UsuarioResponseDTO}>: listado de los usuarios del sistema. 
      * @author </br>
    * Developer: Arnold Campillo.
     */
	@Override
	public List<UsuarioResponseDTO> findAll() {
		return UsuarioAdapter.entityListToResponseList.apply(usuarioRepository.findByEstBorradoIsFalse());
	}
	 /**
    * Método encargado de realizar la creación de los usuarios admin en el sistema.
    * @param dto del tipo {@link NewUserRequestDTO}: Dto request solicitado en la petición.
     * @author </br>
    * Developer: Arnold Campillo.
    * @throws
    */
	@Override
	public void registerAdmin(NewUserRequestDTO dto) throws ValidateServiceException  {
		registerUserAnyRol(dto,TypeRol.ROLE_ADMIN);	
	}

	 /**
    * Método encargado de realizar la creación de los usuarios en el sistema.
    * @param dto del tipo {@link NewUserRequestDTO}: Dto request solicitado en la petición.
     * @author </br>
    * Developer: Arnold Campillo.
    * @throws
    */
	@Override
	public void registerUser(NewUserRequestDTO dto) throws ValidateServiceException  {
		registerUserAnyRol(dto,TypeRol.ROLE_USER);		
	}

	 /**
    * Método encargado de realizar la eliminación del usuario en el sistema.
    * @param usercode del tipo {@link String}: Usercode solicitado en la petición.
     * @author </br>
    * Developer: Arnold Campillo.
    * @throws
    */
	@Override
	public void deleteUser(String usercode) throws ValidateServiceException,NoDataFoundException  {
		Usuario user=findUserByUsercodeEntity(usercode);
		if(user.isEstBorrado()) {
			throw new ValidateServiceException(MensajesEnum.USER_IS_DELETED,user.getUsername());
		}
		user.setEstBorrado(true);
		usuarioRepository.save(user);		
	}
	/**
    * Método encargado de realizar la inactivación del usuario en el sistema.
    * @param usercode del tipo {@link String}: Usercode solicitado en la petición.
     * @author </br>
    * Developer: Arnold Campillo.
    * @throws
    */
	@Override
	public void inactivateUser(String usercode) throws NoDataFoundException, ValidateServiceException{
		activateOrInactivateUser(usercode,false, false);
	}
	
	/**
    * Método encargado de realizar la activación del usuario en el sistema.
    * @param usercode del tipo {@link String}: Usercode solicitado en la petición.
    * @author </br>
    * Developer: Arnold Campillo.
    * @throws
    */
	@Override
	public void activateUser(String usercode) throws NoDataFoundException, ValidateServiceException {
		activateOrInactivateUser(usercode,true, true);	
	}

	
	
	/**
    * Método encargado de realizar la creación de los usuarios admin y user en el sistema.
    * @param dto del tipo {@link NewUserRequestDTO}: Dto request solicitado en la petición.
    * @param typeRol del tipo {@link TypeRol}:Type rol a asignar al usuario.
     * @author </br>
    * Developer: Arnold Campillo.
    * @throws
    */
	private void registerUserAnyRol(NewUserRequestDTO dto, TypeRol typeRol) throws ValidateServiceException {
		if(usuarioRepository.findByUsername(dto.username()).isPresent()) {
			throw new ValidateServiceException(MensajesEnum.USER_FOUND,dto.username());
		}
		String password = passwordEncoder.encode(decryptPassword(dto.password()));
		Usuario user=Usuario.builder().username(dto.username()).password(password).build();	
		user.setPassword(password);
		Optional<Rol> adminRole = rolRepository.findByCodigo(typeRol.name());
		if (!adminRole.isPresent()) {
			throw new ValidateServiceException(MensajesEnum.ROLE_NO_FOUND,dto.username());
		}
		user.setForceChangePass(true);
		user.addRoles(adminRole.get()); 
		usuarioRepository.save(user);
	}
	 /**
    * Método encargado de realizar la inactivación o activación del usuario en el sistema.
    * @param usercode del tipo {@link String}: Usercode solicitado en la petición.
    * @param enabled del tipo {@link boolean}: Valor para asiginar en la petición.
    * @param isActivateUser del tipo {@link boolean}: Valor para asiginar en la petición.
     * @author </br>
    * Developer: Arnold Campillo.
    * @throws
    */
	private void activateOrInactivateUser(String usercode,boolean enabled,  boolean isActivateUser) throws NoDataFoundException, ValidateServiceException  {
		Usuario user=findUserByUsercodeEntity(usercode);
		if(isActivateUser) {
			if(!user.isEnabled()) {
				throw new ValidateServiceException(MensajesEnum.USER_IS_INACTIVE,user.getUsername());
			}
		}else {
			if(user.isEnabled()) {
				throw new ValidateServiceException(MensajesEnum.USER_IS_ACTIVE,user.getUsername());
			}
		}			
		user.setEnabled(enabled);
		usuarioRepository.save(user);		
	}

	 /**
	 * Método encargado de desencriptar la password AES.
	 * @param rawPassword del tipo {@link String}: Texto a desencriptar.
	 * @return text del tipo {@link String}: Texto desencriptado.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private String decryptPassword(String rawPassword) {
		try {
			return AESCryptorUtil.decryptText(rawPassword,AppConfig.getkeyAes());
		} catch (Exception e) {
			return rawPassword;
		}
	}

	 
	
}
