/**************************CREACION DE TABLA PARA PARAMETROS DEL SISTEMA********************************/
DROP TABLE IF EXISTS parametro_sistema;
CREATE TABLE parametro_sistema(
  id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'IDENTIFICADOR DEL REGISTRO',
  codigo VARCHAR(50) NOT NULL COMMENT 'CODIGO ASIGNADO AL REGISTRO',
  valor VARCHAR(200) NOT NULL COMMENT 'VALOR RELACIONADO AL REGISTRO',  
  descripcion VARCHAR(300) NULL COMMENT 'DESCRIPCION DEL REGISTRO',  
  est_borrado TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'ESTADO DE BORRADO DEL REGISTRO',
  permite_update TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'VALIDA SI PERMITE ACTUALIZACIÓN EL REGISTRO'
);
/*CREACION DE LOS COMENTARIOS DE LA TABLA*/
ALTER TABLE parametro_sistema COMMENT 'TABLA DONDE SE ALMACENAN LOS PARAMETROS DEL SISTEMA.';

/*CREACION DE CONSTRAINTS UNIQUE DE LA TABLA*/
ALTER TABLE parametro_sistema ADD CONSTRAINT PS_UNIQ_AUTH UNIQUE (codigo);
/*CREACION DE INDICES DE LA TABLA */
CREATE INDEX IND_PS_AUTH ON parametro_sistema (codigo);
/*********************************************************************************************************/
/**************************INSERT TABLE parametro_sistema PARA PARAMETROS DEL SISTEMA*********************/
INSERT INTO parametro_sistema(codigo, valor, descripcion,permite_update)
VALUES
('APP_CORS_ORIGIN', 'http://localhost:4200', 'Cors Origin configurados para la aplicación, los valores debe estar separados por ;',1),
('APP_JWT_SECRET', 'apiHulkStoreContentSecretKey', 'Secret para codificar JWT.',1),
('APP_JWT_TOKEN_EXPIRATION', '15', 'Tiempo de expiración de token base para JWT en minutos.',1),
('APP_JWT_TOKEN_REFRESH_EXPIRATION', '17', 'Tiempo de expiración de token refresh para JWT en minutos.',1),
('APP_JWT_TOKEN_REFRESH_ATTEMPTS', '3', 'Número de intentos permitidos para refrescar el token.',1),
('APP_SESSION_IDLE', '10', 'Tiempo de expiración de la sesion en minutos.',1),
('APP_ATTEMPTS_PASSWORD', '3', 'Cantidad limite de intentos de contraseña invalida.',1),
('KEY_AES_APP', '*l3v4r34Dm1n20#.', 'Key para encripción',0),
('APP_OPENAPI_TITLE', 'Levare Admin Content Api.', 'Titulo para OpenApi.',1),
('APP_OPENAPI_VERSION', '0.0.1', 'Versión para OpenApi.',1),
('APP_OPENAPI_DESCRIPTION', 'Esta api es la encargada de administrar el contenido de Levare.', 'Descripción para OpenApi.',1),
('APP_OPENAPI_TERMS_URL', 'https://www.hulkstore.com/', 'Url de terminos y condiciones para OpenApi.',1),
('APP_OPENAPI_LICENSE_NAME', 'Licencia', 'Licencia para OpenApi.',1),
('APP_OPENAPI_LICENSE_URL', 'https://www.hulkstore.com/', 'Url de la licencia para OpenApi.',1),
('APP_USER_PET_CODE_EXPIRED', '5', 'Tiempo de expiración de la petición solicitada por el usuario en minutos.',1),
('APP_MAIL_HOST', 'smtp.gmail.com', 'Host del correo.',1),
('APP_MAIL_PORT', '465', 'Puerto del correo.',1),
('APP_MAIL_USERNAME', 'acprentt@gmail.com', 'Username del correo.',1),
('APP_MAIL_PASS', 'ntbtwrrsvbuypaos', 'Password del correo.',1),
('APP_PATH_FILESYSTEM', 'D:/TMP/hulkstore', 'File System de la aplicación.',1),
('MAIL_RECEIVER', 'acprentt@gmail.com', 'Username del correo.',1)
;
/*********************************************************************************************************/
/**************************CREACION DE TABLA PARA USUARIOS DEL SISTEMA********************************/
DROP TABLE IF EXISTS usuario;
CREATE TABLE usuario(
  id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'IDENTIFICADOR DEL REGISTRO',
  username VARCHAR(150) NOT NULL COMMENT 'USERNAME DEL USUARIO',
  password VARCHAR(150) NOT NULL COMMENT 'PASSWORD DEL USUARIO',
  usercode VARCHAR(150) NOT NULL COMMENT 'CODIGO DEL USUARIO',
  force_change_pass TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'INIDICA SI EL USUARIO DEBE CAMBIAR LA CONTRASEÑA',
  bloqued_by_invalid_pass TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'INIDICA SI ESTA BLOQUEADO POR SUPERAR EL LIMITE DE CONTRASEÑAS ERRONEAS',
  enabled TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'INIDICA SI EL USUARIO ESTA ACTIVO',
  est_borrado TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'ESTADO DE BORRADO DEL REGISTRO'
);
/*CREACION DE LOS COMENTARIOS DE LA TABLA*/
ALTER TABLE usuario COMMENT 'TABLA DONDE SE ALMACENAN LOS USUARIOS DEL SISTEMA.';

/*CREACION DE CONSTRAINTS UNIQUE DE LA TABLA*/
ALTER TABLE usuario ADD CONSTRAINT UNIQ_USER UNIQUE (username);
ALTER TABLE usuario ADD CONSTRAINT UNIQ_USER_CODE UNIQUE (usercode);
/*CREACION DE INDICES DE LA TABLA */
CREATE INDEX IND_USER ON usuario (username, password);
/*********************************************************************************************************/
/**************************INSERT TABLE usuario PARA USUARIOS DEL SISTEMA*********************/
INSERT INTO usuario
(username, password, usercode, force_change_pass, enabled, est_borrado,bloqued_by_invalid_pass)
VALUES('acprentt@gmail.com', '$2a$10$Y.IH4N2dXf57ifY.vI2pvugqbo8ez5eY2kOuWKz2eW5c17Xx4cx/2', 'f00cab5f-0aa8-11ef-8b9d-00155dcaf6d2', 0, 1, 0,0);
/*********************************************************************************************************/
/**************************CREACION DE TABLA PARA ROLES DEL SISTEMA********************************/
DROP TABLE IF EXISTS rol;
CREATE TABLE rol(
  id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'IDENTIFICADOR DEL REGISTRO',
  codigo VARCHAR(150) NOT NULL COMMENT 'CODIGO DEL ROL',
  valor VARCHAR(150) NOT NULL COMMENT 'VALOR DEL ROL',
  descripcion VARCHAR(300) NULL COMMENT 'DESCRIPCION DEL REGISTRO',
  enabled TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'INIDICA SI EL ROL ESTA ACTIVO',
  est_borrado TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'ESTADO DE BORRADO DEL REGISTRO'
);
/*CREACION DE LOS COMENTARIOS DE LA TABLA*/
ALTER TABLE rol COMMENT 'TABLA DONDE SE ALMACENAN LOS ROLES DEL SISTEMA.';

/*CREACION DE CONSTRAINTS UNIQUE DE LA TABLA*/
ALTER TABLE rol ADD CONSTRAINT UNIQ_ROL UNIQUE (codigo);
/*CREACION DE INDICES DE LA TABLA */
CREATE INDEX IND_ROL ON rol (codigo, valor);
/*********************************************************************************************************/
/**************************INSERT TABLE usuario PARA USUARIOS DEL SISTEMA*********************/
INSERT INTO rol
(codigo, valor,descripcion, enabled, est_borrado)
VALUES
('ROLE_ADMIN', 'ROLE_ADMIN','Rol administrador del sistema.',1,0),
('ROLE_USER', 'ROLE_USER','Rol de usuario del sistema.',1,0)
;
/*********************************************************************************************************/
/**************************CREACION DE TABLA PARA ROLES ASOCIADOS AL USUARIO DEL SISTEMA********************************/
DROP TABLE IF EXISTS usuario_rol;
CREATE TABLE usuario_rol(
  id_usuario BIGINT NOT NULL COMMENT 'ID DEL USUARIO',
  id_rol BIGINT NOT NULL COMMENT 'ID DEL ROL'
);
/*CREACION DE LOS COMENTARIOS DE LA TABLA*/
ALTER TABLE usuario_rol COMMENT 'TABLA DONDE SE ALMACENAN LOS ROLES ASOCIADOS AL USUARIO DEL SISTEMA.';

/*CREACION DE CONSTRAINTS UNIQUE DE LA TABLA*/
ALTER TABLE usuario_rol ADD CONSTRAINT UNIQ_USER_ROL UNIQUE (id_usuario,id_rol);

/*CREACION DE FOREING KEY DE LA TABLA*/
ALTER TABLE usuario_rol ADD CONSTRAINT FRK_USER_ROL_USER FOREIGN KEY (id_usuario) REFERENCES usuario(id);
ALTER TABLE usuario_rol ADD CONSTRAINT FRK_USER_ROL_ROL FOREIGN KEY (id_rol) REFERENCES rol(id);
/*********************************************************************************************************/
/**************************INSERT TABLE usuario PARA USUARIOS DEL SISTEMA*********************/
INSERT INTO usuario_rol
(id_usuario, id_rol)
VALUES
(1, 1);
/*********************************************************************************************************/
/**************************CREACION DE TABLA PARA TOKENS ASOCIADOS AL USUARIO DEL SISTEMA********************************/
DROP TABLE IF EXISTS usuario_token;
CREATE TABLE usuario_token(
  id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'IDENTIFICADOR DEL REGISTRO',
  id_usuario BIGINT NOT NULL COMMENT 'ID DEL USUARIO',
  token VARCHAR(500) NOT NULL COMMENT 'TOKEN GENERADO AL USUARIO PARA AUTENTICARSE',
  token_expire DATETIME NOT NULL COMMENT 'FECHA EN LA CUAL EXPIRA EL TOKEN GENERADO AL USUARIO PARA AUTENTICARSE',
  refresh_token VARCHAR(1000) NOT NULL COMMENT 'REFRESH TOKEN GENERADO AL USUARIO PARA REFRESCAR EL TOKEN',
  refresh_token_expire DATETIME NOT NULL COMMENT 'FECHA EN LA CUAL EXPIRA EL REFRESH TOKEN GENERADO AL USUARIO PARA AUTENTICARSE'
);
/*CREACION DE LOS COMENTARIOS DE LA TABLA*/
ALTER TABLE usuario_token COMMENT 'TABLA DONDE SE ALMACENAN LOS TOKENS ASOCIADOS AL USUARIO DEL SISTEMA.';
/*CREACION DE CONSTRAINTS UNIQUE DE LA TABLA*/
ALTER TABLE usuario_token ADD CONSTRAINT UNIQ_USER_TOK UNIQUE (id_usuario);
/*CREACION DE INDICES DE LA TABLA */
CREATE INDEX IND_USER_TOKEN ON usuario_token (id_usuario, token,token_expire);
/*CREACION DE FOREING KEY DE LA TABLA*/
ALTER TABLE usuario_token ADD CONSTRAINT FRK_USER_TOKEN_USER FOREIGN KEY (id_usuario) REFERENCES usuario(id);
/*********************************************************************************************************/
/**************************CREACION DE TABLA PARA LAS ORDENES DE PRODUCTO********************************/
DROP TABLE IF EXISTS orden_producto;
CREATE TABLE orden_producto(
  id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'IDENTIFICADOR DEL REGISTRO',
  codigo VARCHAR(50) NOT NULL COMMENT 'CODIGO DE LA ORDEN',
  cantidad  float NOT NULL COMMENT 'CANTIDAD DEL RODUCTO',
  producto_id BIGINT NOT NULL COMMENT 'ID DEL PRODUCTO',
  fecha  DATETIME NOT NULL COMMENT 'FECHA DE CREACIÓN',
  tipo_transaccion VARCHAR(20) NOT NULL COMMENT 'TIPO DE TRANSACCION',
  est_borrado TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'ESTADO DE BORRADO DEL REGISTRO'
);
/*CREACION DE LOS COMENTARIOS DE LA TABLA*/
ALTER TABLE orden_producto COMMENT 'TABLA DONDE SE ALMACENAN LAS ORDENES DE LOS PROODUCTOS.';
/*CREACION DE CONSTRAINTS UNIQUE DE LA TABLA*/
ALTER TABLE orden_producto ADD CONSTRAINT UNIQ__ORDENPROD UNIQUE (codigo);
/*CREACION DE INDICES DE LA TABLA */
CREATE INDEX IND_ORDENPROD ON orden_producto (codigo);
/*CREACION DE FOREING KEY DE LA TABLA*/
ALTER TABLE orden_producto ADD CONSTRAINT FRK_ORDENPROD_PROD FOREIGN KEY (producto_id) REFERENCES producto(id);

/**************************CREACION DE TABLA PARA LAS PRODUCTO********************************/
DROP TABLE IF EXISTS producto;
CREATE TABLE producto(
  id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'IDENTIFICADOR DEL REGISTRO',
  nombre VARCHAR(50) NOT NULL COMMENT 'NOMBRE DEL PRODUCTO',
  referencia VARCHAR(50) NOT NULL COMMENT 'REFERENCIA DEL RODUCTO',
  precio_venta double NOT NULL COMMENT 'PRECIO VENTA DEL RODUCTO',
  cantidad  float NOT NULL COMMENT 'CANTIDAD DEL RODUCTO',
  codigo VARCHAR(50) NOT NULL COMMENT 'CODIGO DEL PRODUCTO',
  descripcion VARCHAR(300) NULL COMMENT 'DESCRIPCION DEL REGISTRO',
  categoria_id BIGINT NOT NULL COMMENT 'ID DE LA CATEGORIA DEL PRODUCTO',
  marca_id BIGINT NOT NULL COMMENT 'ID DE LA MARCA DEL PRODUCTO',
  enabled TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'INIDICA SI EL PRODUCTO ESTA ACTIVO',
  est_borrado TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'ESTADO DE BORRADO DEL REGISTRO'
);
/*CREACION DE LOS COMENTARIOS DE LA TABLA*/
ALTER TABLE producto COMMENT 'TABLA DONDE SE ALMACENAN LOS PROODUCTOS.';
/*CREACION DE CONSTRAINTS UNIQUE DE LA TABLA*/
ALTER TABLE producto ADD CONSTRAINT UNIQ_PROD UNIQUE (codigo);
/*CREACION DE INDICES DE LA TABLA */
CREATE INDEX IND_PROD ON producto (codigo);
/*CREACION DE FOREING KEY DE LA TABLA*/
ALTER TABLE producto ADD CONSTRAINT FRK_PROD_CAT FOREIGN KEY (categoria_id) REFERENCES categoria(id);
ALTER TABLE producto ADD CONSTRAINT FRK_PROD_MAR FOREIGN KEY (marca_id) REFERENCES marca(id);
/**************************CREACION DE TABLA PARA LAS CATEGORIAS DE LOS PRODUCTOS********************************/
DROP TABLE IF EXISTS categoria;
CREATE TABLE categoria(
  id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'IDENTIFICADOR DEL REGISTRO',
  nombre VARCHAR(50) NOT NULL COMMENT 'NOMBRE DE LA CATEGORIA',
  codigo VARCHAR(50) NOT NULL COMMENT 'CODIGO DE LA CATEGORIA',
  descripcion VARCHAR(300) NULL COMMENT 'DESCRIPCION DEL REGISTRO',
  enabled TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'INIDICA SI LA CATEGORIA ESTA ACTIVO',
  est_borrado TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'ESTADO DE BORRADO DEL REGISTRO'
);
/*CREACION DE LOS COMENTARIOS DE LA TABLA*/
ALTER TABLE categoria COMMENT 'TABLA DONDE SE ALMACENAN LAS CATEGORIAS DE LOS PRODUCTOS.';
/*CREACION DE CONSTRAINTS UNIQUE DE LA TABLA*/
ALTER TABLE categoria ADD CONSTRAINT UNIQ_CAT UNIQUE (codigo);
/*CREACION DE INDICES DE LA TABLA */
CREATE INDEX IND_CAT ON categoria (codigo);

/**************************CREACION DE TABLA PARA LAS MARCAS DE LOS PRODUCTOS********************************/
DROP TABLE IF EXISTS marca;
CREATE TABLE marca(
  id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'IDENTIFICADOR DEL REGISTRO',
  nombre VARCHAR(50) NOT NULL COMMENT 'NOMBRE DE LA MARCA',
  codigo VARCHAR(50) NOT NULL COMMENT 'CODIGO DE LA MARCA',
  descripcion VARCHAR(300) NULL COMMENT 'DESCRIPCION DEL REGISTRO',
  enabled TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'INIDICA SI LA MARCA ESTA ACTIVO',
  est_borrado TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'ESTADO DE BORRADO DEL REGISTRO'
);
/*CREACION DE LOS COMENTARIOS DE LA TABLA*/
ALTER TABLE marca COMMENT 'TABLA DONDE SE ALMACENAN LAS MARCAS DE LOS PRODUCTOS.';
/*CREACION DE CONSTRAINTS UNIQUE DE LA TABLA*/
ALTER TABLE marca ADD CONSTRAINT UNIQ_MAR UNIQUE (codigo);
/*CREACION DE INDICES DE LA TABLA */
CREATE INDEX IND_MAR ON marca (codigo);




