package com.hulkstore.app.security.dto.request;

import com.hulkstore.app.consts.ConstantesApp;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Builder;
/**
 * DTO request que contiene la información solicitada para realizar la autenticación en el sistema.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Builder
@Schema(description = "DTO request que contiene la información solicitada para realizar la autenticación en el sistema.")
public record AuthUserRequestDTO(
		@NotNull(message = "{app.message.valid.not.null}")
		@NotBlank(message = "{app.message.valid.not.black}")
		@Size(message =  "{app.message.valid.size.min.max}", min = 1, max = 60)
		@Schema(description = "Username del usuario.",example = "testgcr@test.com")
		@Pattern(regexp = ConstantesApp.PATTERN_CORREO, message = "{app.message.valid.patter.correo}")
		String username,
		@NotNull(message = "{app.message.valid.not.null}")
		@NotBlank(message = "{app.message.valid.not.black}")	
		@Schema(description = "Password del usuario.",example = "testgcr")
		String password) {

}
