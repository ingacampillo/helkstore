package com.hulkstore.app.security.enums;

/**
 * {@link Enum} que contiene los tipos de tokens disponibles a generar.
 * @author Acasi.</br>
 * Developer: Arnold Campillo
 */
public enum TypeTokenEnum {
	ACCESS,
	REFRESH

}
