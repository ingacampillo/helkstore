package com.hulkstore.app.config;


import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.AuthenticationsException;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.util.ResponseUtil;

import lombok.RequiredArgsConstructor;
/**
 * Clase encargada de personalizar y gestionar los errores del sistema.
 * @author </br>
 * Developer: Arnold Campillo
 */
@ControllerAdvice
@RequiredArgsConstructor
public class ErrorHandlerConfig extends ResponseEntityExceptionHandler {

	private final ResponseUtil responseUtil;
	
	
	 /**
     * Método encargado de gestionar los errores({@link NoResourceFoundException}) de recursos no disponibles, presentados en el sistema(NOT_FOUND).
     * @param ex del tipo {@link NoResourceFoundException}: Error presentado a tratar.  
     * @param headers del tipo {@link HttpHeaders}: Headers para uso del tratamiento del error.
     * @param status del tipo {@link HttpStatusCode}: Status para uso del tratamiento del error.
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@Override
	protected ResponseEntity<Object> handleNoResourceFoundException(NoResourceFoundException ex, HttpHeaders headers,
			HttpStatusCode status, WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.NOT_FOUND, ex,MensajesEnum.NOT_FOUND_RESOURCE,request);
	}

	/**
     * Método encargado de gestionar los errores({@link HttpRequestMethodNotSupportedException}) de método no soportado, presentados en el sistema(METHOD_NOT_ALLOWED).
     * @param ex del tipo {@link HttpRequestMethodNotSupportedException}: Error presentado a tratar.  
     * @param headers del tipo {@link HttpHeaders}: Headers para uso del tratamiento del error.
     * @param status del tipo {@link HttpStatusCode}: Status para uso del tratamiento del error.
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.METHOD_NOT_ALLOWED, ex,MensajesEnum.METHOD_NOT_ALLOWED,ObjectUtils.toObjectArray(ex.getMethod()),request);
	}
	/**
     * Método encargado de gestionar los errores({@link MissingServletRequestParameterException}) de parámetros faltantes en la url de la petición, presentados en el sistema(BAD_REQUEST).
     * @param ex del tipo {@link MissingServletRequestParameterException}: Error presentado a tratar.  
     * @param headers del tipo {@link HttpHeaders}: Headers para uso del tratamiento del error.
     * @param status del tipo {@link HttpStatusCode}: Status para uso del tratamiento del error.
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.BAD_REQUEST, ex,MensajesEnum.BAD_REQUEST_QUERY_PARAM,request);
	}
	/**
     * Método encargado de gestionar los errores({@link MissingPathVariableException}) de parámetros faltantes en la estructura url de la petición, presentados en el sistema(BAD_REQUEST).
     * @param ex del tipo {@link MissingPathVariableException}: Error presentado a tratar.  
     * @param headers del tipo {@link HttpHeaders}: Headers para uso del tratamiento del error.
     * @param status del tipo {@link HttpStatusCode}: Status para uso del tratamiento del error.
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@Override
	protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers,
			HttpStatusCode status, WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.BAD_REQUEST, ex,MensajesEnum.BAD_REQUEST_PATH_PARAM,request);
	}
	/**
     * Método encargado de gestionar los errores({@link ServletRequestBindingException}) de validaciones de campos, presentados en el sistema(BAD_REQUEST).
     * @param ex del tipo {@link ServletRequestBindingException}: Error presentado a tratar.  
     * @param headers del tipo {@link HttpHeaders}: Headers para uso del tratamiento del error.
     * @param status del tipo {@link HttpStatusCode}: Status para uso del tratamiento del error.
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@Override
	protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.BAD_REQUEST, ex,MensajesEnum.BAD_REQUEST,request);
	}
	/**
     * Método encargado de gestionar los errores({@link HttpMessageNotReadableException}) de tipos de datos incorrectos, presentados en el sistema(BAD_REQUEST).
     * @param ex del tipo {@link HttpMessageNotReadableException}: Error presentado a tratar.  
     * @param headers del tipo {@link HttpHeaders}: Headers para uso del tratamiento del error.
     * @param status del tipo {@link HttpStatusCode}: Status para uso del tratamiento del error.
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
    * @author </br>
     * Developer: Arnold Campillo
     */
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {
		MensajesEnum error=null;	 
		Object[] params=null;
	    if(ex.getCause() instanceof InvalidFormatException ife) {	
	    	error=MensajesEnum.BAD_REQUEST_TYPE_DATA;
	    	params= new Object[]{ife.getValue(),ife.getTargetType().getSimpleName()};
	    }else if(ex.getCause() instanceof MismatchedInputException) {
	    	error=MensajesEnum.BAD_REQUEST_TYPE_DATA_BODY;
	    } else {
	    	error=MensajesEnum.BAD_REQUEST_TYPE_DATA_GENERIC;
	    }	
	    return this.responseUtil.buildErrorResponse(HttpStatus.BAD_REQUEST, ex,error,params,request);     
	}
	/**
     * Método encargado de gestionar los errores({@link MethodArgumentNotValidException}) de validaciones de campos, presentados en el sistema(BAD_REQUEST).
     * @param ex del tipo {@link MethodArgumentNotValidException}: Error presentado a tratar.  
     * @param headers del tipo {@link HttpHeaders}: Headers para uso del tratamiento del error.
     * @param status del tipo {@link HttpStatusCode}: Status para uso del tratamiento del error.
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
    @Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {
    	  List<String> errors = new ArrayList<>();
          ex.getBindingResult().getAllErrors().forEach(error ->{    
        	 String fieldName = ((FieldError) error).getField();
              String message = error.getDefaultMessage();
              errors.add(message.replace("##validatedValue##", fieldName));
          });
          return this.responseUtil.buildErrorResponse(HttpStatus.BAD_REQUEST, ex,MensajesEnum.BAD_REQUEST,null,errors,request);
	}
	/**
     * Método encargado de gestionar los errores({@link NoHandlerFoundException}) de errores inesperados, presentados en el sistema(INTERNAL_SERVER_ERROR).
     * @param ex del tipo {@link NoHandlerFoundException}: Error presentado a tratar.  
     * @param headers del tipo {@link HttpHeaders}: Headers para uso del tratamiento del error.
     * @param status del tipo {@link HttpStatusCode}: Status para uso del tratamiento del error.
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
			HttpStatusCode status, WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex,MensajesEnum.ERROR,request);
	}
	
	/**
     * Método encargado de gestionar los errores({@link Exception}) de errores inesperados, presentados en el sistema(INTERNAL_SERVER_ERROR).
     * @param ex del tipo {@link Exception}: Error presentado a tratar.  
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
    * @author </br>
     * Developer: Arnold Campillo
     */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> all(Exception ex, WebRequest request) {
        return this.responseUtil.buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex,MensajesEnum.ERROR,request);		
	}

	/**
     * Método encargado de gestionar los errores({@link ValidateServiceException}) de errores de validaciones no cumplidas, presentados en el sistema(BAD_REQUEST).
     * @param ex del tipo {@link ValidateServiceException}: Error presentado a tratar.  
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@ExceptionHandler(ValidateServiceException.class)
	public ResponseEntity<Object> validateServiceException(ValidateServiceException ex,
			WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.BAD_REQUEST, ex,ex.getError(),ex.getParamMessage(),request);
	}
	/**
     * Método encargado de gestionar los errores({@link NoDataFoundException}) de data no encontrada, presentados en el sistema(NOT_FOUND).
     * @param ex del tipo {@link NoDataFoundException}: Error presentado a tratar.  
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@ExceptionHandler(NoDataFoundException.class)
	public ResponseEntity<Object> noDataFoundException(NoDataFoundException ex, WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.NOT_FOUND, ex,ex.getError(),ex.getParamMessage(),request);
	}
	/**
     * Método encargado de gestionar los errores({@link GeneralServiceException}) de errores inesperados, presentados en el sistema(INTERNAL_SERVER_ERROR).
     * @param ex del tipo {@link GeneralServiceException}: Error presentado a tratar.  
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@ExceptionHandler(GeneralServiceException.class)
	public ResponseEntity<Object> generalServiceException(GeneralServiceException ex,
			WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex,ex.getError(),request);
	}
	/**
     * Método encargado de gestionar los errores({@link AuthenticationsException}) de errores de autenticacion y autorización, presentados en el sistema(UNAUTHORIZED).
     * @param ex del tipo {@link AuthenticationsException}: Error presentado a tratar.  
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@ExceptionHandler(AuthenticationsException.class)
	public ResponseEntity<Object> authenticationsException(AuthenticationsException ex,
			WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.UNAUTHORIZED, ex,ex.getError(),ex.getParamMessage(),request);
	}
	/**
     * Método encargado de gestionar los errores({@link AuthenticationsException}) de errores de autenticacion, presentados en el sistema(UNAUTHORIZED).
     * @param ex del tipo {@link AuthenticationsException}: Error presentado a tratar.  
     * @param request del tipo {@link WebRequest}: request para uso del tratamiento del error.  
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response personalizado para el error.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@ExceptionHandler(BadCredentialsException.class)
	public ResponseEntity<Object> badCredentialsException(BadCredentialsException ex,
			WebRequest request) {
		return this.responseUtil.buildErrorResponse(HttpStatus.UNAUTHORIZED, ex,MensajesEnum.ERROR_AUTH,request);
	}
	
}
