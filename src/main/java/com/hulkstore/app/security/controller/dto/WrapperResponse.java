package com.hulkstore.app.security.controller.dto;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.hulkstore.app.consts.ConstantesApp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Clase la cual es una plantilla que contiene la estructura del response de los RestApi, donde el Type T es el contenido del body de response.
 * @author </br>
 * Developer:Arnold Campillo
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
@Schema(description = "Plantilla de response")
public class WrapperResponse<T> {
	@Schema(description = "estado de respuesta(true:Correcto,false:incorrecto).",example = "true")
	private boolean ok;
	@Schema(description = "Mensaje generado por la transacción.",example = "CORRECT")
	private String message;
	@Schema(description = "Codigo del mensaje generado por la transacción.",example = "APP_001")
	private String codeMessage;
	@Schema(description = "tipo del mensaje generado por la transacción (INFO, WARN, ERROR).",example = "INFO")
	private String typeMessage;
	@Schema(description = "tipo del mensaje generado por la transacción (GENERIC,BUSINESS_ERROR,TECHNICAL_ERROR).",example = "GENERIC")
	private String classMessage;
	@Schema(description = "código de la traza de la peticion generado por la transacción.",example = ConstantesApp.SWAGGER_HEADER_TRACE_CODE_EXAMPLE)
	private String traceCode;
	@Schema(description = "resultado generado por la transacción.")
	private T body;

	 /**
     * Método encargado de crear el response de los RestApi, donde el Type T es el contenido del body.
     * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}< T >>>: Response con el resultado de la petición. 
     * @author </br>
     * Developer: Arnold Campillo
     */	
	public ResponseEntity<WrapperResponse<T>> createResponse(){
		return new ResponseEntity<>(this, HttpStatus.OK);
	}
	 /**
     * Método encargado de crear el response de los RestApi que sus servicio es tipo {@link void}.
     * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}<{@link Void}>>>: Response con el resultado de la petición. 
     * @author </br>
     * Developer: Arnold Campillo
     */	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<WrapperResponse<Void>> createResponseVoid(){
		return new ResponseEntity(this, HttpStatus.OK);
	}
	
	 /**
     * Método encargado de crear el response de los RestApi, donde el Type T es el contenido del body.
     * @param status del tipo {@link HttpStatus}: HttpStatus a asignar.  
     * @return {@link ResponseEntity}<{@link WrapperResponse}<{@link List}< T >>>: Response con el resultado de la petición. 
     * @author </br>
     * Developer: Arnold Campillo
     */	
	public ResponseEntity<WrapperResponse<T>> createResponse(HttpStatus status){
		return new ResponseEntity<>(this,status);
	}
}
