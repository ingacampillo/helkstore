package com.hulkstore.app.service;

import java.util.List;

import com.hulkstore.app.dto.request.MarcaRequestDTO;
import com.hulkstore.app.dto.response.MarcaResponseDTO;
import com.hulkstore.app.entity.Marca;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
/**
 * Interfaz Service encargada de realizar las transacciones y operaciones de {@link Marca}.
 * @author </br>
 * Developer: Arnold Campillo
 */
public interface MarcaService {

	Marca obtenerMarcaPorId(Integer marcaId); 
	
	/**
	 * Método encargado de obtener todos las marcas del sistema.
	 * 
	 * @return {@link List}<{@link MarcaResponseDTO}>: listado de las marcas del
	 *         sistema.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	public List<MarcaResponseDTO> findAll()
			throws ValidateServiceException, NoDataFoundException, GeneralServiceException;
			
	 /**
     * Método encargado de actualizar la marca.
     * @param dto del tipo {@link MarcaRequestDTO}: dto solicitado en la actualización.
     * @return {@link MarcaResponseDTO}: Marca del sistema actualizado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	MarcaResponseDTO update(MarcaRequestDTO dto)throws ValidateServiceException;
	
	 /**
     * Método encargado de registrar la marca.
     * @param dto del tipo {@link MarcaRequestDTO}: dto solicitado en el registro.
     * @return {@link MarcaResponseDTO}: Marca del sistema registrado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	MarcaResponseDTO insert(MarcaRequestDTO dto)throws ValidateServiceException;
	
	
}
