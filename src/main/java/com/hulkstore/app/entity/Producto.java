package com.hulkstore.app.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "producto")
@EqualsAndHashCode(callSuper = false)
@ToString
public class Producto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "codigo", nullable = false, updatable = false)
	@Builder.Default
	private String codigo = UUID.randomUUID().toString();

	@Column(name = "enabled", length = 1, columnDefinition = "boolean default true")
	@Builder.Default
	private boolean enabled = true;

	@Column(name = "est_borrado", length = 1, columnDefinition = "boolean default false")
	@Builder.Default
	private boolean estBorrado = false;

	@Column(name = "nombre", nullable = false, length = 150)
	private String nombre;

	@Column(name = "descripcion", length = 1000)
	private String descripcion;

	@Column(name = "referencia", nullable = false, length = 50)
	private String referencia;

	@Column(name = "precio_venta", nullable = false)
	@Builder.Default
	private BigDecimal precioVenta = BigDecimal.ZERO;
	
	@Column(name = "cantidad", nullable = false,columnDefinition = "int default 0")
	@Builder.Default
	private Integer cantidad=0;

	@Transient
	@Builder.Default
	private String statusStock="OUTOFSTOCK";

	@ManyToOne(targetEntity = Marca.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "marca_id", referencedColumnName = "id", nullable = false)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Marca marca;

	@ManyToOne(targetEntity = Categoria.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "categoria_id", referencedColumnName = "id", nullable = false)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Categoria categoria;
	
	@JsonIgnoreProperties(value = { "producto", "hibernateLazyInitializer", "handler" }, allowSetters = true)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "producto", cascade = CascadeType.ALL)
	private List<OrdenProducto> inventariosProducto;




	
}
