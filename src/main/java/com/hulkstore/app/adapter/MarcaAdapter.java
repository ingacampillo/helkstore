package com.hulkstore.app.adapter;

import java.util.List;
import java.util.function.Function;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.dto.request.MarcaRequestDTO;
import com.hulkstore.app.dto.response.MarcaResponseDTO;
import com.hulkstore.app.entity.Marca;

/**
 * Adaptador encargado de definir la manera en que se deben convertir objetos
 * entre Entity y DTOs (Y viceversa).
 * 
 * @author </br>
 *         Developer: Arnold Campillo.
 */
public class MarcaAdapter {

	private MarcaAdapter() {
	}

	/**
	 * Función encargada de adaptar un objeto DTO Request a su respectivo Entity(de
	 * {@link MarcaRequestDTO} a {@link Marca}).
	 * 
	 * @param dto del tipo {@link MarcaRequestDTO}: Dto Request a convertir a
	 *            Entity.
	 * @return {@link Marca}: Entity resultado de la conversión.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 */
	public static final Function<MarcaRequestDTO, Marca> requestToEntity = (MarcaRequestDTO dto) -> {
		if (ObjectUtils.isEmpty(dto)) {
			return null;
		}
		return Marca.builder().id(dto.id()).nombre(dto.nombre()).estBorrado(dto.estBorrado()).build();
	};

	/**
	 * Función encargada de adaptar un objeto Entity a su respectivo DTO Response(de
	 * {@link Marca} a {@link MarcaResponseDTO}).
	 * 
	 * @param entity del tipo {@link Marca}: Entity a convertir a DTO Response.
	 * @return {@link MarcaResponseDTO}: DTO Response resultado de la conversión.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 */
	public static final Function<Marca, MarcaResponseDTO> entityToResponse = (Marca entity) -> {
		if (ObjectUtils.isEmpty(entity)) {
			return null;
		}

		return MarcaResponseDTO.builder().id(entity.getId()).codigo(entity.getCodigo()).nombre(entity.getNombre())
				.estBorrado(entity.isEstBorrado()).build();
	};

	/**
	 * Función encargada de adaptar un listado Entity a su respectivo listado DTO
	 * Response(de {@link List}<{@link Marca}> a
	 * {@link List}<{@link MarcaResponseDTO}>).
	 * 
	 * @param entityList del tipo {@link List}<{@link Marca}>: Entity List a
	 *                   convertir a DTO Response List.
	 * @return {@link List}<{@link MarcaResponseDTO}>: DTO Response List resultado
	 *         de la conversión.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 */

	public static final List<MarcaResponseDTO> listEntityToResponse(List<Marca> entityList) {
		if (CollectionUtils.isEmpty(entityList)) {
			return List.of();
		}
		return entityList.stream().map(entityToResponse).toList();
	}

}
