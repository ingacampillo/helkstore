package com.hulkstore.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hulkstore.app.entity.Marca;

@Repository
public interface MarcaRepository extends JpaRepository<Marca, Integer> {
	
	Boolean existsByNombre(String nombre);

	Optional<Marca> findById(Integer id);
	
	List<Marca> findByEstBorradoIsFalse();
	
	Optional<Marca> findByCodigo(String codigo);
	
    
}
