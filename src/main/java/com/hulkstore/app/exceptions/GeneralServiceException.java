package com.hulkstore.app.exceptions;

import java.io.Serializable;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.hulkstore.app.enums.MensajesEnum;

import lombok.Getter;
import lombok.Setter;
/**
 * Clase la cual es la encargada de contener la información relevante de las excepciones generales del tipo {@link RuntimeException}, presentadas en el sistema.
  * @author </br>
  * Developer: Arnold Campillo
 */
@Getter
@Setter
@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class GeneralServiceException extends RuntimeException implements Serializable {

	private static final long serialVersionUID = 1L;
	private final MensajesEnum error;
	
	 /**
     * Constructor encargado de inicializar la variables de la clase.
     * @param error del tipo {@link MensajesEnum}: mensaje de error a asignar.
     * @param cause del tipo {@link Throwable}: Throwable del error. 
      * @author </br>
     * Developer: Arnold Campillo
     */	
	public GeneralServiceException(MensajesEnum error, Throwable cause) {
		super(error.getCode(), cause);
		this.error=error;
	}
	 /**
     * Constructor encargado de inicializar la variables de la clase.
     * @param mensaje del tipo {@link String}: mensaje de error generado.
     * @param error del tipo {@link MensajesEnum}: mensaje de error a asignar.
     * @param cause del tipo {@link Throwable}: Throwable del error. 
      * @author </br>
     * Developer: Arnold Campillo
     */	
	public GeneralServiceException(String mensaje,MensajesEnum error, Throwable cause) {
		super(mensaje, cause);
		this.error=error;
	}
	
}
