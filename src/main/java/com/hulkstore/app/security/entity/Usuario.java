package com.hulkstore.app.security.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.Basic;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.UniqueConstraint;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.util.UUID;
/**
 * Entity que gestiona la información acerca de los usuarios del sistema.
 * @author </br>
 * Developer:Arnold Campillo
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "usuario")
@EqualsAndHashCode(callSuper = false)
@ToString
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;
	
	@Basic(optional = false)
    @Column(name = "username", nullable = false, length = 150)
	private String username;
	
	@Basic(optional = false)
    @Column(name = "usercode", nullable = false, length = 150)
	@Builder.Default
	private String usercode=UUID.randomUUID().toString();
	
	@Basic(optional = false)
    @Column(name = "password" , nullable = false, length = 150)
	private String password;
		
	@Column(name = "force_change_pass", length = 1, columnDefinition = "boolean default true")
	@Builder.Default
	private boolean forceChangePass = true;
	
	@Column(name = "bloqued_by_invalid_pass", length = 1, columnDefinition = "boolean default false")
	@Builder.Default
	private boolean bloquedByInvalidPass = false;
	
	@Column(name = "enabled", length = 1, columnDefinition = "boolean default true")
	@Builder.Default
	private boolean enabled = true;
	
    @Column(name = "est_borrado", length = 1, columnDefinition = "boolean default false")
	@Builder.Default
	private boolean estBorrado = false;
    
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "usuario_rol", joinColumns = @JoinColumn(name = "id_usuario"), inverseJoinColumns = @JoinColumn(name = "id_rol"), uniqueConstraints = {
			@UniqueConstraint(columnNames = { "id_usuario", "id_rol" }) })
	private List<Rol> roles;
	
	
	public void addRoles(Rol rol) {
		if (rol != null) {
			if (this.roles == null) {
				this.roles = new ArrayList<>();
			}
			this.roles.add(rol);
		}

	}

}
