package com.hulkstore.app.security.dto.request;


import com.hulkstore.app.consts.ConstantesApp;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;
/**
 * DTO request que contiene la información solicitada para solicitar el reestablecimiento de la contraseña del usuario.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Builder
@Schema(description = "DTO request que contiene la información solicitada para solicitar el reestablecimiento de la contraseña del usuario.")
public record UserResetPassRequestDTO (

	@NotNull(message = "{app.message.valid.not.null}")
	@NotBlank(message = "{app.message.valid.not.black}")
	@Pattern(regexp = ConstantesApp.PATTERN_CORREO, message = "{app.message.valid.patter.correo}")
	@Schema(description = "Username del usuario.",example = "testgcr")
	String username,
	@NotNull(message = "{app.message.valid.not.null}")
	@NotBlank(message = "{app.message.valid.not.black}")
	@Schema(description = "Código de petición para reiniciar la clave.",example = "suiyeimsia-0414-asas")
	String codigoPeticion
	) {
}
