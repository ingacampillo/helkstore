package com.hulkstore.app.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;

/**
 * DTO request que contiene la información solicitada de las marcas
 * 
 * @author </br>
 *         Developer: Arnold Campillo.
 */
@Builder
@Schema(description = "DTO request que contiene la información solicitada de categorias.")
public record CategoriaRequestDTO(
		@Schema(description = "Identificador de la categoria.", example = "16551615615611") 
		Integer id,
		@Schema(description = "Nombre de la categoria.", example = "NOMBRE_001") 
		@NotNull(message = "{app.message.valid.not.null}")
		@NotBlank(message = "{app.message.valid.not.black}") 
		@Size(message = "{app.message.valid.size.min.max}", min = 1, max = 200) 
		String nombre,
		@Schema(description = "Indica si la categoria esta activo.", example = "true") 
		@NotNull(message = "{app.message.valid.not.null}") 
		Boolean estBorrado

) {
}
