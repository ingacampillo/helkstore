package com.hulkstore.app.dto.request;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;
/**
 * DTO request que contiene la información solicitada de los parametros del sistema.
 * @author </br>
 * Developer: Arnold Campillo.
 */
@Builder
@Schema(description = "DTO request que contiene la información solicitada de parametros del sistema.")
public record ParametroSistemaRequestDTO(
		@Schema(description = "Identificador del parametro.",example = "16551615615611")
		Integer id,
		@Schema(description = "Código del parametro.",example = "CODE_052")
		@NotNull(message = "{app.message.valid.not.null}")
		@NotBlank(message = "{app.message.valid.not.black}")
		@Size(message =  "{app.message.valid.size.min.max}", min = 1, max = 50)
		String codigo,
		@Schema(description = "Valor del parametro.",example = "VALOR_50")
		@NotNull(message = "{app.message.valid.not.null}")
		@NotBlank(message = "{app.message.valid.not.black}")
		@Size(message =  "{app.message.valid.size.min.max}", min = 1, max = 200)
		String valor,
		@Schema(description = "Descripción del parametro.",example = "DESCRIBE EL PARAMETRO")
		@Size(message =  "{app.message.valid.size.max}", max = 300)
		String descripcion,
		@Schema(description = "Indica si el parametro esta activo.",example = "true")
		@NotNull(message = "{app.message.valid.not.null}")
		Boolean estBorrado,
		@Schema(description = "Indica si el parametro esta permite ser actualizado.",example = "true")
		@NotNull(message = "{app.message.valid.not.null}")
		Boolean permiteUpdate
		
) {}
