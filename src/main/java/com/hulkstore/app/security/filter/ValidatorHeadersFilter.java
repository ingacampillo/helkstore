package com.hulkstore.app.security.filter;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.hulkstore.app.config.AppConfig;
import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.util.ResponseUtil;
import com.hulkstore.app.util.WebUtil;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
/**
 * Clase de seguridad la cual es un filtro de request encargada validar los headers de las peticiones.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
@RequiredArgsConstructor
public class ValidatorHeadersFilter extends OncePerRequestFilter {

	private final WebUtil webUtil;
	private final ResponseUtil responseUtil;

	/**
    * Método encargado de inteceptar y validar los headers de los request de las peticiones.
    * 
    * @param request del tipo {@link HttpServletRequest}: Request interceptado a validar.
    * @param response del tipo {@link HttpServletResponse}: Response a interceptado a validar.
    * @param filterChain del tipo {@link FilterChain}: filterChain a interceptado.
    * @author </br>
    * Developer: Arnold Campillo.
    */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		webUtil.setDataInfoLog(request);
		Map<String, String> headers = this.webUtil.getHeadersRequest(request);

		String requestURI = request.getRequestURI();	
		
		if(webUtil.isPublicRouteSwagger(requestURI)||requestURI.contains(ConstantesApp.PUBLIC_RESOURCES)) {
			filterChain.doFilter(request, response);
			return;
		}
		
		String refer=headers.get(ConstantesApp.HEADER_REFERER.toUpperCase());
		boolean excludeOrigin=false;
		if(!ObjectUtils.isEmpty(refer)&&webUtil.isPublicRouteSwagger(refer)) {
			excludeOrigin= true;
		}
		
		if(!this.validLenguaje(headers,request, response,excludeOrigin)) {
			return;
		}
		
		if(!this.validCorsOrigin(headers,request, response,excludeOrigin)) {
			return;
		}
		
		if (!this.validHeaders(headers,ConstantesApp.BASIC_HEADERS, request, response,true)) {
			return;
		}
		if (!this.validHeaders(headers, ConstantesApp.AUTH_HEADERS,request, response,true)) {
			return;
		}

	    response.setCharacterEncoding(ConstantesApp.ENCODING_UTF8);
        response.setHeader("Access-Control-Allow-Origin", AppConfig.getCorsOriginString()); // Ajusta según tu configuración
        response.setHeader("Access-Control-Allow-Methods",ConstantesApp.ALLOWED_HEADERS_STRING);
        response.setHeader("Access-Control-Allow-Headers", ConstantesApp.FULL_HEADERS_STRING);
        response.setHeader("Access-Control-Allow-Credentials", "true");

		filterChain.doFilter(request, response);
	}

	/**
    * Método encargado de validar la existencia de los header requeridos del request de las peticiones.
    * @param headers del tipo {@link Map}<{@link String},{@link String}>: Mapa con los headers del request.
    * @param headersValid del tipo {@link Map}<{@link String},{@link String}>: Mapa con los headers requeridos.
    * @return String del tipo {@link String}: Resultado de los headers faltantes.  
   * @author </br>
    * Developer: Arnold Campillo.
    */
	private String validExistsHeaders(Map<String, String> headers, List<String> headersValid) {
		StringJoiner missingHeaders = new StringJoiner(", ");
		for (String header : headersValid) {
			if (!headers.containsKey(header.toUpperCase())) {				
					missingHeaders.add(header);
			}
		}
		return missingHeaders.length() > 0 ? missingHeaders.toString() : null;
	}
	
	/**
    * Método encargado de validar los corsOrigin del request de las peticiones.
    * @param headers del tipo {@link Map}<{@link String},{@link String}>: Mapa con los headers del request.
    * @param request del tipo {@link HttpServletRequest}: Request interceptado a validar.
    * @param response del tipo {@link HttpServletResponse}: Response a interceptado a validar.
    * @param excludeOrigin del tipo {@link boolean}: valida si excluye la validación de Cors.
    * @return boolean del tipo {@link boolean}: Resultado de la validación.  
    *
 * @author </br>
    * Developer: Arnold Campillo.
    */
	private boolean validCorsOrigin(Map<String, String> headers, HttpServletRequest request,
			HttpServletResponse response,boolean excludeOrigin) {
		/*if(excludeOrigin) {
			return true;
		}
		String valueCors=headers.get(ConstantesApp.HEADER_ORIGIN.toUpperCase());
		String refer=headers.get(ConstantesApp.HEADER_REFERER.toUpperCase());
		
		if(!ObjectUtils.isEmpty(refer)&&webUtil.isPublicRouteSwagger(refer)) {
			return true;
		}
		
		if(ObjectUtils.isEmpty(valueCors)) {
			this.responseUtil.buildErrorResponse(response, request, HttpServletResponse.SC_FORBIDDEN,
					MensajesEnum.HEADER_REQUIRED, new Object[] {ConstantesApp.HEADER_ORIGIN});
			return false;
		}
		
		if(!AppConfig.getCorsOrigin().contains(valueCors)) {
			this.responseUtil.buildErrorResponse(response, request, HttpServletResponse.SC_FORBIDDEN,
					MensajesEnum.CORS_ORIGIN_ERROR, new Object[] {});
			return false;
		}*/
		return true;
	}
	
	/**
    * Método encargado de validar el idioma[Lang] del request de las peticiones.
    * @param headers del tipo {@link Map}<{@link String},{@link String}>: Mapa con los headers del request.
    * @param request del tipo {@link HttpServletRequest}: Request interceptado a validar.
    * @param response del tipo {@link HttpServletResponse}: Response a interceptado a validar.
    * @param excludeLang del tipo {@link boolean}: valida si excluye la validación de de Lang.
    * @return boolean del tipo {@link boolean}: Resultado de la validación.  
    *
   * @author </br>
    * Developer: Arnold Campillo.
    */
	private boolean validLenguaje(Map<String, String> headers, HttpServletRequest request,
			HttpServletResponse response, boolean excludeLang) {
		if(excludeLang) {
			return true;
		}

		String valueLang=headers.get(ConstantesApp.HEADER_ACCEPT_LENGUAJE.toUpperCase());
		
		if(ObjectUtils.isEmpty(valueLang)) {
			this.responseUtil.buildErrorResponse(response, request, HttpServletResponse.SC_FORBIDDEN,
					MensajesEnum.HEADER_REQUIRED, new Object[] {ConstantesApp.HEADER_ACCEPT_LENGUAJE});
			return false;
		}
		
		List<String> langsAvailables=this.webUtil.langsAvailables();
		if(!langsAvailables.contains(valueLang.split("[,;]")[0])) {
			this.responseUtil.buildErrorResponse(response, request, HttpServletResponse.SC_FORBIDDEN,
					MensajesEnum.LANG_NO_SOPORT, new Object[] {ConstantesApp.HEADER_ACCEPT_LENGUAJE,valueLang,langsAvailables});
			return false;
		}
		
		return true;
	}
	
	/**
    * Método encargado de validar la estructura de los header requeridos del request de las peticiones.
    * @param invalidHeader del tipo {@link StringJoiner}: String con los headers que no cumplen con las validaciones.
    * @param header del tipo {@link String}:Header a validar.
    * @param head del tipo {@link String}: Header a validar del request.
    * @param lang del tipo {@link String}: Lang a aplicar a los mensajes.
    * 
   * @author </br>
    * Developer: Arnold Campillo.
    */
	private void validStructuresHeader(StringJoiner invalidHeader, String header, String head, String lang) {

		switch (header) {
			case ConstantesApp.HEADER_TRACE_CODE: {
				if(ObjectUtils.isEmpty(head)) {
					invalidHeader.add(this.webUtil.translate(MensajesEnum.HEADER_REQUIRED, new Object[] { ConstantesApp.HEADER_TRACE_CODE } ,lang));
				}
				break;
			}
			case ConstantesApp.HEADER_AUTHORIZATION: {
				if(ObjectUtils.isEmpty(head)) {
					invalidHeader.add(this.webUtil.translate(MensajesEnum.HEADER_REQUIRED, new Object[] { ConstantesApp.HEADER_AUTHORIZATION } ,lang));
				}
				break;
			}
			default:
				break;
		}
	}
	/**
    * Método encargado de validar la estructura de los header requeridos del request de las peticiones.
    * @param headers del tipo {@link Map}<{@link String},{@link String}>: Mapa con los headers del request.
    * @param headersValid del tipo {@link Map}<{@link String},{@link String}>: Mapa con los headers requeridos.
    * @param request del tipo {@link HttpServletRequest}: Request interceptado a validar.
    * @return String del tipo {@link String}: Resultado de los headers que no cumplen con las validaciones.  
   * @author </br>
    * Developer: Arnold Campillo..
    */
	private String validStructuresHeaders(Map<String, String> headers, List<String> headersValid, HttpServletRequest request) {
		StringJoiner invalidHeader = new StringJoiner(", ");
		String lang=this.webUtil.getLang(request);
		for (String header : headersValid) {
			String head=headers.get(header.toUpperCase());
			this.validStructuresHeader(invalidHeader, header, head,lang);
		}
		return invalidHeader.length() > 0 ? invalidHeader.toString() : null;
	}
	/**
    * Método encargado de validar la existencia de los header requeridos del request de las peticiones.
    * @param headers del tipo {@link Map}<{@link String},{@link String}>: Mapa con los headers del request.
    * @param headersValid del tipo {@link Map}<{@link String},{@link String}>: Mapa con los headers requeridos.
    * @param request del tipo {@link HttpServletRequest}: Request interceptado a validar.
    * @param response del tipo {@link HttpServletResponse}: Response a interceptado a validar.
    * @param excludeOrigin del tipo {@link boolean}: valida si excluye la validación de Cors.
    * @return boolean del tipo {@link boolean}: Resultado de la validación.  
    *
    * @author </br>
    * Developer: Arnold Campillo.
    */
	private boolean validHeaders(Map<String, String> headers, List<String> headersValid, HttpServletRequest request,
			HttpServletResponse response, boolean excludeOrigin){
		
		if(webUtil.isPublicRoute(request.getRequestURI()) && headersValid.equals(ConstantesApp.AUTH_HEADERS)) {
			return true;
		}
		List<String>headersValidRead=headersValid;
		if(excludeOrigin) {
			headersValidRead=headersValid.stream().filter(item->!item.equalsIgnoreCase(ConstantesApp.HEADER_ORIGIN)).toList();
		}
		String validExistsHeader = this.validExistsHeaders(headers,headersValidRead);
		if (!ObjectUtils.isEmpty(validExistsHeader)) {
			this.responseUtil.buildErrorResponse(response, request, HttpServletResponse.SC_UNAUTHORIZED,
					MensajesEnum.HEADERS_REQUIRED, new Object[] { validExistsHeader });
			return false;
		}

		String validStructureHeaders = this.validStructuresHeaders(headers, headersValidRead,request);
		if (!ObjectUtils.isEmpty(validStructureHeaders)) {
			this.responseUtil.buildErrorResponse(response, request, HttpServletResponse.SC_UNAUTHORIZED,
					MensajesEnum.HEADERS_STRUCTURE_INVALID, new Object[] { validStructureHeaders });
			return false;
		}
		return true;
	}


}
