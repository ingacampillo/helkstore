package com.hulkstore.app.security.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;
/**
 * DTO request que contiene la información solicitada para realizar el RefreshToken en el sistema.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Builder
@Schema(description = "DTO request que contiene la información solicitada para realizar el RefreshToken en el sistema.")
public record RefreshTokenRequestDTO(
		@NotNull(message = "{app.message.valid.not.null}")
		@NotBlank(message = "{app.message.valid.not.black}")
		@Size(message =  "{app.message.valid.size.min}", min = 1)
		@Schema(description = "token access a refrescar.",example = "564564sda645sa4654sd56as65d")
		String tokenAccess)
{

}
