package com.hulkstore.app.util;

import java.security.SecureRandom;

import org.apache.log4j.MDC;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.consts.ConstantesApp;

/**
 * Clase utilitaria encargada de gestionar metodos comúnes o utilitarios de la aplicación.
   * @author </br>
     * Developer: Arnold Campillo
 */
public class AppUtil {
	private AppUtil() {}
	
	 /**
     * Método encargado de asignar un valor al log.
     * @param key del tipo {@link String}: Key del log a asignar.
     * @param value del tipo {@link String}: Valor a asignar en el log.
        * @author </br>
     * Developer: Arnold Campillo
     */
	private static void addValueInLog(String key, String value) {
        MDC.put(key, value);
	}
	
	 /**
     * Método encargado de asignar un valor al log.
     * @param user del tipo {@link String}: Usuario a asignar en el log.
     * @param traceCode del tipo {@link String}: TraceCode a asignar en el log.
     * @param ip del tipo {@link String}: ip a asignar en el log.
        * @author </br>
     * Developer: Arnold Campillo
     */
	public static void addValueInLog(String user, String traceCode, String ip) {		
		addValueInLog(ConstantesApp.USER,ObjectUtils.isEmpty(user)?ConstantesApp.UNDEFINED:user);
		addValueInLog(ConstantesApp.TRACE_CODE,ObjectUtils.isEmpty(traceCode)?ConstantesApp.UNDEFINED:traceCode);
		addValueInLog(ConstantesApp.IP_CLIENT,ObjectUtils.isEmpty(ip)?ConstantesApp.UNDEFINED:ip);
	}
	
	 /**
     * Método encargado de asignar valores default al log.
        * @author </br>
     * Developer: Arnold Campillo
     */
	public static void addValueInLog() {		
		addValueInLog(ConstantesApp.USER,ConstantesApp.UNDEFINED);
		addValueInLog(ConstantesApp.TRACE_CODE,ConstantesApp.UNDEFINED);
		addValueInLog(ConstantesApp.IP_CLIENT,ConstantesApp.UNDEFINED);
	}
	
	 /**
     * Método encargado de generar password random.
     * @return String del tipo {@link String}: Password random.  
        * @author </br>
     * Developer: Arnold Campillo
     */
	public static String generatePassRandom() {
		String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		SecureRandom secureRandom = new SecureRandom();
		StringBuilder codigo = new StringBuilder(10);
		for (int i = 0; i < 10; i++) {
			int index = secureRandom.nextInt(caracteres.length());
			char caracter = caracteres.charAt(index);
			codigo.append(caracter);
		}
		return codigo.toString();
	}
	 /**
     * Método encargado de generar códigos de verificación random.
     * @return String del tipo {@link String}: Código de verificación random.  
        * @author </br>
     * Developer: Arnold Campillo
     */
	public static String generateCodeValidationRandom() {
		String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		SecureRandom secureRandom = new SecureRandom();
		StringBuilder codigo = new StringBuilder(15);
		for (int i = 0; i < 15; i++) {
			int index = secureRandom.nextInt(caracteres.length());
			char caracter = caracteres.charAt(index);
			codigo.append(caracter);
		}
		return codigo.toString();
	}
	

    
	
}
