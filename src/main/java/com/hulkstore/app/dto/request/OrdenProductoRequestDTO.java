package com.hulkstore.app.dto.request;

import com.hulkstore.app.enums.TipoTransaccionInventarioEnum;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;

/**
 * DTO request que contiene la información solicitada del inventario producto
 * 
 * @author </br>
 *         Developer: Arnold Campillo.
 */
@Builder
@Schema(description = "DTO request que contiene la información solicitada de inventario productos.")
public record OrdenProductoRequestDTO(
		@Schema(description = "Identificador del inventario producto.", example = "16551615615611") 
		Integer id,
		@Schema(description = "Identificador del producto.", example = "1")
		@NotNull(message = "El campo 'productoId' no puede ser nulo")
	    Integer productoId,
		@Schema(description = "Cantidad del inventario.", example = "1") 
		@NotNull(message = "{app.message.valid.not.null}")
		@NotBlank(message = "{app.message.valid.not.black}") 
		@Size(message = "{app.message.valid.size.min.max}", min = 0) 
		@Min(value = 0, message = "El campo 'cantidad' debe ser un número positivo mayor o igual a cero")
		String cantidad,
		@Schema(description = "Tipo de transaccion.",example = "COMPRA")
		@NotNull(message = "{app.message.valid.not.null}")
		TipoTransaccionInventarioEnum tipoTransaccion
		

) {
}
