package com.hulkstore.app.security.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Entity que gestiona la información acerca de los tokens asociados a los usuarios del sistema.
 * @author </br>
 * Developer: Arnold Campillo
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "usuario_token")
@EqualsAndHashCode(callSuper = false)
@ToString
public class UsuarioToken implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;
	
	@OneToOne(targetEntity = Usuario.class, fetch = FetchType.LAZY,optional = false)
	@JoinColumn(name = "id_usuario", referencedColumnName = "id", nullable = false)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Usuario usuario;

	@Basic(optional = false)
    @Column(name = "token" , nullable = false, length = 500)
	private String token;
	
	@Basic(optional = false)
	@Column(name = "token_expire", nullable = false, columnDefinition = "DATETIME")
	private LocalDateTime tokenExpire;

	@Basic(optional = false)
    @Column(name = "refresh_token" , nullable = false, length = 1000)
	private String refreshToken;
	
	@Basic(optional = false)
	@Column(name = "refresh_token_expire", nullable = false, columnDefinition = "DATETIME")
	private LocalDateTime refreshTokenExpire;
}
