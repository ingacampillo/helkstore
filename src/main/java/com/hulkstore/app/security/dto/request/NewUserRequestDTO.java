package com.hulkstore.app.security.dto.request;

import com.hulkstore.app.consts.ConstantesApp;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Builder;
/**
 * DTO request que contiene la información solicitada para realizar la creación de un nuevo usuario en el sistema.
 * @author </br>
 * Developer: Arnold Campillo.
 */
@Builder
@Schema(description = "DTO request que contiene la información solicitada para realizar la creación de un nuevo usuario en el sistema.")
public record NewUserRequestDTO(

	@NotNull(message = "{app.message.valid.not.null}")
	@NotBlank(message = "{app.message.valid.not.black}")
	@Size(message =  "{app.message.valid.size.min.max}", min = 10, max = 60)
	@Pattern(regexp = ConstantesApp.PATTERN_CORREO, message = "{app.message.valid.patter.correo}")
	@Schema(description = "Username del nuevo usuario.",example = "testgcr@test.com")
	String username,

	@NotNull(message = "{app.message.valid.not.null}")
	@NotBlank(message = "{app.message.valid.not.black}")
	@Size(message =  "{app.message.valid.size.min.max}", min = 10, max = 60)
	@Schema(description = "Password del nuevo usuario.",example = "testgcr")
	String password
) {
}
