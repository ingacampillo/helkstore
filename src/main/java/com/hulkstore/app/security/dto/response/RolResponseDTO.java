package com.hulkstore.app.security.dto.response;


import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
/**
 * DTO response que contiene la información acerca de los roles del sistema.
 * @author .</br>
 * Developer: Arnold Campillo.
 */
@Builder
@Schema(description = "DTO response que contiene la información acerca de los roles del sistema.")
public record RolResponseDTO(
		@Schema(hidden = true)
		@JsonIgnore
		Long id,
		@Schema(description = "Código del rol.",example = "ROL_11")
		String codigo,
		@Schema(description = "Valor del rol.",example = "ROL_01")
		String valor,
		@Schema(description = "Descripción del rol.",example = "ROL ASIGANDO")
		String descripcion,
		@Schema(description = "Indica si el rol esta activo.",example = "true")
		boolean enabled,	   
		@Schema(description = "Indica si el rol esta eliminado.",example = "true")
		@JsonIgnore
		boolean estBorrado
		) {

}
