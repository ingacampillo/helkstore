package com.hulkstore.app.service;

import java.util.List;

import com.hulkstore.app.dto.request.OrdenProductoRequestDTO;
import com.hulkstore.app.dto.response.OrdenProductoResponseDTO;
import com.hulkstore.app.entity.OrdenProducto;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
/**
 * Interfaz Service encargada de realizar las transacciones y operaciones de {@link OrdenProducto}.
 * @author </br>
 * Developer: Arnold Campillo
 */
public interface OrdenProductoService {
	
	/**
	 * Método encargado de obtener todos los InventarioProductos del sistema.
	 * 
	 * @return {@link List}<{@link OrdenProductoResponseDTO}>: listado de las InventarioProductos del
	 *         sistema.
	 * @author </br>
	 *         Developer: Arnold Campillo
	 * @throws
	 */
	List<OrdenProductoResponseDTO> findAll()
			throws ValidateServiceException, NoDataFoundException, GeneralServiceException;

	 /**
     * Método encargado de actualizar la InventarioProducto.
     * @param dto del tipo {@link OrdenProductoRequestDTO}: dto solicitado en la actualización.
     * @return {@link OrdenProductoResponseDTO}: InventarioProducto del sistema actualizado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	OrdenProductoResponseDTO update(OrdenProductoRequestDTO dto)throws ValidateServiceException;
	
	 /**
     * Método encargado de registrar la marca.
     * @param dto del tipo {@link OrdenProductoRequestDTO}: dto solicitado en el registro.
     * @return {@link OrdenProductoResponseDTO}: Marca del sistema registrado. 
     * @author </br>
     * Developer: Arnold Campillo
     * @throws
     */
	OrdenProductoResponseDTO insert(OrdenProductoRequestDTO dto)throws ValidateServiceException;
	
	
}
