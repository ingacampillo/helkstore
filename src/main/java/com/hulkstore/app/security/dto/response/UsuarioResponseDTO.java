package com.hulkstore.app.security.dto.response;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

/**
 * DTO response que contiene la información acerca de los usuarios del sistema.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Builder
@Schema(description = "Response del usuario")
public record UsuarioResponseDTO(
		@Schema(hidden = true)
		@JsonIgnore
		Long id,
		@Schema(description = "Username del usuario.",example = "USER11")
		String username,
		@Schema(hidden = true)
		@JsonIgnore
		String password,
		@Schema(description = "Usercode del usuario.",example = "USER11")
		String usercode,
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
		@Schema(description = "Fecha de expiración del token generado en la autenticación.",example = "0000-00-00 00:00")
		LocalDateTime tokenExpire,
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
		@Schema(description = "Fecha de expiración del token refresh generado en la autenticación.",example = "0000-00-00 00:00")
		LocalDateTime refreshTokenExpire,
		@Schema(description = "Roles los cuales tiene asignados el usuario.",example = "[ADMIN,USER]")
		List<RolResponseDTO> roles,
		@Schema(description = "Indica si el usuario debe cambiar la contraseña.",example = "true")
	    boolean forceChangePass,
	    @Schema(description = "Indica si el usuario esta activo.",example = "true")
	    boolean enabled,
	    @Schema(description = "Indica si el usuario esta bloqueado por superar el limite de intentos al digitar la password.",example = "false")
	    boolean bloquedByInvalidPass,
		@Schema(description = "Indica si el usuario esta eliminado.",example = "true")
		@JsonIgnore
		boolean estBorrado
) {
	
}
