package com.hulkstore.app.adapter;

import java.util.List;
import java.util.function.Function;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.dto.request.OrdenProductoRequestDTO;
import com.hulkstore.app.dto.response.OrdenProductoResponseDTO;
import com.hulkstore.app.entity.OrdenProducto;
import com.hulkstore.app.entity.Producto;

/**
 * Adaptador encargado de definir la manera en que se deben convertir
 * objetos entre Entity y DTOs (Y viceversa).
 * @author </br>
 * Developer: Arnold Campillo.
 */
public class OrdenProductoAdapter {
	
	private OrdenProductoAdapter() {}
	
	  /**
     * Función encargada de adaptar un objeto DTO Request a su respectivo Entity(de {@link OrdenProductoRequestDTO} a {@link OrdenProducto}).
     * @param dto del tipo {@link OrdenProductoRequestDTO}: Dto Request a convertir a Entity.  
     * @return {@link OrdenProducto}: Entity resultado de la conversión. 
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static final Function<OrdenProductoRequestDTO, OrdenProducto> requestToEntity = (OrdenProductoRequestDTO dto) -> {
		if (ObjectUtils.isEmpty(dto)) {
			return null;
		}  
		
        return OrdenProducto.builder()
        		.id(dto.id())
        		.producto(Producto.builder().id(dto.productoId()).build())
        		.cantidad(Integer.valueOf(dto.cantidad()))
        		.tipoTransaccion(dto.tipoTransaccion())
				.build();
    };
    
	  /**
     * Función encargada de adaptar un objeto Entity a su respectivo DTO Response(de {@link OrdenProducto} a {@link OrdenProductoResponseDTO}).
     * @param entity del tipo {@link OrdenProducto}: Entity a convertir a DTO Response. 
     * @return {@link OrdenProductoResponseDTO}: DTO Response resultado de la conversión. 
     * @author </br>
     * Developer: Arnold Campillo
     */
    public static final Function<OrdenProducto, OrdenProductoResponseDTO> entityToResponse = (OrdenProducto entity) -> {
		if (ObjectUtils.isEmpty(entity)) {
			return null;
		}  
		
		return OrdenProductoResponseDTO.builder()
        		.id(entity.getId())
        		.producto(ProductoAdapter.entityToResponse.apply(entity.getProducto()))
        		.cantidad(entity.getCantidad())
        		.fecha(entity.getFecha())
        		.tipoTransaccion(entity.getTipoTransaccion())
				.build();
    };
    
	  /**
     * Función encargada de adaptar un listado Entity a su respectivo listado DTO Response(de {@link List}<{@link OrdenProducto}> a {@link List}<{@link OrdenProductoResponseDTO}>).
     * @param entityList del tipo {@link List}<{@link OrdenProducto}>: Entity List a convertir a DTO Response List. 
     * @return {@link List}<{@link OrdenProductoResponseDTO}>: DTO Response List resultado de la conversión. 
    * @author </br>
     * Developer: Arnold Campillo
     */
    
    public static final List<OrdenProductoResponseDTO> listEntityToResponse(List<OrdenProducto> entityList) {
    	if (CollectionUtils.isEmpty(entityList)) {
    		return List.of();
    	} 
        return entityList.stream()
                .map(entityToResponse)
               .toList();
    }

}
