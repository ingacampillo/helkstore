package com.hulkstore.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.hulkstore.app.consts.ConstantesApp;


@Configuration
public class ResourceConfiguration implements WebMvcConfigurer {
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {      
        String pathExternalFileSystem = AppConfig.getFileSystemApp();
        registry.addResourceHandler(ConstantesApp.PUBLIC_RESOURCES+"/**").addResourceLocations("file:"+pathExternalFileSystem+"/");
    }
}
