package com.hulkstore.app.security.service.impl;

import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;

import com.hulkstore.app.config.AppConfig;
import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.dto.response.AuthUserResponseDTO;
import com.hulkstore.app.security.enums.TypeTokenEnum;
import com.hulkstore.app.security.service.JwtServices;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;

/**
 * Intefaz Service encargado de realizar las transacciones y operaciones de JWT. Implementación {@link JwtServices}.
 * @author </br>
    * Developer: Arnold Campillo.
 */
@Service
public class JwtServicesImpl implements JwtServices {


	private String secret;
	private int jwtTokenExpiration;	
	private int jwtTokenRefreshExpiration;	
	private int sessionInactiveExpireMinutes;
	private int tokenRefreshAttempts;
	
	
	 /**
	 * Método encargado de inicializar las variables requeridas para la clase.
	 * @author </br>
    * Developer: Arnold Campillo.
	 */
	@PostConstruct
	protected void init() {		
		jwtTokenExpiration=AppConfig.getJwtTokenExpiration();
		jwtTokenRefreshExpiration=AppConfig.getJwtTokenRefreshExpiration();
		sessionInactiveExpireMinutes=AppConfig.getJwtSessionInactiveExpireMinutes();
		tokenRefreshAttempts=AppConfig.getJwtTokenRefressAttempts();
		secret = Base64.getEncoder().encodeToString(AppConfig.getJwtSecret().getBytes());		
	}

	
	 /**
	 * Método encargado de crear los tokens de acceso.
	 * @param user del tipo {@link AuthUserResponseDTO}: Usuario a asignar el token.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario Configurado con los tokens.  
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	@Override
	public AuthUserResponseDTO createToken(AuthUserResponseDTO user) {	
	    return generateToken(user,0);
	}
	
	 /**
	 * Método encargado de crear los tokens de acceso.
	 * @param user del tipo {@link AuthUserResponseDTO}: Usuario a asignar el token.
	 * @param attemptsRefreshToken del tipo {@link int}: intentos para usar el token refresh.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario Configurado con los tokens.  
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	private AuthUserResponseDTO generateToken(AuthUserResponseDTO user,int attemptsRefreshToken) {
		Date now = new Date();
	    Date accessTokenExpiration = DateUtils.addMinutes(now, jwtTokenExpiration);
	    Date refreshTokenExpiration =DateUtils.addMinutes(now, jwtTokenRefreshExpiration);
	    
		// Genera el Token de Acceso
	    String accessToken = Jwts.builder()
	            .setClaims(loadClaimsToken(user))
	            .setIssuedAt(now)
	            .setExpiration(accessTokenExpiration)
	            .signWith(SignatureAlgorithm.HS256, secret)
	            .compact();
	    
		 // Genera el Refresh Token
	    String refreshToken = Jwts.builder()
	            .setClaims(loadClaimsTokenRefresh(user,accessToken,attemptsRefreshToken)) 
	            .setIssuedAt(now)
	            .setExpiration(refreshTokenExpiration)
	            .signWith(SignatureAlgorithm.HS256, secret)
	            .compact();
	  
	    return AuthUserResponseDTO.builder()
	    		.id(user.id())
	    		.username(user.username())
	    		.usercode(user.usercode())
	    	    .token(accessToken)
	    	    .tokenExpire(accessTokenExpiration.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
	    	    .refreshToken(refreshToken)
	    	    .refreshTokenExpire(refreshTokenExpiration.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
	    	    .sessionInactiveExpireMinutes(sessionInactiveExpireMinutes)
	    	    .enabled(user.enabled())
	    	    .forceChangePass(user.forceChangePass())
	    	    .bloquedByInvalidPass(user.bloquedByInvalidPass())
	    	    .roles(user.roles())
	    	    .build();
	}

	 /**
	 * Método encargado de asignar propiedades o valores al token access.
	 * @param user del tipo {@link AuthUserResponseDTO}: Usuario a asignar el token.
	 * @return claims del tipo {@link Map}<{@link String},{@link Object}> : Map con las propiedades o valores asignados al token access.  
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	private Map<String,Object>loadClaimsToken(AuthUserResponseDTO user){
		 Map<String,Object> claims=loadClaims(user);
		 claims.put(ConstantesApp.TOKEN_TYPE,TypeTokenEnum.ACCESS.name());
		return claims;
	}
	 /**
	 * Método encargado de asignar propiedades o valores al refresh token.
	 * @param user del tipo {@link AuthUserResponseDTO}: Usuario a asignar el token.
	 * @param token del tipo {@link String}: Token access a relacionar al refresh token.
	 * @param attempts del tipo {@link int}: Intentos de uso a asignar al refresh token.
	 * @return claims del tipo {@link Map}<{@link String},{@link Object}> : Map con las propiedades o valores asdignados al refresh token.  
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	private Map<String,Object>loadClaimsTokenRefresh(AuthUserResponseDTO user,String token,int attempts){
		 Map<String,Object> claims=loadClaims(user);
		 claims.put(ConstantesApp.TOKEN_TYPE, TypeTokenEnum.REFRESH.name());
		 claims.put(ConstantesApp.TOKEN_ACCESS,token);
		 claims.put(ConstantesApp.TOKEN_REFRESH_ATTEMPTS,attempts);
		return claims;
	}
	
	 /**
	 * Método encargado de asignar propiedades o valores genericos al token.
	 * @param user del tipo {@link AuthUserResponseDTO}: Usuario a asignar el token.
	 * @return claims del tipo {@link Map}<{@link String},{@link Object}> : Map con las propiedades o valores asignados al token.  
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	private Map<String, Object> loadClaims(AuthUserResponseDTO user) {
		Map<String, Object> claims = Jwts.claims().setSubject(user.username());		
		claims.put("id", user.id());
		claims.put("usercode", user.usercode());
		return claims;
	}
	
	 /**
	 * Método encargado de validar el token de acceso.
	 * @param token del tipo {@link String}: Token de acceso a validar.
	 * @return result del tipo {@link boolean}: resultado de la validación.   
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public boolean validateAccessToken(String token) throws ValidateServiceException{
		try {
	        Jwts.parser().setSigningKey(secret).parseClaimsJws(token);	
	        isTokenValid(token,TypeTokenEnum.ACCESS.name());
	        isTokenExpired(token,TypeTokenEnum.ACCESS.name());	
	        return true;
		}catch (ExpiredJwtException e) {
			throw new ValidateServiceException(MensajesEnum.TOKEN_EXPIRED);
		}

	}
	
	/**
	 * Método encargado de validar el refresh token.
	 * @param token del tipo {@link String}: Refresh token de acceso a validar.
	 * @return result del tipo {@link boolean}: resultado de la validación.   
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public boolean validateRefreshToken(String token) throws ValidateServiceException{
		try {
		    Jwts.parser().setSigningKey(secret).parseClaimsJws(token);		    
		    isTokenValid(token,TypeTokenEnum.REFRESH.name());	  
		    isTokenExpired(token,TypeTokenEnum.REFRESH.name());	
		    validAttemptsRefreshToken(token);
	        return true;	   
		}catch (ExpiredJwtException e) {
			throw new ValidateServiceException(MensajesEnum.TOKEN_REFRESH_EXPIRED);
		}
	}
	
	/**
	 * Método encargado de obtener el username del token.
	 * @param token del tipo {@link String}: token de acceso a validar.
	 * @return result del tipo {@link String}: username obtenido del token.   
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public String getUserNameFromToken(String token) throws ValidateServiceException {
		return getUserNameFromTokens(token,true);
	}
	
	/**
	 * Método encargado de obtener el username del refresh token.
	 * @param token del tipo {@link String}: refresh token a validar.
	 * @return result del tipo {@link String}: username obtenido del refresh token.   
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public String getUserNameFromRefreshToken(String token)  throws ValidateServiceException{
		return getUserNameFromTokens(token,false);
	}
	
	/**
	 * Método encargado de obtener el username de los tokens.
	 * @param token del tipo {@link String}: token a validar.
	 * @param isTokenAccess del tipo {@link boolean}: determina si es token access o refresh token.
	 * @return result del tipo {@link String}: username obtenido del refresh token.   
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private String getUserNameFromTokens(String token, boolean isTokenAccess) throws ValidateServiceException{
		try {
			return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject().split(",")[0];
		} catch (ExpiredJwtException e) {
			throw new ValidateServiceException(isTokenAccess?MensajesEnum.TOKEN_EXPIRED:MensajesEnum.TOKEN_REFRESH_EXPIRED);
		}catch (Exception e) {
			throw new ValidateServiceException(MensajesEnum.TOKEN_INCORRECT);
		}
	}

	/**
	 * Método encargado de validar si los tokens ya expiraron.
	 * @param token del tipo {@link String}: token a validar.
	 * @param tokenType del tipo {@link String}: Tipo del token.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private void isTokenExpired(String token, String tokenType) throws ValidateServiceException{
		Date expiration = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getExpiration();
		if(expiration.before(new Date())) {
			throw new ValidateServiceException(tokenType.equals(TypeTokenEnum.ACCESS.name())?MensajesEnum.TOKEN_EXPIRED:MensajesEnum.TOKEN_REFRESH_EXPIRED);
		}		
	}
	
	/**
	 * Método encargado de obtener el el tipo del token {@link TypeTokenEnum}.
	 * @param token del tipo {@link String}: token a validar.
	 * @return result del tipo {@link String}: tipo del token obtenido.   
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public String getTokenType(String token)  throws  ValidateServiceException{
		try {
			return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().get(ConstantesApp.TOKEN_TYPE).toString();
		}catch (ExpiredJwtException e) {
			throw new ValidateServiceException(MensajesEnum.TOKEN_EXPIRED);
		} catch (Exception e) {
			throw new ValidateServiceException(MensajesEnum.TOKEN_INCORRECT);
		}
	}
		
	/**
	 * Método encargado de validar si los tokens son validos.
	 * @param token del tipo {@link String}: token a validar.
	 * @param tokenType del tipo {@link String}: Tipo del token.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private void isTokenValid(String token, String tokenType) throws  ValidateServiceException {
		String type = getTokenType(token);
		if(!type.equals(tokenType)) {
			throw new ValidateServiceException(tokenType.equals(TypeTokenEnum.ACCESS.name())?MensajesEnum.TOKEN_INCORRECT:MensajesEnum.TOKEN_REFRESH_INCORRECT);
		}		
	}

	/**
	 * Método encargado de obtener la cantidad de usos del el refresh token.
	 * @param token del tipo {@link String}: token de acceso a validar.
	 * @return attempts del tipo {@link int}: cantidad de usos del refresh token.   
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
		
	private int getAttemptsRefreshToken(String token) {
		return (int) Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().get(ConstantesApp.TOKEN_REFRESH_ATTEMPTS);
	}
	
	/**
	 * Método encargado de validar si el refresh token ya supero el limite de uso.
	 * @param token del tipo {@link String}: token a validar.
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private void validAttemptsRefreshToken(String token) throws  ValidateServiceException {
		 int attempts=getAttemptsRefreshToken(token);
		 if(attempts>=tokenRefreshAttempts) {
			 throw new ValidateServiceException(MensajesEnum.TOKEN_REFRESH_ATTEMPTS);
		 }
	}

	/**
	 * Método encargado de validar el refresh token sea del token generado.
	 * @param token del tipo {@link String}: token de acceso a validar.
	 * @param tokenRefresh del tipo {@link String}: Refresh token de acceso a validar.
	 * @return result del tipo {@link boolean}: resultado de la validación.   
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public boolean validateRefreshToken(String token, String tokenRefresh) throws ValidateServiceException{
		validateRefreshToken(tokenRefresh);
		validateAccessToken(token);		
		String tokenInternal=(String) Jwts.parser().setSigningKey(secret).parseClaimsJws(tokenRefresh).getBody().get(ConstantesApp.TOKEN_ACCESS);
		if(!token.equals(tokenInternal)) {
			 throw new ValidateServiceException(MensajesEnum.TOKEN_REFRESH_NO_CORRECT);
		}		
		return true;
	}

	 /**
	 * Método encargado de renovar los tokens de acceso.
	 * @param user del tipo {@link AuthUserResponseDTO}: Usuario a renovar los tokens.
	 * @param tokenRefresh del tipo {@link String}: Refresh token.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario Configurado con los tokens.  
	 * @author </br>
    * Developer: Arnold Campillo.
	 */
	@Override
	public AuthUserResponseDTO renovateToken(AuthUserResponseDTO user,String tokenRefresh) {
		return generateToken(user,getAttemptsRefreshToken(tokenRefresh)+1);
	}
	
}
