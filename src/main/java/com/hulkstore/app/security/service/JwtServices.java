package com.hulkstore.app.security.service;

import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.dto.response.AuthUserResponseDTO;
import com.hulkstore.app.security.enums.TypeTokenEnum;
/**
 * Intefaz Service encargado de realizar las transacciones y operaciones de JWT.
 * @author </br>
    * Developer: Arnold Campillo.
 */
public interface JwtServices {
	
	 /**
	 * Método encargado de crear los tokens de acceso.
	 * @param user del tipo {@link AuthUserResponseDTO}: Usuario a asignar el token.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario Configurado con los tokens.  
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	public AuthUserResponseDTO createToken(AuthUserResponseDTO user);
	
	/**
	 * Método encargado de validar el token de acceso.
	 * @param token del tipo {@link String}: Token de acceso a validar.
	 * @return result del tipo {@link boolean}: resultado de la validación.   
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	public boolean validateAccessToken(String token) throws ValidateServiceException;
	
	/**
	 * Método encargado de validar el refresh token.
	 * @param token del tipo {@link String}: Refresh token de acceso a validar.
	 * @return result del tipo {@link boolean}: resultado de la validación.   
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	public boolean validateRefreshToken(String token) throws ValidateServiceException;
	
	/**
	 * Método encargado de validar el refresh token sea del token generado.
	 * @param token del tipo {@link String}: token de acceso a validar.
	 * @param tokenRefresh del tipo {@link String}: Refresh token de acceso a validar.
	 * @return result del tipo {@link boolean}: resultado de la validación.   
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	public boolean validateRefreshToken(String token,String tokenRefresh) throws ValidateServiceException;
	
	/**
	 * Método encargado de obtener el username del token.
	 * @param token del tipo {@link String}: token de acceso a validar.
	 * @return result del tipo {@link String}: username obtenido del token.   
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	public String getUserNameFromToken(String token) throws ValidateServiceException;
	
	/**
	 * Método encargado de obtener el username del refresh token.
	 * @param token del tipo {@link String}: refresh token a validar.
	 * @return result del tipo {@link String}: username obtenido del refresh token.   
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	public String getUserNameFromRefreshToken(String token) throws ValidateServiceException;
	
	/**
	 * Método encargado de obtener el el tipo del token {@link TypeTokenEnum}.
	 * @param token del tipo {@link String}: token a validar.
	 * @return result del tipo {@link String}: tipo del token obtenido.   
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	public String getTokenType(String token) throws ValidateServiceException;
	
	 /**
	 * Método encargado de renovar los tokens de acceso.
	 * @param user del tipo {@link AuthUserResponseDTO}: Usuario a renovar los tokens.
	 * @param tokenRefresh del tipo {@link String}: Refresh token.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario Configurado con los tokens.  
	 * @author </br>
    * Developer: Arnold Campillo.
	 */
	public AuthUserResponseDTO renovateToken(AuthUserResponseDTO user,String tokenRefresh);
	

}
