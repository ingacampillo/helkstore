package com.hulkstore.app.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.adapter.ParametroSistemaAdapter;
import com.hulkstore.app.config.AppConfig;
import com.hulkstore.app.dto.request.ParametroSistemaRequestDTO;
import com.hulkstore.app.dto.response.ParametroSistemaResponseDTO;
import com.hulkstore.app.entity.ParametroSistema;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.GeneralServiceException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.repository.ParametroSistemaRepository;
import com.hulkstore.app.service.ParametroSistemaService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * Service encargado de realizar las transacciones y operaciones de {@link ParametroSistema}. Implementación {@link ParametroSistemaService}.
  * @author </br>
  * Developer: Arnold Campillo
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ParametroSistemaServiceImpl implements ParametroSistemaService {
	
	private final ParametroSistemaRepository repository;
	 /**
     * Método encargado de obtener todos los parámetros del sistema.
     * @return {@link List}<{@link ParametroSistemaResponseDTO}>: listado de los parámetros del sistema. 
     * @author </br>
	 * Developer: Arnold Campillo
     * @throws
     */
	@Override
	public List<ParametroSistemaResponseDTO> findAll() throws ValidateServiceException, NoDataFoundException, GeneralServiceException{
		try {
			List<ParametroSistema> listadoParametros = this.repository.findAll();			
			return ParametroSistemaAdapter.listEntityToResponse(listadoParametros);
		} catch (ValidateServiceException | NoDataFoundException e) {
			throw e;		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(MensajesEnum.ERROR, e);
		}
	}
	 /**
     * Método encargado de obtener todos los parámetros del sistema.
     * @return {@link Map}<{@link String},{@link String}>: Map de los parámetros del sistema. 
     * @author </br>
	 * Developer: Arnold Campillo
     * @throws
     */
	@Override
	public Map<String, String> obtenerParametros() throws ValidateServiceException, NoDataFoundException, GeneralServiceException {
		try {
			List<ParametroSistemaResponseDTO> listadoParametros = this.findAll();
			if(CollectionUtils.isEmpty(listadoParametros)) {
				return Map.of();
			}
			return listadoParametros.stream().collect(
					Collectors.toMap(ParametroSistemaResponseDTO::codigo, ParametroSistemaResponseDTO::valor));
		} catch (ValidateServiceException | NoDataFoundException e) {
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(MensajesEnum.ERROR, e);
		}
	}
	 /**
     * Método encargado de obtener el parámetro del sistema por código.
     * @param codigo del tipo {@link String}: codigo solicitado en la consulta.
     * @return {@link ParametroSistemaResponseDTO}: Parámetro del sistema obtenido. 
     * @author </br>
	 * Developer: Arnold Campillo.
     * @throws
     */
	@Override
	public ParametroSistemaResponseDTO getParametroByCodigo(String codigo) throws ValidateServiceException, NoDataFoundException, GeneralServiceException {
		try {
			ParametroSistema opMarca = repository.findByCodigo(codigo).orElseThrow();
			return ParametroSistemaAdapter.entityToResponse.apply(opMarca);
		} catch (ValidateServiceException | NoDataFoundException e) {
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(MensajesEnum.ERROR, e);
		}
	}
	 /**
     * Método encargado de actualizar el parámetro del sistema.
     * @param dto del tipo {@link ParametroSistemaRequestDTO}: dto solicitado en la actualización.
     * @return {@link ParametroSistemaResponseDTO}: Parámetro del sistema actualizado. 
     * @author </br>
	 * Developer: Arnold Campillo
     * @throws
     */
	@Override
	public ParametroSistemaResponseDTO update(ParametroSistemaRequestDTO dto) throws ValidateServiceException{
		if(ObjectUtils.isEmpty(dto)) {
			return null;
		}
		if(ObjectUtils.isEmpty(dto.id())) {
			throw new ValidateServiceException(MensajesEnum.ID_REQUIRED);
		}		
		this.validarPermiteUpdate(dto);
		this.validarCodigoUnico(dto,true);
		return this.save(dto);
	}

	 /**
     * Método encargado de registrar el parámetro del sistema.
     * @param dto del tipo {@link ParametroSistemaRequestDTO}: dto solicitado en el registro.
     * @return {@link ParametroSistemaResponseDTO}: Parámetro del sistema registrado. 
    * @author </br>
	 * Developer: Arnold Campillo
     * @throws
     */
	@Override
	public ParametroSistemaResponseDTO insert(ParametroSistemaRequestDTO dto) throws ValidateServiceException{
		this.validarCodigoUnico(dto,false);
		return this.save(dto);
	}
	 /**
     * Método encargado de registrar o actualizar el parámetro del sistema.
     * @param dto del tipo {@link ParametroSistemaRequestDTO}: dto solicitado en el registro o actualización.
     * @return {@link ParametroSistemaResponseDTO}: Parámetro del sistema registrado o actualizado. 
     * @author </br>
	 * Developer: Arnold Campillo
     * @throws
     */
	private ParametroSistemaResponseDTO save(ParametroSistemaRequestDTO dto) {
		ParametroSistema entity=ParametroSistemaAdapter.requestToEntity.apply(dto);
		if(!ObjectUtils.isEmpty(dto.id())) {
			ParametroSistemaResponseDTO dtoTmp=getParametroByCodigo(dto.codigo());
			entity.setCodigo(dtoTmp.codigo());
		}	
		entity.setEstBorrado(false);
		ParametroSistema param=repository.save(entity);
		AppConfig.replaceParamSystem(dto.codigo(), dto.valor());
		return ParametroSistemaAdapter.entityToResponse.apply(param);
	}
	
	 /**
     * Método encargado de validar el código del parametro no este registrado.
     * @param param del tipo {@link ParametroSistemaRequestDTO}: dto solicitado en la validación.
     * @param isUpdate del tipo {@link boolean}: Verifica que es un proceso de actualizaión o inserción.
     * @author </br>
	 * Developer: Arnold Campillo
     * @throws
     */

	private void validarCodigoUnico(ParametroSistemaRequestDTO param,boolean isUpdate) throws ValidateServiceException{
		boolean isValid=true;
		if(isUpdate) {
			if (!ObjectUtils.isEmpty(param.id())
					&& Boolean.TRUE.equals(repository.existsByCodigoAndIdNot(param.codigo(),param.id()))) {
				isValid=false;
				
			}
		}else {
			if (Boolean.TRUE.equals(repository.existsByCodigo(param.codigo()))) {
				isValid=false;
			}
		}		
		if(!isValid) {
			throw new ValidateServiceException(MensajesEnum.CODE_ALREADY_REGISTERED,param.codigo());
		}
	}
	
	 /**
     * Método encargado de validar  si el paramatro a actualizar permite la actualización .
     * @param param del tipo {@link ParametroSistemaRequestDTO}: dto solicitado en la validación.
     * @author </br>
	 * Developer: Arnold Campillo
     * @throws
     */

	private void validarPermiteUpdate(ParametroSistemaRequestDTO param) throws ValidateServiceException{
		ParametroSistemaResponseDTO  response=getParametroByCodigo(param.codigo());
		if(Boolean.FALSE.equals(response.permiteUpdate())) {
			throw new ValidateServiceException(MensajesEnum.REGISTRY_NO_UPDATES);
		}
	}


}
