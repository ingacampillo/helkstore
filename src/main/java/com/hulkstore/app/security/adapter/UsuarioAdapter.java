package com.hulkstore.app.security.adapter;

import java.util.List;
import java.util.function.Function;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.security.dto.response.AuthUserResponseDTO;
import com.hulkstore.app.security.dto.response.RolResponseDTO;
import com.hulkstore.app.security.dto.response.UsuarioResponseDTO;
import com.hulkstore.app.security.entity.Rol;
import com.hulkstore.app.security.entity.Usuario;
/**
 * Adaptador encargado de definir la manera en que se deben convertir
 * objetos entre Entity y DTOs (Y viceversa).
 * @author </br>
     * Developer: Arnold Campillo
 */
public class UsuarioAdapter {

	private UsuarioAdapter() {}

	 /**
     * Función encargada de adaptar un objeto DTO Response a su respectivo Auth DTO Response(de {@link UsuarioResponseDTO} a {@link AuthUserResponseDTO}).
     * @param response del tipo {@link UsuarioResponseDTO}: DTO Response a convertir a Auth DTO Response. 
     * @return {@link AuthUserResponseDTO}: Auth DTO Response resultado de la conversión. 
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static final Function<UsuarioResponseDTO, AuthUserResponseDTO> responseToResponseAuth = (UsuarioResponseDTO response) -> {
		if (ObjectUtils.isEmpty(response)) {
			return null;
		}  
        return AuthUserResponseDTO.builder()
        		.id(response.id())
	    		.username(response.username())
	    		.usercode(response.usercode())
	    	    .enabled(response.enabled())
	    	    .forceChangePass(response.forceChangePass())
	    	    .bloquedByInvalidPass(response.bloquedByInvalidPass())
	    	    .roles(CollectionUtils.isEmpty(response.roles())?List.of():response.roles().stream().map(RolResponseDTO::valor).toList())
				.build();
    };
    
	 /**
     * Función encargada de adaptar un objeto Entity a su respectivo DTO Response(de {@link Usuario} a {@link UsuarioResponseDTO}).
     * @param entity del tipo {@link Usuario}: Entity a convertir a DTO Response. 
     * @return {@link UsuarioResponseDTO}: DTO Response resultado de la conversión. 
    * @author </br>
     * Developer: Arnold Campillo
     */
	public static final Function<Usuario, UsuarioResponseDTO> entityToResponse = (Usuario entity) -> {
		if (ObjectUtils.isEmpty(entity)) {
			return null;
		}  
        return UsuarioResponseDTO.builder()
        		.id(entity.getId())
	    		.username(entity.getUsername())
	    		.usercode(entity.getUsercode())
	    		.password(entity.getPassword())
	    	    .enabled(entity.isEnabled())
	    	    .forceChangePass(entity.isForceChangePass())
	    	    .bloquedByInvalidPass(entity.isBloquedByInvalidPass())
	    	    .roles(CollectionUtils.isEmpty(entity.getRoles())?List.of():RolAdapter.entityListToResponseList.apply(entity.getRoles()))
				.build();
    };
    
	 /**
     * Función encargada de adaptar un objeto DTO Response a su respectivo Entity(de {@link UsuarioResponseDTO} a {@link Usuario}).
     * @param response del tipo {@link UsuarioResponseDTO}: DTO Response a convertir a Entity. 
     * @return {@link Usuario}: Entity resultado de la conversión. 
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static final Function<UsuarioResponseDTO,Usuario> responseToEntity = (UsuarioResponseDTO response) -> {
		if (ObjectUtils.isEmpty(response)) {
			return null;
		}  
        return Usuario.builder()
	    		.id(response.id())
	    		.username(response.username())
	    		.usercode(response.usercode())
	    		.password(response.password())
	    		.forceChangePass(response.forceChangePass())
	    		.bloquedByInvalidPass(response.bloquedByInvalidPass())
	    		.enabled(response.enabled())
	    		.estBorrado(response.estBorrado())
	    		.roles(response.roles().stream().map(item->Rol.builder().id(item.id()).build()).toList())
	    		.build();
    };
    
	  /**
     * Función encargada de adaptar un listado Entity a su respectivo listado DTO Response(de {@link List}<{@link Usuario}> a {@link List}<{@link UsuarioResponseDTO}>).
     * @param entityList del tipo {@link List}<{@link Usuario}>: Entity List a convertir a DTO Response List. 
     * @return {@link List}<{@link UsuarioResponseDTO}>: DTO Response List resultado de la conversión. 
     * @author </br>
     * Developer: Arnold Campillo
     */
    
    public static final Function<List<Usuario>, List<UsuarioResponseDTO>> entityListToResponseList = (List<Usuario> entityList) -> {
		if (CollectionUtils.isEmpty(entityList)) {
			return null;
		}  
        return entityList.stream().map(entityToResponse::apply).toList();
    };
    
}
