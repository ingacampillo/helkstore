package com.hulkstore.app.util;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.WebRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hulkstore.app.config.AppConfig;
import com.hulkstore.app.config.ErrorHandlerConfig;
import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.enums.CategoriaMensajesEnum;
import com.hulkstore.app.enums.ClaseMensajesEnum;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.security.AuthenticationEntryPointConfig;
import com.hulkstore.app.security.controller.dto.WrapperResponse;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * Clase utilitaria encargada de gestionar la respuestas del response.
 * @author </br>
     * Developer: Arnold Campillo
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ResponseUtil {
	
	private final WebUtil webUtil;
	private ObjectMapper objectMapper = new ObjectMapper();

	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta del response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @param body del tipo {@link T}: Body a asignar al response.
     * @param isOk del tipo {@link boolean}: valor a asignar al response para determinar si la petición se realizó correctamente.
     * @param e del tipo {@link Exception}: Error presentado a asignar al response.
     * @param request del tipo {@link HttpServletRequest}: Request para tratar y obtener información relevante para el response.
     * @return WrapperResponse del tipo {@link WrapperResponse}: Wrapper construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	private <T> WrapperResponse<T> buildResponseWrapper(MensajesEnum message,Object[] paramMessage,T body,boolean isOk,Exception e,HttpServletRequest request){
		String lang=ConstantesApp.LOCALE_DEFAULT;
		if(!ObjectUtils.isEmpty(request)) {
			lang=this.webUtil.getLang(request);
		}	
		String msg=this.webUtil.translate(message,paramMessage,lang);	
		String trace=this.webUtil.getTraceCode(request);
		if(message.equals(MensajesEnum.OK)) {
			log.info(msg,(!ObjectUtils.isEmpty(e)?e:null));
		}else {
			log.error(msg,(!ObjectUtils.isEmpty(e)?e:null));	
		}
		return new  WrapperResponse<>(isOk, msg, message.getCode(), message.getCategory().name(), message.getClas().name(), trace, body);
	}
	
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta del response a patir de {@link ErrorHandlerConfig}.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @param body del tipo {@link T}: Body a asignar al response.
     * @param isOk del tipo {@link boolean}: valor a asignar al response para determinar si la petición se realizó correctamente.
     * @param e del tipo {@link Exception}: Error presentado a asignar al response.
     * @param request del tipo {@link WebRequest}: Request para tratar y obtener información relevante para el response.
     * @return WrapperResponse del tipo {@link WrapperResponse}: Wrapper construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	@SuppressWarnings("unchecked")
	private <T> T buildResponseWrapperObject(MensajesEnum message,Object[] paramMessage,T body,boolean isOk,Exception e,WebRequest request){
		String lang=ConstantesApp.LOCALE_DEFAULT;
		if(!ObjectUtils.isEmpty(request)) {
			lang=this.webUtil.getLang(request);
		}		
		String msg=this.webUtil.translate(message,paramMessage,lang);		
		String trace=this.webUtil.getTraceCode(request);
		log.error(msg,(!ObjectUtils.isEmpty(e)?e:null));
		return (T) new  WrapperResponse<>(isOk, msg, message.getCode(), message.getCategory().name(), message.getClas().name(),trace, body);
	}
	
	
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta del response a patir de {@link AuthenticationEntryPointConfig}  o Filters.
     * @param response del tipo {@link HttpServletResponse}: Response a tratar.
     * @param request del tipo {@link HttpServletRequest}: Request para tratar y obtener información relevante para el response.
     * @param statusCode del tipo {@link Integer}: statusCode a asignar al response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @param ex del tipo {@link Exception}: Error presentado a asignar al response.
     *
    * @author </br>
     * Developer: Arnold Campillo
     */
	private void buildErrorResponse(HttpServletResponse response,HttpServletRequest request,Integer statusCode, MensajesEnum message,Object[] paramMessage, Exception ex) {
		 response.setContentType(ConstantesApp.CONTENT_TYPE_JSON_UTF8);
	     response.setStatus(statusCode);
	     response.setCharacterEncoding(ConstantesApp.ENCODING_UTF8);
	     response.setHeader("Access-Control-Allow-Origin", AppConfig.getCorsOriginString()); // Ajusta según tu configuración
	     response.setHeader("Access-Control-Allow-Methods",ConstantesApp.ALLOWED_HEADERS_STRING);
	     response.setHeader("Access-Control-Allow-Headers", ConstantesApp.FULL_HEADERS_STRING);
	     response.setHeader("Access-Control-Allow-Credentials", "true");

	     if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
	         response.setStatus(HttpServletResponse.SC_OK);
	         return;
	     }
	     
	     try {
	    	 OutputStream out = response.getOutputStream();
	         PrintWriter writer = new PrintWriter(new OutputStreamWriter(out, StandardCharsets.UTF_8), true);
	         writer.println(objectMapper.writeValueAsString(this.buildErrorResponseWrapper(message,ex,request,paramMessage)));
		} catch (Exception e) {
			String lang=ConstantesApp.LOCALE_DEFAULT;
			if(!ObjectUtils.isEmpty(request)) {
				lang=this.webUtil.getLang(request);
			}			
			log.error(this.webUtil.translate(MensajesEnum.ERROR_PROCESS,lang), e);
		} 
	}
	 /**
     * Método encargado de construir el {@link ResponseEntity} a patir de {@link ErrorHandlerConfig}.
     * @param status del tipo {@link HttpStatus}: Status a asignar al response.
     * @param e del tipo {@link Exception}: Error presentado a asignar al response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param body del tipo {@link T}: Body a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @param request del tipo {@link WebRequest}: Request para tratar y obtener información relevante para el response.
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response construido.  
    * @author </br>
     * Developer: Arnold Campillo
     */
	private <T> ResponseEntity<T> buildErrorResponseGeneric(HttpStatus status, Exception e,MensajesEnum message,T body, Object[] paramMessage,WebRequest request) {	
		return ResponseEntity.status(status).body(this.buildErrorResponseWrapperObject(message,e,body,paramMessage,request));
	}
	
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta del response a patir de {@link ErrorHandlerConfig}.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param e del tipo {@link Exception}: Error presentado a asignar al response.
     * @param body del tipo {@link T}: Body a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @param request del tipo {@link WebRequest}: Request para tratar y obtener información relevante para el response.
     * @return WrapperResponse del tipo {@link WrapperResponse}: Wrapper construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	private <T> T buildErrorResponseWrapperObject(MensajesEnum message,Exception e,T body, Object[] paramMessage,WebRequest request){
		return this.buildResponseWrapperObject(message, paramMessage, body, false,e,request);
	}	

	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta del response a patir de {@link AuthenticationEntryPointConfig} o Filters.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param e del tipo {@link Exception}: Error presentado a asignar al response.
     * @param request del tipo {@link HttpServletRequest}: Request para tratar y obtener información relevante para el response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @return WrapperResponse del tipo {@link WrapperResponse}: Wrapper construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	private <T> WrapperResponse<T> buildErrorResponseWrapper(MensajesEnum message,Exception e, HttpServletRequest request,Object[] paramMessage){
		return this.buildResponseWrapper(message, paramMessage, null, false,e,request);
	}	

	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta exitosa del response.
     * @param body del tipo {@link T}: Body a asignar al response.
     * @return WrapperResponse del tipo {@link WrapperResponse}: Wrapper construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public <T> WrapperResponse<T> buildSuccessResponseWrapper(T body){
		return this.buildResponseWrapper(MensajesEnum.OK, null, body, true,null,this.webUtil.getRequest());
	}
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta exitosa del response sin body.
     * @return WrapperResponse del tipo {@link WrapperResponse}: Wrapper construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public <T> WrapperResponse<T> buildSuccessResponseWrapper(){
		return this.buildResponseWrapper(MensajesEnum.OK, null, null, true,null,this.webUtil.getRequest());
	}
	
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta exitosa del response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param body del tipo {@link T}: Body a asignar al response.
     * @return WrapperResponse del tipo {@link WrapperResponse}: Wrapper construido.  
    * @author </br>
     * Developer: Arnold Campillo
     */
	public <T> WrapperResponse<T> buildSuccessResponseWrapper(MensajesEnum message, T body){
		if(message.getCategory()!=CategoriaMensajesEnum.INFO||message.getClas()!=ClaseMensajesEnum.GENERIC) {
			message=MensajesEnum.OK;
		}
		return this.buildResponseWrapper(message, null, body, true,null,this.webUtil.getRequest());
	}
	
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta exitosa del response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @param body del tipo {@link T}: Body a asignar al response.
     * @return WrapperResponse del tipo {@link WrapperResponse}: Wrapper construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public <T> WrapperResponse<T> buildSuccessResponseWrapper(MensajesEnum message,Object[] paramMessage, T body){
		if(message.getCategory()!=CategoriaMensajesEnum.INFO||message.getClas()!=ClaseMensajesEnum.GENERIC) {
			message=MensajesEnum.OK;
		}
		return this.buildResponseWrapper(message, paramMessage, body, true,null,this.webUtil.getRequest());
	}
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta exitosa del response sin body.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @return WrapperResponse del tipo {@link WrapperResponse}: Wrapper construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public <T> WrapperResponse<T> buildSuccessResponseWrapper(MensajesEnum message){
		if(message.getCategory()!=CategoriaMensajesEnum.INFO||message.getClas()!=ClaseMensajesEnum.GENERIC) {
			message=MensajesEnum.OK;
		}
		return this.buildResponseWrapper(message, null, null, true,null,this.webUtil.getRequest());
	}
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta exitosa del response sin body.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @return WrapperResponse del tipo {@link WrapperResponse}: Wrapper construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public <T> WrapperResponse<T> buildSuccessResponseWrapper(MensajesEnum message,Object[] paramMessage){
		if(message.getCategory()!=CategoriaMensajesEnum.INFO||message.getClas()!=ClaseMensajesEnum.GENERIC) {
			message=MensajesEnum.OK;
		}
		return this.buildResponseWrapper(message, paramMessage, null, true,null,this.webUtil.getRequest());
	}
	 /**
     * Método encargado de construir el {@link ResponseEntity} a patir de {@link ErrorHandlerConfig}.
     * @param status del tipo {@link HttpStatus}: Status a asignar al response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	
	public <T> ResponseEntity<T> buildErrorResponse(HttpStatus status,MensajesEnum message) {
		return this.buildErrorResponseGeneric(status,null,message,null,null,null);
	}
	 /**
     * Método encargado de construir el {@link ResponseEntity} a patir de {@link ErrorHandlerConfig}.
     * @param status del tipo {@link HttpStatus}: Status a asignar al response.
     * @param e del tipo {@link Exception}: Error presentado a asignar al response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @param body del tipo {@link T}: Body a asignar al response.   
     * @param request del tipo {@link WebRequest}: Request para tratar y obtener información relevante para el response.
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public <T> ResponseEntity<T> buildErrorResponse(HttpStatus status, Exception e,MensajesEnum message,Object[] paramMessage,T body,WebRequest request) {
		return this.buildErrorResponseGeneric(status,e,message,body,paramMessage,request);
	}
	 /**
     * Método encargado de construir el {@link ResponseEntity} a patir de {@link ErrorHandlerConfig}.
     * @param status del tipo {@link HttpStatus}: Status a asignar al response.
     * @param e del tipo {@link Exception}: Error presentado a asignar al response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * @param request del tipo {@link WebRequest}: Request para tratar y obtener información relevante para el response.
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response construido.  
    * @author </br>
     * Developer: Arnold Campillo
     */
	public <T> ResponseEntity<T> buildErrorResponse(HttpStatus status, Exception e,MensajesEnum message,Object[] paramMessage,WebRequest reques) {
		return this.buildErrorResponseGeneric(status,e,message,null,paramMessage,reques);
	}
	 /**
     * Método encargado de construir el {@link ResponseEntity} a patir de {@link ErrorHandlerConfig}.
     * @param status del tipo {@link HttpStatus}: Status a asignar al response.
     * @param e del tipo {@link Exception}: Error presentado a asignar al response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param request del tipo {@link WebRequest}: Request para tratar y obtener información relevante para el response.
     * @return ResponseEntity del tipo {@link ResponseEntity}: Response construido.  
     * @author </br>
     * Developer: Arnold Campillo
     */
	public <T> ResponseEntity<T> buildErrorResponse(HttpStatus status, Exception e,MensajesEnum message,WebRequest request) {
		return this.buildErrorResponseGeneric(status,e,message,null,null,request);
	}
	
	
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta del response a patir de {@link AuthenticationEntryPointConfig}  o Filters.
     * @param response del tipo {@link HttpServletResponse}: Response a tratar.
     * @param request del tipo {@link HttpServletRequest}: Request para tratar y obtener información relevante para el response.
     * @param statusCode del tipo {@link Integer}: statusCode a asignar al response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     *
     * @author </br>
     * Developer: Arnold Campillo
     */
	public void buildErrorResponse(HttpServletResponse response,HttpServletRequest request,Integer statusCode, MensajesEnum message,Object[] paramMessage) {
		 this.buildErrorResponse(response, request,statusCode,message, paramMessage,null );
	}
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta del response a patir de {@link AuthenticationEntryPointConfig}  o Filters.
     * @param response del tipo {@link HttpServletResponse}: Response a tratar.
     * @param request del tipo {@link HttpServletRequest}: Request para tratar y obtener información relevante para el response.
     * @param statusCode del tipo {@link Integer}: statusCode a asignar al response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param ex del tipo {@link Exception}: Error presentado a asignar al response.
     *
    * @author </br>
     * Developer: Arnold Campillo
     */
	public void buildErrorResponse(HttpServletResponse response,HttpServletRequest request,Integer statusCode, MensajesEnum message, Exception ex) {		
		 this.buildErrorResponse(response,request, statusCode,message, null ,ex);
	}
	 /**
     * Método encargado de construir el {@link WrapperResponse} para la respuesta del response a patir de {@link AuthenticationEntryPointConfig}  o Filters.
     * @param response del tipo {@link HttpServletResponse}: Response a tratar.
     * @param request del tipo {@link HttpServletRequest}: Request para tratar y obtener información relevante para el response.
     * @param statusCode del tipo {@link Integer}: statusCode a asignar al response.
     * @param message del tipo {@link MensajesEnum}: Mensaje a asignar al response.
     * @param ex del tipo {@link Exception}: Error presentado a asignar al response.
     * @param paramMessage del tipo {@link Object}[]: Parametros requeridos para el mensaje.
     * 
    * @author </br>
     * Developer: Arnold Campillo
     */
	public void buildErrorResponse(HttpServletResponse response,HttpServletRequest request,Integer statusCode, MensajesEnum message, Exception ex,Object[] paramMessage) {		
		 this.buildErrorResponse(response,request, statusCode,message, paramMessage ,ex);
	}
	
}
