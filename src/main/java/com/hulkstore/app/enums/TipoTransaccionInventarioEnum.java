package com.hulkstore.app.enums;
/**
 * {@link Enum} que contiene los tipos de articulos para {@link TipoTransaccionInventarioEnum}.
 * @author </br>
 * Developer: Arnold Campillo
 */
public enum TipoTransaccionInventarioEnum {
	 VENTA, COMPRA
}