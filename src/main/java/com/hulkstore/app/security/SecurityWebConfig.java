package com.hulkstore.app.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.hulkstore.app.config.AppConfig;
import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.security.filter.Securityfilter;

import lombok.RequiredArgsConstructor;


/**
 * Clase de configuración encargada de las configuraciones de seguridad de la aplicación.
 * @author </br>
     * Developer: Arnold Campillo
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true, prePostEnabled = true)
@RequiredArgsConstructor
public class SecurityWebConfig {
	
    private final AuthenticationEntryPointConfig authenticationEntryPoint;
    private final AppAuthenticationProvider appAuthenticationProvider;
    private final Securityfilter securityfilter;
    
	 /**
     * Método encargado de inicializar y configurar el Bean {@link SecurityFilterChain}, encargado configurar la seguridad en la aplicación.
     * @param http del tipo {@link HttpSecurity}: Http security a configurar.
     * @return SecurityFilterChain del tipo {@link SecurityFilterChain}: Bean inicializado.  
     * @author </br>
     * Developer: Arnold Campillo
     */
   @Bean
   SecurityFilterChain filterChain(HttpSecurity http) throws Exception {	  
		http.csrf(AbstractHttpConfigurer::disable).cors(basicCors->
			basicCors.configurationSource(corsConfigurationSource())
		)
	      .authorizeHttpRequests(authorizationManagerRequestMatcherRegistry ->
	              authorizationManagerRequestMatcherRegistry.requestMatchers(HttpMethod.DELETE).hasRole(ConstantesApp.ROL_ADMIN)
	              		  .requestMatchers(ConstantesApp.ACCESS_SWAGGER.toArray(new String[0])).hasAnyRole(ConstantesApp.ROLES_SWAGGER.toArray(new String[0]))
	                      .requestMatchers(ConstantesApp.ENDPOINTS_PERMITALL.toArray(new String[0])).permitAll()
	                      .anyRequest().authenticated())
	
	      .httpBasic(httpSecurityHttpBasicConfigurer -> httpSecurityHttpBasicConfigurer.authenticationEntryPoint(authenticationEntryPoint))
	      .sessionManagement(httpSecuritySessionManagementConfigurer -> httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
		.addFilterBefore(securityfilter, UsernamePasswordAuthenticationFilter.class)
		.authenticationProvider(appAuthenticationProvider);
        return http.build();
    }
    
	 /**
    * Método encargado de inicializar y configurar el Bean {@link CorsConfigurationSource}, encargado configurar los Cors permitidos en la aplicación.
    * @return CorsConfigurationSource del tipo {@link CorsConfigurationSource}: Bean inicializado.  
    * @author </br>
     * Developer: Arnold Campillo
    */
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowedOrigins(AppConfig.getCorsOrigin());
		config.setAllowedMethods(ConstantesApp.ALLOWED_METHODS);
		config.setAllowCredentials(true);
		config.setAllowedHeaders(ConstantesApp.getFullHeaders());
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration(ConstantesApp.GENERAL_ACCESS_PATH, config);
		return source;
	}
	
}
