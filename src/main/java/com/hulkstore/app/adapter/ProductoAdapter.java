package com.hulkstore.app.adapter;

import java.util.List;
import java.util.function.Function;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.hulkstore.app.dto.request.ProductoRequestDTO;
import com.hulkstore.app.dto.response.ProductoResponseDTO;
import com.hulkstore.app.entity.Categoria;
import com.hulkstore.app.entity.Marca;
import com.hulkstore.app.entity.Producto;

/**
 * Adaptador encargado de definir la manera en que se deben convertir
 * objetos entre Entity y DTOs (Y viceversa).
 * @author </br>
 * Developer: Arnold Campillo.
 */
public class ProductoAdapter {
	
	private ProductoAdapter() {}
	
	  /**
     * Función encargada de adaptar un objeto DTO Request a su respectivo Entity(de {@link ProductoRequestDTO} a {@link Producto}).
     * @param dto del tipo {@link ProductoRequestDTO}: Dto Request a convertir a Entity.  
     * @return {@link Producto}: Entity resultado de la conversión. 
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static final Function<ProductoRequestDTO, Producto> requestToEntity = (ProductoRequestDTO dto) -> {
		if (ObjectUtils.isEmpty(dto)) {
			return null;
		}  
		
        return Producto.builder()
        		.id(dto.id())
        		.codigo(dto.codigo())
        		.nombre(dto.nombre())
        		.categoria(Categoria.builder().id(dto.categoriaId()).build())
        		.marca(Marca.builder().id(dto.marcaId()).build())
        		.cantidad(Integer.valueOf(dto.cantidad()))
        		.precioVenta(dto.precioVenta())
        		.referencia(dto.referencia())        		
        		.descripcion(dto.descripcion())
        		.estBorrado(dto.estBorrado())
				.build();
    };
    
	  /**
     * Función encargada de adaptar un objeto Entity a su respectivo DTO Response(de {@link Producto} a {@link ProductoResponseDTO}).
     * @param entity del tipo {@link Producto}: Entity a convertir a DTO Response. 
     * @return {@link ProductoResponseDTO}: DTO Response resultado de la conversión. 
     * @author </br>
     * Developer: Arnold Campillo
     */
    public static final Function<Producto, ProductoResponseDTO> entityToResponse = (Producto entity) -> {
		if (ObjectUtils.isEmpty(entity)) {
			return null;
		}  
		
		return ProductoResponseDTO.builder()
        		.id(entity.getId())
        		.codigo(entity.getCodigo())
        		.nombre(entity.getNombre())
        		.categoria(CategoriaAdapter.entityToResponse.apply(entity.getCategoria()))
        		.marca(MarcaAdapter.entityToResponse.apply(entity.getMarca()))
        		.cantidad(entity.getCantidad())
        		.precioVenta(entity.getPrecioVenta())
        		.referencia(entity.getReferencia())        		
        		.descripcion(entity.getDescripcion())
        		.estBorrado(entity.isEstBorrado())
				.build();
    };
    
	  /**
     * Función encargada de adaptar un listado Entity a su respectivo listado DTO Response(de {@link List}<{@link Producto}> a {@link List}<{@link ProductoResponseDTO}>).
     * @param entityList del tipo {@link List}<{@link Producto}>: Entity List a convertir a DTO Response List. 
     * @return {@link List}<{@link ProductoResponseDTO}>: DTO Response List resultado de la conversión. 
    * @author </br>
     * Developer: Arnold Campillo
     */
    
    public static final List<ProductoResponseDTO> listEntityToResponse(List<Producto> entityList) {
    	if (CollectionUtils.isEmpty(entityList)) {
    		return List.of();
    	} 
        return entityList.stream()
                .map(entityToResponse)
               .toList();
    }

}
