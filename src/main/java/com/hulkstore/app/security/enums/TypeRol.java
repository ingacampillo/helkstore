package com.hulkstore.app.security.enums;

/**
 * {@link Enum} que contiene los tipos de roles disponibles para el usuario.
 * @author </br>
 * Developer: Arnold Campillo
 */
public enum TypeRol {
	ROLE_ADMIN,
	ROLE_USER		
}
