package com.hulkstore.app.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hulkstore.app.enums.TipoTransaccionInventarioEnum;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "orden_producto")
@EqualsAndHashCode(callSuper = false)
@ToString
public class OrdenProducto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "codigo", nullable = false, updatable = false)
	@Builder.Default
	private String codigo = UUID.randomUUID().toString();
	
	@Column(name = "cantidad", nullable = false,columnDefinition = "int default 0")
	@Builder.Default
	private Integer cantidad=0;
	
	@ManyToOne(targetEntity = Producto.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "producto_id", referencedColumnName = "id", nullable = false)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Producto producto;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_transaccion", nullable = false, length = 20)
	private TipoTransaccionInventarioEnum tipoTransaccion;
    
	@Basic(optional = false)
	@Column(name = "fecha", nullable = false, columnDefinition = "DATETIME")
	@Builder.Default
	private LocalDateTime fecha=LocalDateTime.now();
	

}
