package com.hulkstore.app.security;

import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.AuthenticationsException;
import com.hulkstore.app.security.dto.request.AuthUserRequestDTO;
import com.hulkstore.app.security.dto.response.AuthUserResponseDTO;
import com.hulkstore.app.security.service.SecurityService;

import lombok.RequiredArgsConstructor;
/**
 * Clase provider de AUTH, encargada de realizar la autenticación por HTTP_BASIC.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Component
@RequiredArgsConstructor
public class AppAuthenticationProvider implements AuthenticationProvider {
	
	private final SecurityService securityService;
	
	 /**
     * Método encargado de realizar la autenticación HTTP_BASIC.
     * @param authentication del tipo {@link Authentication}: Autenticación a realizar.
     * @return Authentication del tipo {@link Authentication}: resultado de la autenticación.  
     * @author 
     * Developer: Arnold Campillo.
     * @throws
     */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationsException {
			if ((authentication.getName() == null || authentication.getName().isEmpty())
					&& (authentication.getCredentials().toString() == null
							|| authentication.getCredentials().toString().isEmpty())) {
				throw new AuthenticationsException(MensajesEnum.CREDENCIALS_REQUIRED);
			}
			if (authentication.getName() == null || authentication.getName().isEmpty()) {
				throw new AuthenticationsException(MensajesEnum.USER_REQUIRED);
			}
			if (authentication.getCredentials().toString() == null
					|| authentication.getCredentials().toString().isEmpty()) {
				throw new AuthenticationsException(MensajesEnum.PASSWORD_REQUIRED);
			}
			if (!authentication.getName().matches(ConstantesApp.PATTERN_CORREO)) {
				throw new AuthenticationsException(MensajesEnum.USER_USERNAME_INVALID,authentication.getName());
			}

			AuthUserResponseDTO userAuth=securityService.authenticate(AuthUserRequestDTO.builder()
					.username(authentication.getName())
					.password(authentication.getCredentials().toString())
					.build());
			List<SimpleGrantedAuthority> grantedAuths=userAuth.roles().stream().map(SimpleGrantedAuthority::new).toList();
			return new UsernamePasswordAuthenticationToken(userAuth,ConstantesApp.MASK_PASSWORD, grantedAuths);
	}
	
	/**
     * Método de configuración encargado de asignar los metodos de autenticación permitidos.
     * @param authentication del tipo {@link Class}: Autenticación a configurar.
     * @return boolean: resultado de la configuración.  
     * @author Acasi.</br>
     * Developer: Giovanny Camacho.
     * @throws
     */
	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}

}
