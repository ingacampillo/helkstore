package com.hulkstore.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hulkstore.app.entity.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Integer> {
	
	Boolean existsByNombre(String nombre);

	Optional<Producto> findById(Integer id);

	Optional<Producto> findByCodigo(String codigo);
	
	Boolean existsByReferencia(String referencia);
	

}
