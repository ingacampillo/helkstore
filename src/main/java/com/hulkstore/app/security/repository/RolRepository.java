package com.hulkstore.app.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hulkstore.app.security.entity.Rol;
/**
 * Interfaz Repository encargada de realizar las transacciones hacia la base de datos del entity {@link Rol}.
 * @author </br>
    * Developer: Arnold Campillo.
 */
@Repository
public interface RolRepository extends JpaRepository<Rol, Long> {

	  /**
	 * Método encargado de obtener el {@link Rol} a partir del codigo.
	 * @param codigo del tipo {@link String}: Parámetro requerido para la transacción.  
	 * @return {@link Optional}<{@link Rol}>: Entity resultado de la transacción. 
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	Optional<Rol> findByCodigo(String codigo);
}
