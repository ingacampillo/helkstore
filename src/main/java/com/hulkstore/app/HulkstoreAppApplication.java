package com.hulkstore.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hulkstore.app.util.AppUtil;

@SpringBootApplication
public class HulkstoreAppApplication {


	  /**
     * Método encargado de iniciar El Framewok de SpringBoot.
     * @param args del tipo String[]: Recibe los valores necesarios para ejecutar la aplicación.
     * @author </br>
     * Developer: Arnold Campillo
     */
	public static void main(String[] args) {
		AppUtil.addValueInLog();
		SpringApplication.run(HulkstoreAppApplication.class, args);
	}
}
