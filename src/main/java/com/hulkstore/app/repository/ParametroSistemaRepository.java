package com.hulkstore.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hulkstore.app.entity.ParametroSistema;

/**
 * Interfaz Repository encargada de realizar las transacciones hacia la base de datos del entity {@link ParametroSistema}.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Repository
public interface ParametroSistemaRepository extends JpaRepository<ParametroSistema, Integer> {

	  /**
	 * Método encargado de obtener el {@link ParametroSistema} a partir del codigo.
	 * @param codigo del tipo {@link String}: Parámetro requerido para la transacción.  
	 * @return {@link Optional}<{@link ParametroSistema}>: Entity resultado de la transacción. 
	 * @author </br>
	 * Developer: Arnold Campillo
	 */
	Optional<ParametroSistema> findByCodigo(String codigo);
	
	  /**
	 * Método encargado de validar la existencia del {@link ParametroSistema} a partir del codigo y id.
	 * @param codigo del tipo {@link String}: Parámetro requerido para la transacción.  
	 * @param id del tipo {@link Integer}: Parámetro requerido para la transacción.  
	 * @return {@link Boolean}: resultado de la transacción. 
	 * @author </br>
	 * Developer: Arnold Campillo
	 */
	Boolean existsByCodigoAndIdNot(String codigo,Integer id);
	
	  /**
	 * Método encargado de validar la existencia del {@link ParametroSistema} a partir del codigo.
	 * @param codigo del tipo {@link String}: Parámetro requerido para la transacción.  
	 * @return {@link Boolean}: resultado de la transacción.
	 * @author </br>
	 * Developer: Arnold Campillo
	 */
	Boolean existsByCodigo(String codigo);
}
