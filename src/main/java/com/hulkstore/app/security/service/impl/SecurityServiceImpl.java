package com.hulkstore.app.security.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.hulkstore.app.config.AppConfig;
import com.hulkstore.app.consts.ConstantesApp;
import com.hulkstore.app.enums.MensajesEnum;
import com.hulkstore.app.exceptions.AuthenticationsException;
import com.hulkstore.app.exceptions.NoDataFoundException;
import com.hulkstore.app.exceptions.ValidateServiceException;
import com.hulkstore.app.security.adapter.UsuarioAdapter;
import com.hulkstore.app.security.dto.request.AuthUserRequestDTO;
import com.hulkstore.app.security.dto.request.RefreshTokenRequestDTO;
import com.hulkstore.app.security.dto.response.AuthUserResponseDTO;
import com.hulkstore.app.security.dto.response.UsuarioResponseDTO;
import com.hulkstore.app.security.entity.UsuarioToken;
import com.hulkstore.app.security.enums.TypeTokenEnum;
import com.hulkstore.app.security.repository.UsuarioTokenRepository;
import com.hulkstore.app.security.service.JwtServices;
import com.hulkstore.app.security.service.SecurityService;
import com.hulkstore.app.security.service.UsuarioService;
import com.hulkstore.app.util.AESCryptorUtil;
import com.hulkstore.app.util.WebUtil;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
/**
 * Service encargado de realizar las transacciones y operaciones de seguridad en el sistema. Implementación {@link SecurityService}
 * @author </br>
    * Developer: Arnold Campillo.
 */
@Service
@RequiredArgsConstructor
public class SecurityServiceImpl implements SecurityService {

	
	private final JwtServices jwtServices;
	private final WebUtil webUtil;
	private final UsuarioTokenRepository usuarioTokenRepository;
	private final UsuarioService usuarioServices;
	private final PasswordEncoder passwordEncoder;
	
	Map<String, Integer> attempsPass = new HashMap<>();
	
	
	 /**
	 * Método encargado de obtener la información del usuario a partir del usercode.
	 * @param username del tipo {@link String}: Usuario a buscar.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario obtenido.  
	 * @author </br>
    * Developer: Arnold Campillo.
	 */
	@Override
	public AuthUserResponseDTO loadUserByUsername(String username) throws NoDataFoundException {
		UsuarioResponseDTO user=usuarioServices.findUserByUsername(username);		
		return UsuarioAdapter.responseToResponseAuth.apply(user);		
	}	
	
	 /**
	 * Método encargado de realizar la autenticación del usuario en el sistema.
	 * @param dto del tipo {@link AuthUserRequestDTO}: Dto request con la información requerida para la autenticación.
	 * @param token del tipo {@link String}: Token a validar.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario autenticado.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public AuthUserResponseDTO authenticate(AuthUserRequestDTO dto) throws  AuthenticationsException{	
		try {		
			UsuarioResponseDTO user=usuarioServices.findUserByUsername(dto.username());		
			validStatusUser(user,false);
			if (passwordMatches(dto.password(), user.password())) {
				attempsPass.remove(dto.username()); // Restablecer intentos de inicio de sesión fallidos	
				AuthUserResponseDTO userAuth=jwtServices.createToken(UsuarioAdapter.responseToResponseAuth.apply(user));					
				this.registerToken(userAuth,user);
				return userAuth;
			}
			
			int attempts = attempsPass.getOrDefault(dto.username(), 0) + 1;
			if (attempts <= AppConfig.getAttemptsPassword()) {
				attempsPass.put(dto.username(), attempts);
				throw new AuthenticationsException(MensajesEnum.USER_PASS_INCORRECT);
			} else {
				//usuarioServices.bloqUserByPasswordIncorrect(user.usercode());
				attempsPass.remove(dto.username()); // Restablecer intentos de inicio de sesión fallidos
				throw new AuthenticationsException(MensajesEnum.USER_BLOQ_PASS_INCORRECT,dto.username());
			}
		}catch (Exception  e) {
			 if(e instanceof NoDataFoundException nfe){
				throw new AuthenticationsException(nfe.getError(), nfe.getParamMessage());
			}
			throw e;
		}
		
	}

	 /**
	 * Método encargado de realizar el refresh token y generar nuevos tokens de acceso.
	 * @param dto del tipo {@link RefreshTokenRequestDTO}: Dto request con la información requerida para la petición.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario con los tokens actualizados.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	@Transactional
	public AuthUserResponseDTO refreshToken(RefreshTokenRequestDTO dto)throws ValidateServiceException, NoDataFoundException, AuthenticationsException {
		String token = webUtil.getToken();
		jwtServices.validateRefreshToken(dto.tokenAccess(), token);
		AuthUserResponseDTO userDetails = getUserByRefreshToken(token);
		UsuarioResponseDTO user=usuarioServices.findUserByUsername(userDetails.username());	
		validStatusUser(user,true);
		setAuthenticationContext(userDetails,webUtil.getRequest());
		userDetails.refreshToken();
		AuthUserResponseDTO userAuth=jwtServices.renovateToken(userDetails,token);
		this.registerToken(userAuth,user);
		return userAuth;
	}

	 /**
	 * Método encargado de asignar el usuario al contexto de autenticación {@link SecurityContextHolder}.
	 * @param token del tipo {@link String}: token requerido para la petición. 
	 * @param request del tipo {@link HttpServletRequest}: Request requerido para la solicitud.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public String setAuthenticationContext(String token, HttpServletRequest request)throws ValidateServiceException, NoDataFoundException {
		String username = ConstantesApp.UNDEFINED;	
		AuthUserResponseDTO userDetails = getUserByToken(token);
		if (userDetails != null) {
			username = userDetails.username();
			setAuthenticationContext(userDetails,request);
		}
		return username;
	}
	
	 /**
	 * Método encargado de validar el token con relación al usuario.
	 * @param username del tipo {@link String}: Usuario a validar.
	 * @param token del tipo {@link String}: Token a validar.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	public void validateTokenUser(String username, String token) throws AuthenticationsException, NoDataFoundException, ValidateServiceException {
		UsuarioResponseDTO user=usuarioServices.findUserByUsername(username);
		validStatusUser(user,false);
		String tokenType=jwtServices.getTokenType(token);
		boolean existsToken;
		if(tokenType.equals(TypeTokenEnum.ACCESS.name())) {
			existsToken=usuarioTokenRepository.existsByToken(token);
		}else {
			existsToken=usuarioTokenRepository.existsByRefreshToken(token);
		}
		
		if(!existsToken) {
			throw new AuthenticationsException(tokenType.equals(TypeTokenEnum.ACCESS.name())?MensajesEnum.TOKEN_INCORRECT:MensajesEnum.TOKEN_REFRESH_INCORRECT);	
		}		
		UsuarioToken userToken=usuarioTokenRepository.findByUsuarioUsername(user.username()).orElseThrow(() ->new AuthenticationsException(MensajesEnum.TOKEN_INCORRECT_USER_NO_AUTH));
		String tokenEvaluate=tokenType.equals(TypeTokenEnum.ACCESS.name())?userToken.getToken():userToken.getRefreshToken();

		if(!tokenEvaluate.equals(token)) {
			throw new AuthenticationsException(MensajesEnum.TOKEN_INCORRECT_USER);				
		}
		
	}

	 /**
	 * Método encargado de realizar el logout del usuario en el sistema.
	 * @param usercode del tipo {@link String}: usercode requerido para la petición. 
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	@Override
	@Transactional
	public void logout(String usercode) throws NoDataFoundException, AuthenticationsException{
		UsuarioResponseDTO user=usuarioServices.findUserByUsercode(usercode);		
		UsuarioToken userToken=usuarioTokenRepository.findByUsuarioUsername(user.username()).orElseThrow(() ->new AuthenticationsException(MensajesEnum.TOKEN_INCORRECT_USER_NO_AUTH));
		usuarioTokenRepository.deleteById(userToken.getId());
		
	}

	 /**
	 * Método encargado de asignar el usuario al contexto de autenticación {@link SecurityContextHolder}.
	 * @param token del tipo {@link String}: token requerido para la petición. 
	 * @param request del tipo {@link HttpServletRequest}: Request requerido para la solicitud.
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	private void setAuthenticationContext(AuthUserResponseDTO user,HttpServletRequest request) {
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user,
				null,user.roles().stream().map(SimpleGrantedAuthority::new).toList());
		authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	 /**
	 * Método encargado de obtener el usuario a patir del token access.
	 * @param token del tipo {@link String}: Token access al cual se le obtendra el usuario.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario obtenido del token.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private AuthUserResponseDTO getUserByToken(String token) throws ValidateServiceException, NoDataFoundException{
		return getUserByTokens(token,true);
	}
	
	 /**
	 * Método encargado de obtener el usuario a patir del refresh token.
	 * @param token del tipo {@link String}: Refresh token al cual se le obtendra el usuario.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario obtenido del token.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private AuthUserResponseDTO getUserByRefreshToken(String token) throws ValidateServiceException, NoDataFoundException{
		return getUserByTokens(token,false);
	}
	
	 /**
	 * Método encargado de obtener el usuario a patir de los tokens.
	 * @param token del tipo {@link String}: token al cual se le obtendra el usuario.
	 * @param isTokenAccess del tipo {@link boolean}: Valida si es token access o refresh token.
	 * @return user del tipo {@link AuthUserResponseDTO}: Usuario obtenido del token.
	  * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private AuthUserResponseDTO getUserByTokens(String token,boolean isTokenAccess) throws ValidateServiceException, NoDataFoundException {
		String user =isTokenAccess?jwtServices.getUserNameFromToken(token):jwtServices.getUserNameFromRefreshToken(token);	
		return loadUserByUsername(user);
	}
	
	 /**
	 * Método encargado de validar si las password coinciden.
	 * @param rawPassword del tipo {@link String}: Password a comparar.
	 * @param encodedPassword del tipo {@link String}: Password cifrada a comparar.
	 * @return matches del tipo {@link boolean}: Respuesta de la validación.
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	private boolean passwordMatches(String rawPassword, String encodedPassword) {
		return passwordEncoder.matches(decryptPassword(rawPassword), encodedPassword);
	}

	 /**
	 * Método encargado de desencriptar valores de AES.
	 * @param rawPassword del tipo {@link String}: Password a desencriptar.
	 * @return pass del tipo {@link String}: Password desencriptada.
	 * @author </br>
    * Developer: Arnold Campillo.
	 */
	private String decryptPassword(String rawPassword) {
		try {
			return AESCryptorUtil.decryptText(rawPassword,AppConfig.getkeyAes());
		} catch (Exception e) {
			return rawPassword;
		}
	}
	 /**
	 * Método encargado de asignar los tokens de acceso al usuario.
	 * @param userAuth del tipo {@link AuthUserResponseDTO}: Información del usuario autenticado.
	 * @param user del tipo {@link UsuarioResponseDTO}: Información del usuario.
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	private void registerToken(AuthUserResponseDTO userAuth, UsuarioResponseDTO user) {		
		UsuarioToken userToken;
		Optional<UsuarioToken> userTokenOpt=usuarioTokenRepository.findByUsuarioUsername(userAuth.username());
		if(userTokenOpt.isPresent()) {
			userToken=userTokenOpt.get();
			userToken.setToken(userAuth.token());
			userToken.setTokenExpire(userAuth.tokenExpire());
			userToken.setRefreshToken(userAuth.refreshToken());
			userToken.setRefreshTokenExpire(userAuth.refreshTokenExpire());
		}else {
			userToken=UsuarioToken.builder()
					.token(userAuth.token())
					.tokenExpire(userAuth.tokenExpire())
					.refreshToken(userAuth.refreshToken())
					.refreshTokenExpire(userAuth.refreshTokenExpire())
					.usuario(UsuarioAdapter.responseToEntity.apply(user))
					.build();
		}
		usuarioTokenRepository.save(userToken);
	}
	
	 /**
	 * Método encargado de validar el usuario.
	 * @param user del tipo {@link UsuarioResponseDTO}: Información del usuario.
	 * @param applyValidForceChangePass del tipo {@link boolean}: Valida si se debe aplicar la validacion de force chenge Pass.
	 * @author </br>
    * Developer: Arnold Campillo.
	 * @throws
	 */
	private void validStatusUser(UsuarioResponseDTO user,boolean applyValidForceChangePass) throws AuthenticationsException {

		if (user.bloquedByInvalidPass()) {
			throw new AuthenticationsException(MensajesEnum.USER_BLOQ_PASS_INCORRECT,user.username());
		}
		if (user.forceChangePass() && applyValidForceChangePass) {
			throw new AuthenticationsException(MensajesEnum.USER_CHANGE_PASS,user.username());
		}
		if (!user.enabled()) {
			throw new AuthenticationsException(MensajesEnum.USER_NO_ACTIVE,user.username());
		}
		if (CollectionUtils.isEmpty(user.roles())) {
			throw new AuthenticationsException(MensajesEnum.USER_NO_ROLES,user.username());
		}
	}

}
