FROM openjdk:17-jdk

ENV PORT=8080

EXPOSE 8080

COPY ./target/*.jar /app.jar
 
CMD exec java ${JAVA_USER_OPTS} -jar /app.jar
