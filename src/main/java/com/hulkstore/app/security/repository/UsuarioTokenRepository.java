package com.hulkstore.app.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hulkstore.app.security.entity.UsuarioToken;

/**
 * Interfaz Repository encargada de realizar las transacciones hacia la base de datos del entity {@link UsuarioToken}.
 * @author </br>
    * Developer: Arnold Campillo.
 */
@Repository
public interface UsuarioTokenRepository extends JpaRepository<UsuarioToken, Long> {

	  /**
	 * Método encargado de obtener el {@link UsuarioToken} a partir del username.
	 * @param username del tipo {@link String}: Parámetro requerido para la transacción.  
	 * @return {@link Optional}<{@link UsuarioToken}>: Entity resultado de la transacción. 
	  * @author </br>
    * Developer: Arnold Campillo.
	 */
	Optional<UsuarioToken> findByUsuarioUsername(String username);

	  /**
	 * Método encargado de validar la existencia del token.
	 * @param token del tipo {@link String}: Parámetro requerido para la transacción.  
	 * @return {@link Boolean}: resultado de la transacción.
	 * @author </br>
    * Developer: Arnold Campillo.
	 */
	Boolean existsByToken(String token);
	
	  /**
	 * Método encargado de validar la existencia del refresh token.
	 * @param refreshToken del tipo {@link String}: Parámetro requerido para la transacción.  
	 * @return {@link Boolean}: resultado de la transacción.
	 * @author </br>
    * Developer: Arnold Campillo.
	 */
	Boolean existsByRefreshToken(String refreshToken);

}
