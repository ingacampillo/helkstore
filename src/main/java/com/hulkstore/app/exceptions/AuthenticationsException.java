package com.hulkstore.app.exceptions;

import java.io.Serializable;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.hulkstore.app.enums.MensajesEnum;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Clase la cual es la encargada de contener la información relevante de las excepciones de autenticación y autorización del tipo {@link AuthenticationException}, presentadas en el sistema.
 * @author </br>
 * Developer: Arnold Campillo
 */
@Getter
@Setter
@Builder
@ResponseStatus(code= HttpStatus.UNAUTHORIZED)
public class AuthenticationsException extends  RuntimeException implements Serializable {

	private static final long serialVersionUID = 1L;
	private final MensajesEnum error;
	private final transient Object[] paramMessage;
	
	 /**
     * Constructor encargado de inicializar la variables de la clase.
     * @param error del tipo {@link MensajesEnum}: mensaje de error a asignar.
     * @param paramMessage del tipo {@link Object}[]: parametros que requiere el mensaje({@link MensajesEnum}). 
     * @author </br>
     * Developer: Arnold Campillo
     */	
	public AuthenticationsException(MensajesEnum error,Object... paramMessage) {
		super(error.getCode());
		this.error=error;
		this.paramMessage=paramMessage;
	}
	 /**
     * Constructor encargado de inicializar la variables de la clase.
     * @param error del tipo {@link MensajesEnum}: mensaje de error a asignar.
     * @author </br>
     * Developer: Arnold Campillo
     */	
	public AuthenticationsException(MensajesEnum error) {
		super(error.getCode());
		this.error=error;
		this.paramMessage=null;
	}
	
	
}
